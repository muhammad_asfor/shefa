import 'package:flutter/material.dart';
import 'package:tajra/Utils/Style.dart';

class SplashPagetest extends StatefulWidget {
  @override
  _SplashPagetestState createState() => _SplashPagetestState();
}

class _SplashPagetestState extends State<SplashPagetest> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [AppStyle.whiteColor, Color(0xFFE3E3E3)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Image.asset(
          'assets/Splash Screen.png',
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
