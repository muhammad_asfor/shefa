import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Utils/Style.dart';
import './/Utils/AppSnackBar.dart';
import './/Utils/SizeConfig.dart';
import '../../injections.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key? key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> with TickerProviderStateMixin {
  late HomeSettingsWrapper homeSettingsWrapper;
  final GlobalKey<_SplashContentState> splashKey =
      GlobalKey<_SplashContentState>();

  @override
  void initState() {
    homeSettingsWrapper = HomeSettingsWrapper(
      successCallback: goToHomePage,
      failureCallback: failledToGetSettings,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return AuthWrapper(
        onTokenReady: (isafterLogout, {token}) {
          if (!isafterLogout) {
            homeSettingsWrapper.getSettings(countryId: 166);
          }
        },
        onErrorInToken: (error) =>
            AppSnackBar.show(context, error, ToastType.Error),
        authBloc: sl<AuthBloc>(),
        child: homeSettingsWrapper
          ..child = SplashContent(
            key: splashKey,
          ));
  }

  failledToGetSettings(String error) {
    AppSnackBar.show(context, error, ToastType.Error);
  }

  goToHomePage() {
    splashKey.currentState?.setState(() {
      splashKey.currentState?.done = true;
    });
  }
}

class SplashContent extends StatefulWidget {
  SplashContent({
    Key? key,
  }) : super(key: key);

  @override
  _SplashContentState createState() => _SplashContentState();
}

class _SplashContentState extends State<SplashContent> {
  bool done = false;

  @override
  Widget build(BuildContext context) {
    if (done) {
      WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
        if (sl<AuthBloc>().isFirstLaunch) {
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //       builder: (context) => Create_account_Step_5Screen()),
          // );
          Navigator.pushReplacementNamed(context, '/Welcome');
        } else if (sl<AuthBloc>().isGuest) {
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //       builder: (context) => Create_account_Step_5Screen()),
          // );
          Navigator.pushReplacementNamed(context, '/base');
        } else {
          // Navigator.push(
          //   context,
          //   MaterialPageRoute(
          //       builder: (context) => Create_account_Step_5Screen()),
          // );
          Navigator.pushReplacementNamed(context, '/base');
        }
      });
    }
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [AppStyle.whiteColor, Color(0xFFE3E3E3)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
        ),
        child: Image.asset(
          'assets/Splash Screen.png',
          fit: BoxFit.fill,
        ),
      ),
    );
  }
}
