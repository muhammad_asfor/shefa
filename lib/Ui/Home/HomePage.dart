import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import 'package:tajra/App/Widgets/ProductCard2.dart';
import 'package:tajra/App/Widgets/ProductCardTest.dart';
import 'package:tajra/Ui/BasePage/BasePage.dart';
import 'package:tajra/Ui/Blog/presentation/widgets/Blog.dart';
import 'package:tajra/Ui/Home/widgets/carousel_section.dart';
import 'package:tajra/Ui/Notifications/bloc/notifications_bloc.dart';
import 'package:tajra/Ui/Search/widgets/search_bar.dart';
import '/Ui/Home/widgets/home_shimmer.dart';
import '/generated/l10n.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import '../../injections.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:badges/badges.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentCarouselIndex = 0;
  CountryModel? selectedCountry;
  CityModel? selectedCity;
  CityModel? selectedhand;
  StatesModel? selectedState;
  @override
  void initState() {
    super.initState();
    UniLinks.initDynamicLinks((id) {}, (id) {
      Navigator.pushNamed(context, "/productDetails", arguments: {"id": id});
    });
  }

  final searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var unreadNotifications =
        sl<HomesettingsBloc>().settings?.unreadNotifications ?? 0;

    var settings = sl<HomesettingsBloc>().settings!;
    return SafeArea(
      child: Scaffold(
        body: RefreshIndicator(
          onRefresh: () {
            sl<HomesettingsBloc>().add(GetSettings());
            return Future.delayed(Duration(seconds: 1)).then((value) {});
          },
          child: BlocBuilder(
            bloc: sl<HomesettingsBloc>(),
            builder: (context, state) {
              if (state is LoadingSettings) {
                return Column(
                  children: [HomePageShimmer()],
                );
              }
              settings = sl<HomesettingsBloc>().settings!;
              return SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          left: SizeConfig.w(30),
                          right: SizeConfig.w(30),
                          bottom: SizeConfig.h(23),
                          top: SizeConfig.h(23)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(50),
                            child: CircleAvatar(
                              backgroundColor: AppStyle.primaryColor,
                              foregroundColor: AppStyle.primaryColor,
                              radius: SizeConfig.h(50),
                              backgroundImage: (sl<HomesettingsBloc>()
                                          .settings
                                          ?.user
                                          ?.coverImage !=
                                      null)
                                  ? NetworkImage(
                                      (sl<HomesettingsBloc>()
                                              .settings
                                              ?.user
                                              ?.coverImage) ??
                                          "https://e7.pngegg.com/pngimages/146/551/png-clipart-user-login-mobile-phones-password-user-miscellaneous-blue-thumbnail.png",
                                    )
                                  : NetworkImage(
                                      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBgZbchuTiUtA3Egi1arN4BEQeoTaUtutQ8A&usqp=CAU"),
                            ),
                          ),
                          SizedBox(
                            width: SizeConfig.w(5),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                S.of(context).Good_Morning,
                                style: TextStyle(
                                    color: AppStyle.greyDark, fontSize: 12),
                              ),
                              Container(
                                width: SizeConfig.w(90),
                                height: SizeConfig.h(32),
                                child: FittedBox(
                                  fit: BoxFit.fitWidth,
                                  child: Text(
                                    sl<HomesettingsBloc>()
                                            .settings
                                            ?.user
                                            ?.name ??
                                        S.of(context).new_user,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            width: SizeConfig.w(60),
                          ),
                          SearchBar(
                            country: selectedCountry,
                            state: selectedState,
                            onTap: (Map<String, dynamic> data) {
                              selectedCountry = data['country'];
                              selectedCity = data['city'];
                              selectedState = data['state'];
                              selectedhand = data['hand'];
                            },
                          ),
                          unreadNotifications == 0
                              ? InkWell(
                                  onTap: () {
                                    if (sl<AuthBloc>().isGuest) {
                                      showLoginDialoge(context);
                                    } else {
                                      unreadNotifications = 0;
                                      sl<HomesettingsBloc>()
                                          .settings
                                          ?.unreadNotifications = 0;

                                      Navigator.pushNamed(
                                          context, "/notifications");
                                    }
                                  },
                                  child: Container(
                                    child: Image.asset("assets/note.png"),
                                  ),
                                )
                              : Badge(
                                  position: BadgePosition.topStart(
                                    start: SizeConfig.h(5),
                                    top: SizeConfig.h(1),
                                  ),
                                  badgeContent: Text(
                                    unreadNotifications.toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: SizeConfig.h(11)),
                                  ),
                                  child: InkWell(
                                    onTap: () {
                                      if (sl<AuthBloc>().isGuest) {
                                        showLoginDialoge(context);
                                      } else {
                                        unreadNotifications = 0;
                                        sl<HomesettingsBloc>()
                                            .settings
                                            ?.unreadNotifications = 0;
                                        Navigator.pushNamed(
                                            context, "/notifications");
                                      }
                                    },
                                    child: Container(
                                      child: Image.asset("assets/note.png"),
                                    ),
                                  ),
                                ),
                        ],
                      ),
                    ),
                    settings.slides?.isNotEmpty ?? false
                        ? CarouselSection(slides: settings.slides!)
                        : Padding(
                            padding: EdgeInsets.only(
                              left: SizeConfig.w(30),
                              right: SizeConfig.w(30),
                              bottom: SizeConfig.h(40),
                            ),
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: SizeConfig.h(13)),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(SizeConfig.h(20)),
                                gradient: LinearGradient(
                                  colors: [
                                    AppStyle.primaryColor,
                                    AppStyle.secondaryColor,
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              height: SizeConfig.h(127),
                              width: SizeConfig.w(315),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          S.of(context).Sorry,
                                          style: AppStyle.vexa14.copyWith(
                                              color: AppStyle.whiteColor,
                                              fontSize: 18),
                                        ),
                                        Text(
                                          S.of(context).You_donot_have_any,
                                          style: AppStyle.vexa14.copyWith(
                                              color: AppStyle.whiteColor),
                                        ),
                                        Text(
                                          S.of(context).appointments_today,
                                          style: AppStyle.vexa14.copyWith(
                                              color: AppStyle.whiteColor),
                                        ),
                                      ],
                                    ),
                                    Image.asset(
                                      'assets/Clipboard.png',
                                      fit: BoxFit.fill,
                                      height: SizeConfig.h(100),
                                      width: SizeConfig.h(100),
                                    ),
                                  ]),
                            ),
                          ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Padding(
                          padding:
                              Localizations.localeOf(context).languageCode ==
                                      "en"
                                  ? EdgeInsets.only(left: SizeConfig.w(30))
                                  : EdgeInsets.only(right: SizeConfig.w(30)),
                          child: buildSectionLabel(
                              S.of(context).Specialties, () {},
                              hasSeeAll: false),
                        ),
                        Padding(
                          padding:
                              Localizations.localeOf(context).languageCode ==
                                      "en"
                                  ? EdgeInsets.only(left: SizeConfig.w(25))
                                  : EdgeInsets.only(right: SizeConfig.w(25)),
                          child: buildCategoriesSection(settings),
                        ),
                        SizedBox(height: SizeConfig.h(24)),
                        ...[
                          if (settings.topDoctors!.isNotEmpty)
                            Column(
                              children: [
                                Padding(
                                  padding: Localizations.localeOf(context)
                                              .languageCode ==
                                          "en"
                                      ? EdgeInsets.only(left: SizeConfig.w(30))
                                      : EdgeInsets.only(
                                          right: SizeConfig.w(30)),
                                  child: buildSectionLabel(
                                      S.of(context).top_doctor, () {
                                    //   Navigator.
                                    Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                            builder: (context) => BasePage(
                                                  p: 1,
                                                )),
                                        (Route<dynamic> route) => false);
                                  }),
                                ),
                                SizedBox(
                                  height: SizeConfig.h(4),
                                ),
                                Padding(
                                  padding: Localizations.localeOf(context)
                                              .languageCode ==
                                          "en"
                                      ? EdgeInsets.only(left: 22.0)
                                      : EdgeInsets.only(right: 22.0),
                                  child: buildProductsSection(
                                      settings.topDoctors!),
                                ),
                              ],
                            ),
                          SizedBox(
                            height: SizeConfig.h(24),
                          ),
                          if (settings.blogPosts!.isNotEmpty)
                            Column(
                              children: [
                                if (settings.blogPosts!.isNotEmpty)
                                  Padding(
                                    padding: Localizations.localeOf(context)
                                                .languageCode ==
                                            "en"
                                        ? EdgeInsets.only(
                                            left: SizeConfig.w(30))
                                        : EdgeInsets.only(
                                            right: SizeConfig.w(30)),
                                    child: buildSectionLabel(
                                        S.of(context).blogs, () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => BlogPage()),
                                      );
                                    }),
                                  ),
                                SizedBox(
                                  height: SizeConfig.h(4),
                                ),
                                if (settings.blogPosts!.isNotEmpty)
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: SizeConfig.w(30),
                                        right: SizeConfig.w(30)),
                                    child: buildProducts2Section(
                                        settings.blogPosts!),
                                  ),
                              ],
                            ),
                        ],
                      ],
                    )
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  Container buildCategoriesSection(SettingsModel settings) {
    var specialities_length1 = settings.specialities!.length / 4;
    var specialities_length = specialities_length1.ceil().toDouble();

    return Container(
      height: SizeConfig.h(
          112 * (settings.specialities!.length > 4 ? specialities_length : 1)),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
            child: Wrap(
              children: settings.specialities!
                  .map((e) => InkWell(
                      onTap: () {
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => BasePage(
                                      p: 1,
                                      idCategory: e.id.toString(),
                                    )),
                            (Route<dynamic> route) => false);
                      },
                      child: buildCategoryCard(e)))
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }

  Container buildProductsSection(List<TopDoctor> section) {
    return Container(
      height: SizeConfig.h(165),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
            child: ListView.builder(
                padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(3)),
                scrollDirection: Axis.horizontal,
                itemCount: section.length,
                itemBuilder: (context, index) {
                  return ProductCardTest(product: section[index]);
                }),
          ),
        ],
      ),
    );
  }

  Container buildProducts2Section(List<Blog> section) {
    return Container(
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
              child: Column(
            children: section.map((e) => ProductCard2(product: e)).toList(),
          )),
        ],
      ),
    );
  }

  Widget buildCategoryCard(Specialitie specialitie) {
    return SizedBox(
      width: SizeConfig.w(80),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(16),
            width: SizeConfig.h(70),
            height: SizeConfig.h(70),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0.0, 3.0),
                      blurRadius: 6,
                      color: Colors.black12)
                ]),
            child: Image(
                image: NetworkImage(
              specialitie.coverImage,
            )),
          ),
          SizedBox(
            height: SizeConfig.h(10),
          ),
          SizedBox(
            width: SizeConfig.w(70),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    child: Text(
                  specialitie.title,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: SizeConfig.h(12), color: AppStyle.greyDark),
                ))
              ],
            ),
          ),
          SizedBox(
            height: SizeConfig.h(10),
          ),
        ],
      ),
    );
  }

  Container buildSectionLabel(String title, Function() onTap,
      {bool? hasSeeAll}) {
    return Container(
      margin: EdgeInsets.only(
        bottom: 14,
      ),
      padding: Localizations.localeOf(context).languageCode == "ar"
          ? EdgeInsets.only(left: SizeConfig.w(30))
          : EdgeInsets.only(right: SizeConfig.w(30)),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(
                fontSize: 16,
                color: Color(0xFF001133),
                fontWeight: FontWeight.w700),
          ),
          if (hasSeeAll ?? true)
            GestureDetector(
              onTap: onTap,
              child: Text(
                S.of(context).see_all,
                style: TextStyle(
                    height: 1.1, fontSize: 14, color: AppStyle.greyColor),
              ),
            ),
        ],
      ),
    );
  }
}
