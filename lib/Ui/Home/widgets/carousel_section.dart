import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Ui/BasePage/BasePage.dart';
import 'package:tajra/Utils/Style.dart';
import 'package:tajra/generated/l10n.dart';
import '/Utils/SizeConfig.dart';

class CarouselSection extends StatefulWidget {
  CarouselSection({Key? key, required this.slides}) : super(key: key);
  final List<Slide> slides;
  @override
  _CarouselSectionState createState() => _CarouselSectionState();
}

class _CarouselSectionState extends State<CarouselSection> {
  int currentCarouselIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: SizeConfig.h(154),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          CarouselSlider(
              options: CarouselOptions(
                  onPageChanged: (v, reason) {
                    setState(() {
                      currentCarouselIndex = v;
                    });
                  },
                  height: SizeConfig.h(154),
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 7),
                  scrollDirection: Axis.horizontal,
                  viewportFraction: SizeConfig.isWideScreen() ? 0.7 : 1),
              items: [
                for (Slide slide in widget.slides)
                  Padding(
                    padding: EdgeInsets.only(
                      left: SizeConfig.w(30),
                      right: SizeConfig.w(30),
                      bottom: SizeConfig.h(40),
                    ),
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: SizeConfig.h(13)),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(SizeConfig.h(20)),
                        gradient: LinearGradient(
                          colors: [
                            AppStyle.primaryColor,
                            AppStyle.secondaryColor,
                          ],
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                        ),
                      ),
                      height: SizeConfig.h(140),
                      width: SizeConfig.w(315),
                      child: Container(
                          height: SizeConfig.h(54),
                          width: SizeConfig.h(54),
                          child: Container(
                              height: SizeConfig.h(60),
                              width: SizeConfig.screenWidth - 50,
                              child: Container(
                                  child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          CircleAvatar(
                                            radius: SizeConfig.h(18),
                                            backgroundImage: NetworkImage(
                                                slide.doctor!.coverImage),
                                          ),
                                          SizedBox(
                                            width: SizeConfig.w(8),
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Text(
                                                slide.doctor!.name,
                                                style: AppStyle.vexa14.copyWith(
                                                    color: AppStyle.whiteColor,
                                                    fontSize: 12),
                                              ),
                                              Text(
                                                slide.date! +
                                                    "  " +
                                                    slide.time
                                                        .toString()
                                                        .split(":")[0] +
                                                    ":" +
                                                    slide.time
                                                        .toString()
                                                        .split(":")[1],
                                                style: AppStyle.vexa14.copyWith(
                                                    color: AppStyle.whiteColor,
                                                    fontSize: 12),
                                              ),
                                            ],
                                          ),
                                        ]),
                                    InkWell(
                                      onTap: () {
                                        Navigator.of(context)
                                            .pushAndRemoveUntil(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        BasePage(
                                                          p: 2,
                                                        )),
                                                (Route<dynamic> route) =>
                                                    false);
                                      },
                                      child: Container(
                                          decoration: BoxDecoration(
                                            border:
                                                Border.all(color: Colors.white),
                                            borderRadius: BorderRadius.circular(
                                                SizeConfig.h(50)),
                                          ),
                                          height: SizeConfig.h(26),
                                          width: SizeConfig.w(93),
                                          child: Center(
                                            child: Text(
                                              S.of(context).view_details,
                                              style: TextStyle(
                                                  fontSize: SizeConfig.h(10),
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          )),
                                    ),
                                  ])))),
                    ),
                  )
              ]),
        ],
      ),
    );
  }
}
