part of 'productdetails_bloc.dart';

@immutable
abstract class ProductdetailsState {}

class ProductdetailsInitial extends ProductdetailsState {}

class DetailsReady extends ProductdetailsState {
  final TopDoctorWithReviews product;
  DetailsReady(this.product);

}class ReservationsTablReady extends ProductdetailsState {
  final  Map<dynamic, dynamic>  Reservations;
  ReservationsTablReady(this. Reservations);
}

class LoadingDetails extends ProductdetailsState {}
class LoadingReservationsTabl extends ProductdetailsState {}

class ErrorInDetails extends ProductdetailsState {
  final String error;
  ErrorInDetails(this.error);
}
