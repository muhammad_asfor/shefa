import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/Ui/ProductDetails/bloc/productdetails_bloc.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'dart:async';
import 'package:flutter_svg/svg.dart';

import 'package:tajra/Utils/SizeConfig.dart';
import 'package:tajra/Utils/Style.dart';
import 'package:tajra/constants.dart';
import 'package:tajra/generated/l10n.dart';
import 'package:progiom_cms/ecommerce.dart';

import '../../injections.dart';

Future<Map<dynamic, dynamic>?> showSelectreservationsSheet(
    BuildContext context, String id, String date) {
  return showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0))),
      builder: (_) => Container(
            padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.h(16), vertical: SizeConfig.h(32)),
            decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(16.0),
                    topRight: Radius.circular(16.0))),
            height: SizeConfig.screenHeight * 0.85,
            child: showreservationsSheet(
              id: id,
              date: date,
            ),
          ));
}

class showreservationsSheet extends StatefulWidget {
  String? id;
  String? date;
  showreservationsSheet({Key? key, this.date, this.id}) : super(key: key);

  @override
  _showreservationsSheetState createState() => _showreservationsSheetState();
}

// MerchantsBloc bankBloc = MerchantsBloc(sl());

class _showreservationsSheetState extends State<showreservationsSheet> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    BlocProvider.of<ProductdetailsBloc>(context)
        .add(GetReservationsTabl(widget.id!, widget.date!));
  }

  setReservations(
    AddReservationsParams params,
  ) async {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
              height: 50,
              width: 20,
              child: Center(child: new CircularProgressIndicator())),
        );
      },
    );
    final result = await AddReservationsUsecass(sl()).call(params);
    result.fold((l) {
      Navigator.pop(context);
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      Navigator.pop(context);
      Navigator.pop(context);
      isAppo = true;
      AppSnackBar.show(
          context, S.of(context).Successfully_booked, ToastType.Success);
      getAppointments() async {
        final result = await GetAppointmentsUsecass(sl()).call("false");
        result.fold((l) {
          AppSnackBar.show(context, l.errorMessage, ToastType.Error);
        }, (r) {
          print(r);
          appointments = r;
        });
      }
    });
  }

  Map<dynamic, dynamic>? selectedCity;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: SizeConfig.h(30),
        ),
        Text(
          widget.date!,
          style: TextStyle(
              fontWeight: FontWeight.w500, fontSize: SizeConfig.h(17)),
        ),
        SizedBox(
          height: SizeConfig.h(30),
        ),
        Expanded(child: BlocBuilder<ProductdetailsBloc, ProductdetailsState>(
          builder: (context, state) {
            if (state is ReservationsTablReady) {
              return SingleChildScrollView(
                child: Row(
                  children: [
                    Column(
                      children: state.Reservations.keys
                          .map((e) =>
                              Container(height: 58, width: 50, child: Text(e)))
                          .toList(),
                    ),
                    SizedBox(
                      width: SizeConfig.w(8),
                    ),
                    Column(
                      children: state.Reservations.keys
                          .map((e) => InkWell(
                                onTap: () {
                                  if (!state.Reservations[e]["reserved"])
                                    showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (BuildContext context) {
                                        return Dialog(
                                          child: Container(
                                              height: SizeConfig.h(478),
                                              width: SizeConfig.w(317),
                                              child: Column(
                                                children: [
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        top: SizeConfig.h(45),
                                                        bottom:
                                                            SizeConfig.h(35)),
                                                    child: SvgPicture.asset(
                                                        "assets/Component 36 – 1.svg",
                                                        width:
                                                            SizeConfig.w(100),
                                                        height:
                                                            SizeConfig.h(100)),
                                                  ),
                                                  Container(
                                                      width: SizeConfig.w(188),
                                                      child: Text(
                                                        S
                                                            .of(context)
                                                            .bookingappointment,
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                          fontSize:
                                                              SizeConfig.h(17),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                        ),
                                                      )),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        top: SizeConfig.h(11),
                                                        bottom:
                                                            SizeConfig.h(22)),
                                                    child: Container(
                                                        width:
                                                            SizeConfig.w(263),
                                                        child: Text(
                                                          S
                                                              .of(context)
                                                              .Currentlythe,
                                                          textAlign:
                                                              TextAlign.center,
                                                          style: TextStyle(
                                                            fontSize:
                                                                SizeConfig.h(
                                                                    15),
                                                            fontWeight:
                                                                FontWeight.w400,
                                                          ),
                                                        )),
                                                  ),
                                                  SizedBox(
                                                    height: SizeConfig.h(30),
                                                  ),
                                                  InkWell(
                                                    onTap: () async {
                                                      Navigator.pop(context);
                                                      setReservations(
                                                        AddReservationsParams(
                                                            date: widget.date!,
                                                            doctor_id:
                                                                int.parse(
                                                                    widget.id!),
                                                            time: e),
                                                      );
                                                    },
                                                    child: Container(
                                                        decoration:
                                                            BoxDecoration(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      SizeConfig
                                                                          .h(50)),
                                                          gradient:
                                                              LinearGradient(
                                                            colors: [
                                                              AppStyle
                                                                  .primaryColor,
                                                              AppStyle
                                                                  .secondaryColor,
                                                            ],
                                                            begin: Alignment
                                                                .topLeft,
                                                            end: Alignment
                                                                .bottomRight,
                                                          ),
                                                        ),
                                                        height:
                                                            SizeConfig.h(55),
                                                        width:
                                                            SizeConfig.w(275),
                                                        child: Center(
                                                          child: Text(
                                                            S
                                                                .of(context)
                                                                .Book_Appointment,
                                                            style: TextStyle(
                                                                fontSize:
                                                                    SizeConfig
                                                                        .h(17),
                                                                color: Colors
                                                                    .white,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        )),
                                                  ),
                                                  SizedBox(
                                                    height: SizeConfig.h(30),
                                                  ),
                                                  TextButton(
                                                      onPressed: () {
                                                        Navigator.of(context)
                                                            .pop();
                                                      },
                                                      child: Text(
                                                          S.of(context).cancel,
                                                          style: TextStyle(
                                                              fontSize:
                                                                  SizeConfig.h(
                                                                      17),
                                                              color: Colors.red,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w500)))
                                                ],
                                              )),
                                        );
                                      },
                                    );
                                },
                                child: Container(
                                  width: SizeConfig.h(270),
                                  decoration: BoxDecoration(
                                      color: !state.Reservations[e]["reserved"]
                                          ? AppStyle.primaryColor
                                          : null,
                                      border: Border.all(
                                          color: state.Reservations[e]
                                                  ["reserved"]
                                              ? AppStyle.primaryColor
                                              : Color(0xFFE8E8E8),
                                          width: 1.5)),
                                  child: Padding(
                                    padding: EdgeInsets.only(
                                        left: 10,
                                        right: 10,
                                        top: 18.25,
                                        bottom: 18.25),
                                    child: Text(
                                      !state.Reservations[e]["reserved"]
                                          ? S.of(context).Open
                                          : S.of(context).Unavailable,
                                      style: TextStyle(
                                          color: !state.Reservations[e]
                                                  ["reserved"]
                                              ? Colors.white
                                              : Color(0xFF999999),
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                              ))
                          .toList(),
                    ),
                  ],
                ),
              );
            }
            if (state is LoadingReservationsTabl) {
              return Center(child: const CircularProgressIndicator());
            }
            return const SizedBox();
          },
        )),
        Row(
          children: [
            SizedBox(
              width: SizeConfig.h(44),
            ),
            //  Expanded(
            //     child: MainButton(
            //   onTap: () {
            //     Navigator.pop(context, selectedCity);
            //   },
            //   text: S.of(context).next,
            // )),
            SizedBox(
              width: SizeConfig.h(44),
            ),
          ],
        ),
        SizedBox(
          height: SizeConfig.h(8),
        ),
      ],
    );
  }

  // Widget buildBankCard(bool selected, CityModel bank) {
  //   return GestureDetector(
  //     onTap: () {
  //       setState(() {
  //         selectedCity = bank;
  //       });
  //     },
  //     child: Container(
  //       padding: padding16,
  //       height: SizeConfig.h(64),
  //       decoration: BoxDecoration(
  //           color: selected ? AppColors.primaryColor.shade700 : null,
  //           borderRadius: selected ? basicBorderRadius : null,
  //           boxShadow: selected ? const [AppStyle.basicBoxShadow] : null),
  //       child: Row(
  //         children: [
  //           SizedBox(
  //             width: SizeConfig.h(16),
  //           ),
  //           Text(
  //             bank.name,
  //             style: AppStyle.textTheme.bodyText2!.copyWith(
  //                 color: selected ? AppColors.whiteColor : AppColors.blackColor,
  //                 fontWeight: FontWeight.w400),
  //           ),
  //           const Spacer(),
  //           if (selected)
  //             SvgPicture.asset(
  //               "assets/check.svg",
  //               height: SizeConfig.h(24),
  //               width: SizeConfig.h(24),
  //             )
  //         ],
  //       ),
  //     ),
  //   );
  // }
}









// import 'package:ecash_core/core_styles.dart';
// import 'package:ecash_core/core_utils.dart';
// import 'package:ecash_core/core_widgets.dart';
// import 'package:ecash_user/generated/l10n.dart';
// import 'package:ecash_user/src/features/ourMerchants/presentation/bloc/merchants_bloc.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

// import 'package:flutter_svg/flutter_svg.dart';

// showCityFilterSheet(BuildContext context) {
//   return showModalBottomSheet(
//       isScrollControlled: true,
//       context: context,
//       shape: const RoundedRectangleBorder(
//           borderRadius: BorderRadius.only(
//               topLeft: Radius.circular(16.0), topRight: Radius.circular(16.0))),
//       builder: (_) => Container(
//             padding: EdgeInsets.symmetric(
//                 horizontal: SizeConfig.h(16), vertical: SizeConfig.h(32)),
//             decoration: const BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(16.0),
//                     topRight: Radius.circular(16.0))),
//             height: SizeConfig.screenHeight * 0.8,
//             child: const CityFilterSheet(),
//           ));
// }

// class CityFilterSheet extends StatefulWidget {
//   const CityFilterSheet({Key? key}) : super(key: key);

//   @override
//   _CityFilterSheetState createState() => _CityFilterSheetState();
// }

// class _CityFilterSheetState extends State<CityFilterSheet> {
//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   void didChangeDependencies() {
//     BlocProvider.of<MerchantsBloc>(context).add(GetAllCitys());
//     super.didChangeDependencies();
//   }

//   List<String> cities = [
//     "Damascus",
//     "Halab",
//     "Homs",
//     "Damascus",
//     "Halab",
//   ];
//   int selectedCity = 2;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.symmetric(
//         horizontal: SizeConfig.h(16),
//       ),
//       decoration: const BoxDecoration(
//           color: AppColors.whiteColor,
//           borderRadius: BorderRadius.only(
//               topLeft: Radius.circular(32.0), topRight: Radius.circular(32.0))),
//       height: SizeConfig.h(325),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           SizedBox(
//             height: SizeConfig.h(16),
//           ),
//           Text(
//             S.of(context).filter_by_city,
//             style: AppStyle.textTheme.bodyText2!
//                 .copyWith(fontWeight: FontWeight.w700),
//           ),
//           SizedBox(
//             height: SizeConfig.h(5),
//           ),
//           ClipRRect(
//             borderRadius: basicBorderRadius,
//             child: Padding(
//               padding: const EdgeInsets.all(5),
//               child: TextFormField(
//                 style: AppStyle.textTheme.subtitle2!,
//                 decoration: InputDecoration(
//                     filled: true,
//                     fillColor: AppColors.primaryColor[05],
//                     border: InputBorder.none,
//                     focusedBorder: OutlineInputBorder(
//                       borderRadius: basicBorderRadius,
//                       borderSide: BorderSide(
//                           color: AppColors.primaryColor.shade300, width: 2.0),
//                     ),
//                     focusColor: AppColors.primaryColor.shade300,
//                     label: Text(S.of(context).search_by_city),
//                     labelStyle: AppStyle.textTheme.subtitle2!
//                         .copyWith(color: AppColors.greyColor.shade800)),
//               ),
//             ),
//           ),
//           SizedBox(
//             height: SizeConfig.h(10),
//           ),
//           for (int i = 0; i < cities.length; i++)
//             Column(
//               children: [
//                 GestureDetector(
//                   behavior: HitTestBehavior.opaque,
//                   onTap: () {
//                     setState(() {
//                       selectedCity = i;
//                     });
//                   },
//                   child: Row(
//                     children: [
//                       if (selectedCity == i)
//                         SvgPicture.asset(
//                           "assets/check-circle-fill.svg",
//                           height: SizeConfig.h(24),
//                           width: SizeConfig.h(24),
//                         )
//                       else
//                         SizedBox(
//                           height: SizeConfig.h(24),
//                           width: SizeConfig.h(24),
//                         ),
//                       SizedBox(
//                         width: SizeConfig.h(8),
//                       ),
//                       Text(
//                         cities[i],
//                         style: AppStyle.textTheme.subtitle2!.copyWith(
//                             color: AppColors.greyColor.shade800,
//                             fontWeight: FontWeight.w700,
//                             fontSize: SizeConfig.h(14)),
//                       )
//                     ],
//                   ),
//                 ),
//                 spacing32,
//               ],
//             ),
//         ],
//       ),
//     );
//   }
// }
