import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:progiom_cms/auth.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import 'package:tajra/Ui/ProductDetails/map.dart';
import 'package:tajra/Ui/ProductDetails/reservations.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import '/App/Widgets/AppErrorWidget.dart';
import '/App/Widgets/AppLoader.dart';
import '/Ui/ProductDetails/bloc/productdetails_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '/Ui/ProductDetails/photo_zoom.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/HeroDialoge.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';
import '../../injections.dart';
import 'package:flutter_svg/svg.dart';
import 'package:webviewx/webviewx.dart';
import 'package:url_launcher/url_launcher.dart';

class ProductsDetailsPage extends StatefulWidget {
  final String id;
  final bool goToOptions;
  final bool forPointSale;

  ProductsDetailsPage(
      {required this.id,
      Key? key,
      required this.goToOptions,
      this.forPointSale: false})
      : super(key: key);

  @override
  _ProductsDetailsPageState createState() => _ProductsDetailsPageState();
}

class _ProductsDetailsPageState extends State<ProductsDetailsPage> {
  final ProductdetailsBloc bloc = ProductdetailsBloc();
  // Map? preferences;
  late YoutubePlayerController _controller;
  late WebViewXController<dynamic> webviewController;
  // getPreferences() async {
  //   final result = await GetPreferences(sl()).call(NoParams());
  //   result.fold((l) {
  //     AppSnackBar.show(context, l.errorMessage, ToastType.Error);
  //   }, (r) {
  //     if (mounted)
  //       setState(() {
  //         preferences = r;
  //       });
  //   });
  // }

  @override
  void initState() {
    // getPreferences();
    bloc.add(GetDetails(widget.id));
    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    _controller.close();
    super.dispose();
  }

  final optionsKey = GlobalKey();
  bool showSelectOptions = false;
  Map<String, String> selectedOptions = {};
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    var topPadding = MediaQuery.of(context).padding.top;

    return Scaffold(
      backgroundColor: AppStyle.backgroundColor,
      body: BlocConsumer(
        bloc: bloc,
        listener: (prev, curr) async {
          if (widget.goToOptions) if (curr is DetailsReady) {
            await Future.delayed(const Duration(milliseconds: 200));
            Scrollable.ensureVisible(optionsKey.currentContext!,
                duration: const Duration(milliseconds: 350),
                curve: Curves.fastOutSlowIn,
                alignment: 0.5);
            AppSnackBar.show(
                context, S.of(context).selectOptions, ToastType.Info);
          }
        },
        builder: (context, state) {
          if (state is LoadingDetails) {
            return Column(
              children: [Expanded(child: AppLoader())],
            );
          } else if (state is ErrorInDetails)
            return Center(
              child: AppErrorWidget(text: state.error),
            );
          else if (state is DetailsReady) {
            final product = state.product;

            return Stack(
              alignment: Alignment.topCenter,
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: SizeConfig.h(50), bottom: SizeConfig.h(20)),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                IconButton(
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                    icon: Icon(Icons.arrow_back)),
                                Padding(
                                  padding: EdgeInsets.only(
                                      right: SizeConfig.w(40),
                                      left: SizeConfig.w(80)),
                                  child: Center(
                                      child: Text(
                                    S.of(context).Doctor_Profile,
                                    style: TextStyle(
                                        fontSize: SizeConfig.h(20),
                                        fontWeight: FontWeight.bold),
                                  )),
                                ),
                              ]),
                        ),
                      ),
                      GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            Navigator.push(
                              context,
                              HeroDialogRoute(
                                builder: (context) => GallaryPage(
                                  image: product.coverImage,
                                  currentIndex: currentIndex,
                                ),
                              ),
                            );
                          },
                          child: buildCarousel(product.coverImage)),
                      SizedBox(
                        height: SizeConfig.h(12),
                      ),
                      Text(
                        product.name,
                        style: TextStyle(
                            fontSize: SizeConfig.h(20),
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: SizeConfig.h(5),
                      ),
                      Text(
                        product.speciality?.title ?? "",
                        style: TextStyle(
                            fontSize: SizeConfig.h(13),
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF999999)),
                      ),
                      // Text(
                      //   "Starting from 20/45 minutes",
                      //   style: TextStyle(
                      //       fontSize: SizeConfig.h(10),
                      //       fontWeight: FontWeight.bold,
                      //       color: Color(0xFF999999)),
                      // ),
                      SizedBox(
                        height: SizeConfig.h(12),
                      ),
                      RatingBar.builder(
                        initialRating: product.user_rating.toDouble(),
                        direction: Axis.horizontal,
                        allowHalfRating: true,
                        itemCount: 5,
                        itemSize: SizeConfig.h(15),
                        itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                        itemBuilder: (context, _) => Icon(
                          Icons.star,
                          color: Colors.amber,
                        ),
                        updateOnDrag: true,
                        glow: false,
                        ignoreGestures: true,
                        onRatingUpdate: (rating) {},
                      ),
                      SizedBox(
                        height: SizeConfig.h(24),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                          left: SizeConfig.w(30),
                          right: SizeConfig.w(30),
                          bottom: SizeConfig.h(22),
                        ),
                        child: Container(
                          padding: EdgeInsets.only(
                              top: SizeConfig.h(15),
                              left: SizeConfig.w(30),
                              right: SizeConfig.w(30)),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.circular(SizeConfig.h(80)),
                            boxShadow: [
                              BoxShadow(
                                  offset: Offset(0, 0),
                                  blurRadius: 10,
                                  color: Colors.black12)
                            ],
                          ),
                          height: SizeConfig.h(75),
                          width: SizeConfig.screenWidth,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(
                                      product.doctor_card?.experience_years
                                              .toString() ??
                                          "0",
                                      style: TextStyle(
                                          fontSize: SizeConfig.h(24),
                                          fontWeight: FontWeight.bold,
                                          color: AppStyle.primaryColor),
                                    ),
                                    Text(
                                      S.of(context).Years_exp,
                                      style: TextStyle(
                                          fontSize: SizeConfig.w(12),
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xFF999999)),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                height: 20,
                                width: 2,
                                color: Colors.grey,
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(
                                      product.clients_count.toString(),
                                      style: TextStyle(
                                          fontSize: SizeConfig.h(24),
                                          fontWeight: FontWeight.bold,
                                          color: AppStyle.primaryColor),
                                    ),
                                    Text(
                                      S.of(context).Clients,
                                      style: TextStyle(
                                          fontSize: SizeConfig.w(12),
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xFF999999)),
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                width: 5,
                              ),
                              Container(
                                height: 20,
                                width: 2,
                                color: Colors.grey,
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(
                                      product.certificates_count.toString(),
                                      style: TextStyle(
                                          fontSize: SizeConfig.h(24),
                                          fontWeight: FontWeight.bold,
                                          color: AppStyle.primaryColor),
                                    ),
                                    Text(
                                      S.of(context).Certificates,
                                      style: TextStyle(
                                          fontSize: SizeConfig.w(12),
                                          fontWeight: FontWeight.bold,
                                          color: Color(0xFF999999)),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      if (product.doctor_card?.bio != null)
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.w(30), right: SizeConfig.w(30)),
                          child: Container(
                            width: double.infinity,
                            child: Text(
                              S.of(context).About,
                              style: TextStyle(
                                  fontSize: SizeConfig.h(17),
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xFF333333)),
                            ),
                          ),
                        ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: SizeConfig.w(30),
                            right: SizeConfig.w(30),
                            top: SizeConfig.h(8),
                            bottom: SizeConfig.h(13)),
                        child: Container(
                          width: double.infinity,
                          child: Text(
                            product.doctor_card?.bio ?? "",
                            style: TextStyle(
                                fontSize: SizeConfig.h(12),
                                fontWeight: FontWeight.w400,
                                color: Color(0xFF333333)),
                          ),
                        ),
                      ),

                      if (product.doctor_card?.certificates_bag != null)
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.w(30), right: SizeConfig.w(30)),
                          child: Container(
                            width: double.infinity,
                            child: Text(
                              S.of(context).Certificates,
                              style: TextStyle(
                                  fontSize: SizeConfig.h(17),
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xFF333333)),
                            ),
                          ),
                        ),
                      if (product.doctor_card?.certificates_bag != null)
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.w(30),
                              right: SizeConfig.w(30),
                              top: SizeConfig.h(8),
                              bottom: SizeConfig.h(13)),
                          child: Container(
                              width: double.infinity,
                              child: Wrap(
                                children: product.doctor_card!.certificates_bag!
                                    .map((e) => GestureDetector(
                                        behavior: HitTestBehavior.opaque,
                                        onTap: () {
                                          Navigator.push(
                                            context,
                                            HeroDialogRoute(
                                              builder: (context) => GallaryPage(
                                                image: e,
                                                currentIndex: currentIndex,
                                              ),
                                            ),
                                          );
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Container(
                                              width: SizeConfig.h(85),
                                              height: SizeConfig.h(85),
                                              child: Image.network(e)),
                                        )))
                                    .toList(),
                              )),
                        ),
                      if (product.doctor_card?.address != null)
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.w(30),
                              right: SizeConfig.w(30),
                              bottom: SizeConfig.h(10)),
                          child: Container(
                            width: double.infinity,
                            child: Text(
                              S.of(context).address,
                              style: TextStyle(
                                  fontSize: SizeConfig.h(17),
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xFF333333)),
                            ),
                          ),
                        ),
                      if (product.doctor_card?.address != null)
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.w(30), right: SizeConfig.w(30)),
                          child: InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MapPage(
                                          map: product.doctor_card?.address! ??
                                              '',
                                        )),
                              );
                            },
                            child: WebViewX(
                              ignoreAllGestures: true,
                              initialContent:
                                  product.doctor_card?.address! ?? '',
                              initialSourceType: SourceType.html,
                              onWebViewCreated: (controller) =>
                                  webviewController = controller,
                              height: SizeConfig.h(150),
                              width: SizeConfig.h(350),
                            ),
                          ),
                        ),

                      ///
                      if (product.address != null)
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.w(30), right: SizeConfig.w(30)),
                          child: Row(
                            children: [
                              Container(
                                width: SizeConfig.w(20),
                                height: SizeConfig.h(20),
                                child: SvgPicture.asset("assets/marker.svg",
                                    width: SizeConfig.w(20),
                                    color: Color(0xFF096BB5),
                                    height: SizeConfig.h(20)),
                              ),
                              SizedBox(
                                width: SizeConfig.w(5),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    product.address ?? "",
                                    style: TextStyle(
                                        fontSize: SizeConfig.h(15),
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xFF096BB5)),
                                  ),
                                  Text(
                                    (product.country_text ?? "") +
                                        (product.city_text ?? "") +
                                        (product.state_text ?? ""),
                                    style: TextStyle(
                                        fontSize: SizeConfig.h(10),
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xFF999999)),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      // SizedBox(
                      //   height: SizeConfig.h(12),
                      // ),
                      // Padding(
                      //   padding: EdgeInsets.only(
                      //       left: SizeConfig.h(30), right: SizeConfig.h(30)),
                      //   child: Row(
                      //     children: [
                      //       Container(
                      //         width: SizeConfig.w(20),
                      //         height: SizeConfig.h(20),
                      //         child: SvgPicture.asset("assets/education.svg",
                      //             width: SizeConfig.w(20),
                      //             color: Color(0xFF096BB5),
                      //             height: SizeConfig.h(20)),
                      //       ),
                      //       SizedBox(
                      //         width: SizeConfig.w(5),
                      //       ),
                      //       Column(
                      //         crossAxisAlignment: CrossAxisAlignment.start,
                      //         children: [
                      //           Text(
                      //             'Medicare hospital',
                      //             style: TextStyle(
                      //                 fontSize: SizeConfig.h(15),
                      //                 fontWeight: FontWeight.w500,
                      //                 color: Color(0xFF096BB5)),
                      //           ),
                      //           Text(
                      //             '2250 G, Beach Road, New York, USA',
                      //             style: TextStyle(
                      //                 fontSize: SizeConfig.h(10),
                      //                 fontWeight: FontWeight.w400,
                      //                 color: Color(0xFF999999)),
                      //           )
                      //         ],
                      //       )
                      //     ],
                      //   ),
                      // ),

                      if (product.latest_reviews.isNotEmpty)
                        Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.w(30),
                              right: SizeConfig.w(30),
                              top: SizeConfig.h(34),
                              bottom: SizeConfig.h(12)),
                          child: Container(
                            width: double.infinity,
                            child: Text(
                              S.of(context).rates,
                              style: TextStyle(
                                  fontSize: SizeConfig.h(17),
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xFF333333)),
                            ),
                          ),
                        ),
                      if (product.latest_reviews.isNotEmpty)
                        Padding(
                          padding: EdgeInsets.only(
                            left: SizeConfig.w(18),
                            right: SizeConfig.w(18),
                          ),
                          child: Container(
                            height: SizeConfig.h(156),
                            width: double.infinity,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: product.latest_reviews.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Padding(
                                    padding: EdgeInsets.only(
                                        left: SizeConfig.w(12),
                                        right: SizeConfig.w(12),
                                        bottom: SizeConfig.h(12)),
                                    child: Container(
                                      padding: EdgeInsets.all(20),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.circular(
                                            SizeConfig.h(12)),
                                        boxShadow: [
                                          BoxShadow(
                                              offset: Offset(0, 0),
                                              blurRadius: 10,
                                              color: Colors.black12)
                                        ],
                                      ),
                                      height: SizeConfig.h(144),
                                      width: SizeConfig.w(297),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                S.of(context).evaluation_date,
                                                style: TextStyle(
                                                    fontSize: SizeConfig.h(13),
                                                    fontWeight: FontWeight.w500,
                                                    color: Color(0xFF333333)),
                                              ),
                                              Text(
                                                product.latest_reviews[index]
                                                    .created_at!
                                                    .split("T")[0],
                                                style: TextStyle(
                                                    fontSize: SizeConfig.h(12),
                                                    fontWeight: FontWeight.w400,
                                                    color: Color(0xFF999999)),
                                              )
                                            ],
                                          ),
                                          SizedBox(
                                            height: SizeConfig.h(8),
                                          ),
                                          RatingBar.builder(
                                            initialRating: product
                                                .latest_reviews[index].rate
                                                .toDouble(),
                                            direction: Axis.horizontal,
                                            allowHalfRating: true,
                                            itemCount: 5,
                                            itemSize: SizeConfig.h(15),
                                            itemPadding: EdgeInsets.symmetric(
                                                horizontal: 0.0),
                                            itemBuilder: (context, _) => Icon(
                                              Icons.star,
                                              color: Colors.amber,
                                            ),
                                            updateOnDrag: true,
                                            glow: false,
                                            ignoreGestures: true,
                                            onRatingUpdate: (rating) {},
                                          ),
                                          SizedBox(
                                            height: SizeConfig.h(14),
                                          ),
                                          Text(
                                            product.latest_reviews[index]
                                                    .title ??
                                                "",
                                            style: TextStyle(
                                                fontSize: SizeConfig.h(12),
                                                fontWeight: FontWeight.w500,
                                                color: Color(0xFF333333)),
                                          ),
                                          SizedBox(
                                            height: SizeConfig.h(4),
                                          ),
                                          Text(
                                            product
                                                .latest_reviews[index].comment!,
                                            style: TextStyle(
                                                fontSize: SizeConfig.h(12),
                                                fontWeight: FontWeight.w400,
                                                color: Color(0xFF333333)),
                                          )
                                        ],
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        ),
                      SizedBox(
                        height: SizeConfig.h(100),
                      )
                    ],
                  ),
                ),
                Positioned(
                  left: SizeConfig.w(30),
                  right: SizeConfig.w(30),
                  bottom: SizeConfig.h(30),
                  child: InkWell(
                    onTap: () async {
                      if (sl<AuthBloc>().isGuest) {
                        showLoginDialoge(context);
                      } else {
                        var result = await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime.now(),
                            lastDate: DateTime(2050));
                        if (result != null)
                          showSelectreservationsSheet(
                                  context,
                                  state.product.id.toString(),
                                  result.toString().split(" ")[0])
                              .then((value) {
                            if (value != null) {
                              setState(() {});
                            }
                          });
                      }
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(SizeConfig.h(50)),
                          gradient: LinearGradient(
                            colors: [
                              AppStyle.primaryColor,
                              AppStyle.secondaryColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        height: SizeConfig.h(55),
                        width: double.infinity,
                        child: Center(
                          child: Text(
                            S.of(context).Book_Appointment,
                            style: TextStyle(
                                fontSize: SizeConfig.h(17),
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                  ),
                ),
              ],
            );
          }
          return Container();
        },
      ),
    );
  }

  Widget buildCarousel(String image) {
    return Container(
      width: double.infinity,
      height: SizeConfig.h(120),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          CircleAvatar(
            radius: SizeConfig.h(60),
            backgroundImage: NetworkImage(image),
          ),
        ],
      ),
    );
  }
}
