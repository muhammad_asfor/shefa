import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:progiom_cms/auth.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import 'package:tajra/Ui/ProductDetails/reservations.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';
import '/App/Widgets/AppErrorWidget.dart';
import '/App/Widgets/AppLoader.dart';
import '/Ui/ProductDetails/bloc/productdetails_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '/Ui/ProductDetails/photo_zoom.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/HeroDialoge.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';
import '../../injections.dart';
import 'package:flutter_svg/svg.dart';
import 'package:webviewx/webviewx.dart';
import 'package:url_launcher/url_launcher.dart';

class MapPage extends StatefulWidget {
  final String map;

  MapPage({
    required this.map,
    Key? key,
  }) : super(key: key);

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  late WebViewXController<dynamic> webviewController;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: SizeConfig.screenHeight,
          width: SizeConfig.screenWidth,
          child: Padding(
            padding: EdgeInsets.only(top: 100),
            child: Center(
              child: WebViewX(
                initialContent: widget.map,
                initialSourceType: SourceType.html,
                onWebViewCreated: (controller) =>
                    webviewController = controller,
                height: SizeConfig.screenHeight + 500,
                width: SizeConfig.screenWidth,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
