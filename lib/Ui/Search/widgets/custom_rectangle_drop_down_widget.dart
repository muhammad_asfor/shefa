import 'package:flutter/material.dart';

import '../../../Utils/SizeConfig.dart';
import '../../../Utils/Style.dart';

class CustomRectangleDropdownWidget<T> extends StatefulWidget {
  final String title;
  final Color titleColor;
  final T? value;
  final List<DropdownMenuItem>? children;
  final Function? onChanged;

  const CustomRectangleDropdownWidget(
      {Key? key,
      required this.title,
      required this.titleColor,
      this.children,
      this.onChanged,
      this.value})
      : super(key: key);

  @override
  _CustomRectangleDropdownWidgetState createState() =>
      _CustomRectangleDropdownWidgetState();
}

class _CustomRectangleDropdownWidgetState
    extends State<CustomRectangleDropdownWidget> {
  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      value: widget.value,
      style: TextStyle(
        fontFamily: "Almaria",
        fontSize: SizeConfig.w(12),
        color: widget.titleColor,
        height: 1,
      ),
      items: widget.children,
      onChanged: (dynamic newValue) {
        if (widget.onChanged != null) widget.onChanged!(newValue);
        // do other stuff with _category
        // setState(() => _category = newValue);
      },
      decoration: InputDecoration(
        label: Text(
          widget.title,
          style: TextStyle(
            fontFamily: "Almaria",
            color: widget.titleColor,
            fontSize: SizeConfig.w(12),
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppStyle.greyColor),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: AppStyle.greyColor),
        ),
      ),
      icon: Icon(
        Icons.keyboard_arrow_down,
        color: AppStyle.primaryColor,
      ),
      // value: _category,
    );
  }
}
