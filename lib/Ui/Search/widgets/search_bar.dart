import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:progiom_cms/Getit_instance.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/LocationDialoge.dart';
import 'package:tajra/Ui/Search/widgets/custom_rectangle_drop_down_widget.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/Utils/SizeConfig.dart';
import 'package:tajra/Utils/Style.dart';
import 'package:tajra/data/sharedPreferences/SharedPrefHelper.dart';
import 'package:tajra/generated/l10n.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchBar extends StatefulWidget {
  final CountryModel? country;
  final StatesModel? state;
  final CityModel? city;
  final CityModel? hand;
  final Function onTap;
  final bool isHome;

  const SearchBar(
      {Key? key,
      required this.onTap,
      this.country,
      this.state,
      this.city,
      this.hand,
      this.isHome = true})
      : super(key: key);

  @override
  State<SearchBar> createState() => _SearchBarState();
}

CountryModel? selectedCountry;
List<CountryModel>? countries;

StatesModel? selectedState;
List<StatesModel> states = [];

CityModel? selectedCity;
List<CityModel> cities = [];

List<CityModel> hands = [];
CityModel? selectedhand;

final countryController = TextEditingController();
final stateController = TextEditingController();
final cityController = TextEditingController();
final handController = TextEditingController();

bool Application = false;

class _SearchBarState extends State<SearchBar> {
  getSelectedLocation() async {
    final result = await GetCountries(sl()).call(NoParams());
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      setState(() {
        countries = r;

        if (sl<SharedPreferences>().getString("country") != " ") {
          int idc = int.parse(
              sl<SharedPreferences>().getString("country").toString());
          countries!.forEach((element) {
            if (element.id == idc) {
              selectedCountry = element;
            }
          });
        }

        if (selectedCountry == null) {
          selectedCountry = countries![0];
        }
        countryController.text = selectedCountry!.title!;
        getStatesOfCurrentCountry(context, null);
      });
    });
    setState(() {
      isLoading = false;
    });
  }

  bool isLoading = true;

  @override
  void initState() {
    getSelectedLocation();

    super.initState();
  }

  restart(String country, String state, String city, String hand) async {
    await sl<PrefsHelper>().saveFilter(country, state, city, hand);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showModalBottomSheet(
            isScrollControlled: true,
            context: context,
            builder: (context) {
              return StatefulBuilder(builder: (context, modalState) {
                return Container(
                  height: SizeConfig.h(550),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: SizeConfig.w(14)),
                    child: ListView(
                      shrinkWrap: true,
                      children: [
                        SizedBox(height: SizeConfig.h(28)),
                        Container(
                            child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              S.of(context).filter,
                              style: TextStyle(
                                fontFamily: "Almaria",
                                color: AppStyle.primaryColor,
                                height: 1,
                                fontSize: SizeConfig.h(14),
                              ),
                            ),
                            InkWell(
                              onTap: () {
                                setState(() async {
                                  cities = [];
                                  Application = false;
                                  selectedState = null;
                                  selectedhand = null;
                                  selectedCity = null;
                                  stateController.text = '';
                                  cityController.text = '';
                                  handController.text = '';
                                  states = [];
                                  hands = [];
                                  Navigator.pop(context);
                                  if (widget.isHome) {
                                    await restart(" ", " ", " ", "");
                                    sl<HomesettingsBloc>().add(GetSettings());
                                  }
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/base', (Route<dynamic> route) => false);
                                });
                              },
                              child: Text(
                                S.of(context).delete_all,
                                style: TextStyle(
                                  fontFamily: "Almaria",
                                  color: Colors.red,
                                  fontSize: SizeConfig.h(16),
                                ),
                              ),
                            )
                          ],
                        )),
                        SizedBox(height: SizeConfig.h(40)),
                        TextFormField(
                          readOnly: true,
                          controller: countryController,
                          onTap: () {
                            FocusScope.of(context).unfocus();
                            showDialog(
                                    context: context,
                                    builder: (_) => LocationDialoge())
                                .then((value) {
                              if (value != null) {
                                modalState(() {
                                  states = [];
                                  cities = [];
                                  hands = [];
                                  selectedCity = null;
                                  selectedState = null;
                                  selectedhand = null;
                                  stateController.text = '';
                                  cityController.text = '';
                                  handController.text = "";
                                  selectedCountry = value;
                                  countryController.text =
                                      selectedCountry!.title!;
                                });
                                getStatesOfCurrentCountry(context, modalState);
                              }
                            });
                          },
                          decoration: InputDecoration(
                            suffixIcon: GestureDetector(
                              onTap: () {
                                modalState(() {
                                  selectedCity = null;
                                  selectedState = null;
                                  cities = [];
                                  states = [];
                                  hands = [];
                                  selectedCountry = null;
                                  selectedhand = null;
                                  handController.text = "";

                                  countryController.text = '';
                                  stateController.text = '';
                                  cityController.text = '';
                                });
                              },
                              child: Icon(
                                Icons.cancel_outlined,
                                color: AppStyle.disabledColor,
                              ),
                            ),
                            border: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: AppStyle.greyColor,
                              ),
                              borderRadius: BorderRadius.circular(3),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: AppStyle.greyColor,
                              ),
                              borderRadius: BorderRadius.circular(3),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                color: AppStyle.greyColor,
                              ),
                              borderRadius: BorderRadius.circular(3),
                            ),
                            hintText: S.of(context).country,
                            hintStyle: TextStyle(
                              fontFamily: "Almaria",
                              color: AppStyle.primaryColor,
                              height: 1,
                              fontSize: SizeConfig.h(12),
                            ),
                          ),
                        ),
                        SizedBox(height: SizeConfig.h(20)),
                        CustomRectangleDropdownWidget<StatesModel>(
                            title: S.of(context).state,
                            value: selectedState,
                            titleColor: states.isEmpty
                                ? AppStyle.disabledColor
                                : AppStyle.blacColor,
                            onChanged: (v) {
                              modalState(() {
                                selectedState = v;
                              });

                              getCitiesOfCurrentCountry(context, modalState);
                            },
                            children: states.isEmpty
                                ? null
                                : states.map((e) {
                                    return DropdownMenuItem<StatesModel>(
                                        value: e,
                                        child: Text(
                                          (e.name ?? ""),
                                          style: AppStyle.vexa12.copyWith(
                                              fontFamily: "Almaria",
                                              fontSize: SizeConfig.h(14)),
                                        ));
                                  }).toList()),
                        SizedBox(height: SizeConfig.h(20)),
                        CustomRectangleDropdownWidget<CityModel>(
                            title: S.of(context).city,
                            value: selectedCity,
                            titleColor: cities.isEmpty
                                ? AppStyle.disabledColor
                                : AppStyle.blacColor,
                            onChanged: (v) {
                              modalState(() {
                                selectedCity = v;
                              });
                              gethandOfCurrentCountry(context, modalState);
                            },
                            children: cities.isEmpty
                                ? null
                                : cities.map((e) {
                                    return DropdownMenuItem<CityModel>(
                                        value: e,
                                        child: Text(
                                          (e.name ?? ""),
                                          style: AppStyle.vexa12.copyWith(
                                              fontFamily: "Almaria",
                                              fontSize: SizeConfig.h(14)),
                                        ));
                                  }).toList()),
                        SizedBox(height: SizeConfig.h(20)),
                        CustomRectangleDropdownWidget<CityModel>(
                            title: S.of(context).hand + ":",
                            value: selectedhand,
                            titleColor: hands.isEmpty
                                ? AppStyle.disabledColor
                                : AppStyle.blacColor,
                            onChanged: (v) {
                              modalState(() {
                                selectedhand = v;
                              });
                            },
                            children: hands.isEmpty
                                ? null
                                : hands.map((e) {
                                    return DropdownMenuItem<CityModel>(
                                        value: e,
                                        child: Text(
                                          (e.name ?? ""),
                                          style: AppStyle.vexa12.copyWith(
                                              fontFamily: "Almaria",
                                              fontSize: SizeConfig.h(14)),
                                        ));
                                  }).toList()),
                        SizedBox(height: SizeConfig.h(40)),
                        GestureDetector(
                          onTap: () async {
                            if (widget.isHome) {
                              await restart(
                                selectedCountry?.id.toString() ?? " ",
                                selectedState?.id.toString() ?? " ",
                                selectedCity?.id.toString() ?? " ",
                                selectedhand?.id.toString() ?? " ",
                              );

                              sl<HomesettingsBloc>().add(GetSettings());
                            }

                            setState(() {
                              Application = true;
                            });
                            widget.onTap({
                              'country': selectedCountry,
                              'state': selectedState,
                              'city': selectedCity,
                            });

                            Navigator.of(context).pushNamedAndRemoveUntil(
                                '/base', (Route<dynamic> route) => false);
                          },
                          child: Container(
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                    AppStyle.primaryColor,
                                    AppStyle.secondaryColor,
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            padding: EdgeInsets.symmetric(
                                vertical: SizeConfig.w(14)),
                            child: Text(
                              S.of(context).submit,
                              style: TextStyle(
                                fontFamily: "Almaria",
                                fontSize: SizeConfig.w(16),
                                color: AppStyle.whiteColor,
                                height: 1,
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).viewInsets.bottom,
                        ),
                      ],
                    ),
                  ),
                );
              });
            });
      },
      child: Stack(
        children: [
          Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/not2.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Positioned(
            top: SizeConfig.h(12),
            right: SizeConfig.w(14),
            child: Container(
              child: SvgPicture.asset(
                "assets/icon-filter.svg",
                color: Application ? Colors.green : Colors.grey[800],
                height: SizeConfig.h(18),
                width: SizeConfig.h(18),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> getStatesOfCurrentCountry(
      BuildContext context, modalState) async {
    final result =
        await GetStates(sl()).call(GetStatesParams(selectedCountry!.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      if (modalState == null) {
        setState(() {
          states.clear();

          states.addAll(r);
          if (sl<SharedPreferences>().getString("state") != " ") {
            int idc = int.parse(
                sl<SharedPreferences>().getString("state").toString());
            states.forEach((element) {
              if (element.id == idc) {
                selectedState = element;
              }
            });
          }
          if (selectedState == null) {
            selectedState = states[0];
          }

          stateController.text = selectedState!.name!;
        });
      } else {
        modalState(() {
          states.clear();

          states.addAll(r);

          if (sl<SharedPreferences>().getString("state") != " ") {
            int idc = int.parse(
                sl<SharedPreferences>().getString("state").toString());
            states.forEach((element) {
              if (element.id == idc) {
                selectedState = element;
              }
            });
          }
          if (selectedState == null) {
            selectedState = states[0];
          }
          stateController.text = selectedState!.name!;
        });
      }
    });
  }

  Future<void> getCitiesOfCurrentCountry(
      BuildContext context, modalState) async {
    cities.clear();
    hands = [];
    final result = await GetCities(sl()).call(GetCitiesParams(
        countryId: selectedCountry!.id, stateId: selectedState?.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      if (modalState == null) {
        setState(() {
          cities.addAll(r);
          selectedCity =
              widget.city ?? (cities.isNotEmpty ? cities.first : null);
        });
      } else {
        modalState(() {
          cities.addAll(r);
          selectedCity =
              widget.city ?? (cities.isNotEmpty ? cities.first : null);
        });
      }
    });
  }

  Future<void> gethandOfCurrentCountry(BuildContext context, modalState) async {
    hands.clear();
    print(selectedhand?.id);
    final result = await GetCities(sl()).call(GetCitiesParams(
        countryId: selectedCountry!.id,
        stateId: selectedState?.id,
        handId: selectedCity?.id));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      if (modalState == null) {
        setState(() {
          hands.addAll(r);
          selectedhand = widget.hand ?? (hands.isNotEmpty ? hands.first : null);
        });
      } else {
        modalState(() {
          hands.addAll(r);
          selectedhand = widget.hand ?? (hands.isNotEmpty ? hands.first : null);
        });
      }
    });
  }
}
