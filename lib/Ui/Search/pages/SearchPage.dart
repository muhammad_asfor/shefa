// import 'package:flutter/material.dart';
// import 'package:progiom_cms/homeSettings.dart';
// import 'package:tajra/Ui/Home/widgets/service_card_widget.dart';
// import 'package:tajra/Ui/Search/widgets/search_bar.dart';
// import 'package:tajra/Ui/ServiceProviders/widgets/service_provider_list.dart';
// import '/App/Widgets/AppErrorWidget.dart';
// import '/App/Widgets/EmptyPlacholder.dart';
// import '/App/Widgets/Products_shimmer_grid.dart';
// import '/Ui/search/bloc/search_bloc.dart';
// import '/Utils/SizeConfig.dart';
// import '/Utils/Style.dart';
// import '/generated/l10n.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:progiom_cms/core.dart';
// import 'package:progiom_cms/ecommerce.dart';

// class SearchPage extends StatefulWidget {
//   final String? name;

//   SearchPage({Key? key, this.name}) : super(key: key);

//   @override
//   _SearchPageState createState() => _SearchPageState();
// }

// class _SearchPageState extends State<SearchPage> with TickerProviderStateMixin {
//   final SearchBLoc searchBLoc = SearchBLoc();
//   final TextEditingController searchController = TextEditingController();
//   bool showResult = false;
//   late SettingsModel settings;
//   late List<Category>? categories;
//   late bool isLoading;
//   late TabController tabController;
//   Category? category;
//   @override
//   void dispose() {
//     searchBLoc.close();
//     super.dispose();
//   }

//   @override
//   void initState() {
//     searchBLoc.add(LoadEvent({
//       'name': widget.name,
//     }));
//     searchController.text = widget.name!;
//     super.initState();
//   }

//   // List<String>? searchHistory;
//   // List<Product>? commonSearches;

//   @override
//   Widget build(BuildContext context) {
//     Future<bool> onWillPop() async {
//       if (category == null) {
//         return Future.value(true);
//       } else {
//         setState(() {});
//         category = null;
//         return Future.value(false);
//       }
//     }

//     return WillPopScope(
//       onWillPop: onWillPop,
//       child: SafeArea(
//         child: Scaffold(
//           appBar: category != null
//               ? AppBar(
//                   title: Text(
//                     // S.of(context).service_providers,
//                     category == null
//                         ? S.of(context).all_services
//                         : category!.title,
//                     style: TextStyle(
//                       color: AppStyle.darkTextColor,
//                       fontSize: SizeConfig.w(18),
//                       fontFamily: 'HelveticaNeueLTArabicBold',
//                       height: 1,
//                     ),
//                   ),
//                   leading: category == null
//                       ? null
//                       : IconButton(
//                           icon: Icon(
//                             Icons.arrow_back,
//                             color: Colors.black,
//                           ),
//                           onPressed: () {
//                             setState(() {
//                               category = null;
//                             });
//                           },
//                         ),
//                   centerTitle: true,
//                   backgroundColor: Colors.transparent,
//                   elevation: 0,
//                 )
//               : null,
//           backgroundColor: Colors.white,
//           body: category == null
//               ? Column(
//                   children: [
//                     buildSearchBar(context),
//                     Expanded(
//                       child: Padding(
//                         padding:
//                             EdgeInsets.symmetric(horizontal: SizeConfig.h(16)),
//                         child: Column(
//                           children: [
//                             // if (searchBLoc.query.length > 1 || showResult)
//                             buildSearchResult()
//                             // else
//                             //   Center(
//                             //     child: AppLoader(),
//                             //   )
//                           ],
//                         ),
//                       ),
//                     ),
//                   ],
//                 )
//               : Column(
//                   children: [
//                     TabBar(
//                         isScrollable: true,
//                         unselectedLabelColor: AppStyle.greyTextColor,
//                         indicatorSize: TabBarIndicatorSize.label,
//                         labelColor: AppStyle.primaryColor,
//                         indicatorColor: AppStyle.lightColor,
//                         labelStyle: TextStyle(
//                           color: AppStyle.primaryColor,
//                           fontSize: SizeConfig.w(14),
//                           fontFamily: 'HelveticaNeueLTArabicBold',
//                           height: 1,
//                         ),
//                         controller: tabController,
//                         tabs: [
//                           Tab(
//                             text: S.of(context).all,
//                           ),
//                           ...category?.subCategories
//                                   ?.map((e) => Tab(
//                                         text: e.title,
//                                       ))
//                                   .toList() ??
//                               [],
//                         ]),
//                     Expanded(
//                       child: TabBarView(
//                         controller: tabController,
//                         children: [
//                           ServiceProviderList(
//                             categoryId: category!.id.toString(),
//                             // service: state.items[index],
//                           ),
//                           ...category?.subCategories
//                                   ?.map((e) => ServiceProviderList(
//                                         categoryId: e.id.toString(),
//                                         // service: state.items[index],
//                                       ))
//                                   .toList() ??
//                               []
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//         ),
//       ),
//     );
//   }

//   buildSearchResult() {
//     return Expanded(
//       child: Column(
//         children: [
//           Expanded(
//             child: Padding(
//               padding: EdgeInsets.symmetric(
//                   horizontal: SizeConfig.h(0), vertical: SizeConfig.h(10)),
//               child: BlocBuilder(
//                 bloc: searchBLoc,
//                 builder: (context, state) {
//                   if (state is ErrorState) {
//                     return AppErrorWidget(text: state.error);
//                   }
//                   if (state is LoadingState) {
//                     return ProductsShimmerGrid(returnCustomScrollView: true);
//                   }
//                   if (state is SuccessState<List<Category>>) {
//                     if (state.items.isEmpty)
//                       return EmptyPlacholder(
//                         title: S.of(context).no_result,
//                         imageName: "assets/noSearch.png",
//                         subtitle: S.of(context).no_search_subtitle,
//                       );
//                     return GridView.count(
//                       shrinkWrap: true,
//                       padding: EdgeInsets.only(
//                           top: SizeConfig.w(20),
//                           left: SizeConfig.w(20),
//                           right: SizeConfig.w(20),
//                           bottom: SizeConfig.w(100)),
//                       // physics: NeverScrollableScrollPhysics(),
//                       crossAxisCount: 3,
//                       crossAxisSpacing: SizeConfig.w(10),
//                       mainAxisSpacing: SizeConfig.w(10),
//                       children: state.items
//                           .map((e) => ServiceCardWidget(
//                                 svgPath: e.coverImage,
//                                 service: e.title,
//                                 isFocused: true,
//                                 onTap: () {
//                                   setState(() {
//                                     category = e;
//                                     tabController = TabController(
//                                         length:
//                                             category!.subCategories!.length + 1,
//                                         vsync: this);
//                                     // // widget.onServiceTap(e.id.toString());
//                                   });
//                                 },
//                               ))
//                           .toList(),
//                     );
//                   }
//                   return Container();
//                 },
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   final Debouncer debouncer = Debouncer();

//   Widget buildSearchBar(BuildContext context) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.start,
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         IconButton(
//           icon: Icon(
//             Icons.arrow_back,
//             color: AppStyle.primaryColor,
//           ),
//           onPressed: () {
//             Navigator.pop(context);
//           },
//         ),
//       ],
//     );
//   }
// }
