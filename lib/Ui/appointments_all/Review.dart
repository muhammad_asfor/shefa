import 'dart:developer';
import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/ProductCard3.dart';
import 'package:tajra/App/Widgets/ProductCard4.dart';
import 'package:tajra/App/Widgets/ProductCard5.dart';
import 'package:tajra/constants.dart';
import '../../Utils/AppSnackBar.dart';
import '../../injections.dart';
import '/App/Widgets/AppErrorWidget.dart';
import '/App/Widgets/AppLoader.dart';
import '/App/Widgets/EmptyPlacholder.dart';
import '/App/Widgets/ProductCard.dart';
import '/App/Widgets/Products_shimmer_grid.dart';
import 'package:flutter_svg/svg.dart';
import '/Ui/Categories/bloc/category_bloc.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';

class ReviewPage extends StatefulWidget {
  int? idDoctor;
  ReviewPage({Key? key, this.idDoctor}) : super(key: key);

  @override
  _ReviewPageState createState() => _ReviewPageState();
}

class _ReviewPageState extends State<ReviewPage> {
  late GetProducatsByCategoryParams selectedCategoryId;
  bool loading = false;
  String comment = "";
  int rate = 0;
  @override
  void initState() {
    super.initState();
  }

  final settings = sl<HomesettingsBloc>().settings!;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: Padding(
            padding: EdgeInsets.only(
                left: SizeConfig.w(30),
                right: SizeConfig.w(30),
                bottom: SizeConfig.h(12)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: SizeConfig.h(8),
                ),
                Center(
                  child: Text(
                    S.of(context).Write_review,
                    style: AppStyle.vexa20.copyWith(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.h(25)),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.h(35),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      S.of(context).Perfect,
                      style: TextStyle(
                          fontSize: SizeConfig.h(17),
                          fontWeight: FontWeight.w500,
                          color: Color(0xFF333333)),
                    ),
                    SizedBox(
                      height: SizeConfig.h(8),
                    ),
                    RatingBar.builder(
                      initialRating: 3,
                      minRating: 1,
                      itemSize: SizeConfig.h(25),
                      direction: Axis.horizontal,
                      allowHalfRating: true,
                      itemCount: 5,
                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                      itemBuilder: (context, _) => Icon(
                        Icons.star,
                        color: Colors.amber,
                      ),
                      onRatingUpdate: (rating) {
                        rate = rating.toInt();
                      },
                    ),

                    // RatingBar.builder(
                    //   initialRating: 5,
                    //   direction: Axis.horizontal,
                    //   allowHalfRating: true,
                    //   itemCount: 5,
                    //   itemSize: SizeConfig.h(25),
                    //   itemPadding: EdgeInsets.symmetric(horizontal: 0.0),
                    //   itemBuilder: (context, _) => Icon(
                    //     Icons.star,
                    //     color: Colors.amber,
                    //   ),
                    //   updateOnDrag: true,
                    //   ignoreGestures: true,
                    //   onRatingUpdate: (rating) {},
                    // ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(20),
                ),
                Container(
                  height: 1,
                  color: Colors.grey,
                ),
                SizedBox(
                  height: SizeConfig.h(20),
                ),
                Text(
                  S.of(context).experience,
                  style: TextStyle(
                      fontSize: SizeConfig.h(17),
                      fontWeight: FontWeight.w500,
                      color: Color(0xFF333333)),
                ),
                SizedBox(
                  height: SizeConfig.h(20),
                ),
                TextField(
                  maxLines: 7,
                  onChanged: (v) {
                    setState(() {
                      comment = v;
                    });
                  },
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.0)),
                    ),
                    hintText: '',
                  ),
                ),
                SizedBox(
                  height: SizeConfig.h(50),
                ),
                if (loading)
                  AppLoader()
                else
                  InkWell(
                    onTap: () async {
                      await sendRating(context);
                    },
                    child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(SizeConfig.h(50)),
                          gradient: LinearGradient(
                            colors: [
                              AppStyle.primaryColor,
                              AppStyle.secondaryColor,
                            ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ),
                        ),
                        height: SizeConfig.h(55),
                        width: double.infinity,
                        child: Center(
                          child: Text(
                            S.of(context).send,
                            style: TextStyle(
                                fontSize: SizeConfig.h(17),
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        )),
                  ),
              ],
            ),
          )),
    );
  }

  Future<void> sendRating(BuildContext context) async {
    setState(() {
      loading = true;
    });
    final result = await ReviewOrderProduct(sl()).call(ReviewOrderProductParams(
        itemId: widget.idDoctor!, comment: comment, rating: rate));
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);

      setState(() {
        loading = false;
      });
    }, (r) {
      setState(() {
        loading = false;
      });
      Navigator.pop(context, true);
    });
  }
}
