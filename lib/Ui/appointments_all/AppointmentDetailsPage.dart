import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/ProductCard4.dart';
import 'package:tajra/Ui/appointments_all/Review.dart';
import 'package:tajra/Ui/appointments_all/rasheta.dart';
import 'package:tajra/constants.dart';
import '../../Utils/AppSnackBar.dart';
import '../../injections.dart';
import '/App/Widgets/AppLoader.dart';
import '/App/Widgets/EmptyPlacholder.dart';
import '/Ui/Categories/bloc/category_bloc.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';

class AppointmentDetailsPage extends StatefulWidget {
  final Map<dynamic, dynamic> appointment;
  AppointmentDetailsPage({Key? key, required this.appointment})
      : super(key: key);

  @override
  _AppointmentDetailsPageState createState() => _AppointmentDetailsPageState();
}

class _AppointmentDetailsPageState extends State<AppointmentDetailsPage> {
  @override
  void initState() {
    super.initState();
  }

  List ll = [];
  final settings = sl<HomesettingsBloc>().settings!;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: SizedBox(
                  height: SizeConfig.h(14),
                ),
              ),
              SliverToBoxAdapter(
                  child: Center(
                child: Text(
                  Localizations.localeOf(context).languageCode == "en"
                      ? "Appointment Details"
                      : "تفاصيل المعاينة",
                  style: AppStyle.vexa20.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.h(25)),
                ),
              )),
              widget.appointment["analyses_data"].isNotEmpty
                  ? SliverToBoxAdapter(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.h(30),
                              right: SizeConfig.h(30),
                              top: SizeConfig.h(30)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                S.of(context).Tests_list,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                              ),
                              SizedBox(
                                height: SizeConfig.h(15),
                              ),
                              Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    color: AppStyle.whiteColor,
                                    borderRadius: BorderRadius.circular(12),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(0, 0),
                                          blurRadius: 10,
                                          color: Colors.black12)
                                    ]),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ...widget.appointment["analyses_data"]
                                        .map((e) => Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.h(15)),
                                              child: Container(
                                                child: Text(e["title"]),
                                              ),
                                            ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  : SliverToBoxAdapter(child: Container()),
              widget.appointment["medicines_data"].isNotEmpty
                  ? SliverToBoxAdapter(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.h(30),
                              right: SizeConfig.h(30),
                              top: SizeConfig.h(30)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                S.of(context).Medicine,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                              ),
                              SizedBox(
                                height: SizeConfig.h(15),
                              ),
                              Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    color: AppStyle.whiteColor,
                                    borderRadius: BorderRadius.circular(12),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(0, 0),
                                          blurRadius: 10,
                                          color: Colors.black12)
                                    ]),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ...widget.appointment["medicines_data"]
                                        .map((e) => Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.h(15)),
                                              child: Container(
                                                child: Text(e["title"]),
                                              ),
                                            ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  : SliverToBoxAdapter(child: Container()),
              widget.appointment["allergies"] != null
                  ? SliverToBoxAdapter(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.h(30),
                              right: SizeConfig.h(30),
                              top: SizeConfig.h(30)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                S.of(context).sensitivities,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                              ),
                              SizedBox(
                                height: SizeConfig.h(15),
                              ),
                              Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    color: AppStyle.whiteColor,
                                    borderRadius: BorderRadius.circular(12),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(0, 0),
                                          blurRadius: 10,
                                          color: Colors.black12)
                                    ]),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                        padding:
                                            EdgeInsets.all(SizeConfig.h(15)),
                                        child: Container(
                                          child: Text(
                                              widget.appointment["allergies"]),
                                        ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  : SliverToBoxAdapter(child: Container()),
              widget.appointment["medicines_groups_data"].isNotEmpty
                  ? SliverToBoxAdapter(
                      child: SingleChildScrollView(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: SizeConfig.h(30),
                              right: SizeConfig.h(30),
                              top: SizeConfig.h(30)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                S.of(context).Diagnosis,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w500),
                              ),
                              SizedBox(
                                height: SizeConfig.h(15),
                              ),
                              Container(
                                width: double.infinity,
                                decoration: BoxDecoration(
                                    color: AppStyle.whiteColor,
                                    borderRadius: BorderRadius.circular(12),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(0, 0),
                                          blurRadius: 10,
                                          color: Colors.black12)
                                    ]),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    ...widget
                                        .appointment["medicines_groups_data"]
                                        .map((e) => Padding(
                                              padding: EdgeInsets.all(
                                                  SizeConfig.h(15)),
                                              child: Container(
                                                child: Text(e["title"]),
                                              ),
                                            ))
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  : SliverToBoxAdapter(child: Container()),
              SliverToBoxAdapter(
                  child: Row(
                children: [
                  widget.appointment["doctor"]["can_review"]
                      ? Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(
                              left: SizeConfig.w(10),
                              right: SizeConfig.w(10),
                              bottom: SizeConfig.h(30),
                              top: SizeConfig.h(50),
                            ),
                            child: InkWell(
                              onTap: () async {
                                bool rat = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ReviewPage(
                                          idDoctor: widget.appointment["doctor"]
                                              ["id"])),
                                );
                                if (rat)
                                  AppSnackBar.show(context,
                                      S.of(context).rev_sc, ToastType.Error);
                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.circular(SizeConfig.h(50)),
                                    gradient: LinearGradient(
                                      colors: [
                                        AppStyle.primaryColor,
                                        AppStyle.secondaryColor,
                                      ],
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                    ),
                                  ),
                                  height: SizeConfig.h(55),
                                  width: double.infinity,
                                  child: Center(
                                    child: Text(
                                      S.of(context).Preview_evaluation,
                                      style: TextStyle(
                                          fontSize: SizeConfig.h(17),
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )),
                            ),
                          ),
                        )
                      : Container(),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: SizeConfig.w(10),
                        right: SizeConfig.w(10),
                        bottom: SizeConfig.h(30),
                        top: SizeConfig.h(50),
                      ),
                      child: InkWell(
                        onTap: () async {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RashetaPage(
                                    idRasheta: widget.appointment["id"])),
                          );
                        },
                        child: Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(SizeConfig.h(50)),
                              gradient: LinearGradient(
                                colors: [
                                  AppStyle.primaryColor,
                                  AppStyle.secondaryColor,
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                              ),
                            ),
                            height: SizeConfig.h(55),
                            width: double.infinity,
                            child: Center(
                              child: Text(
                                S.of(context).Ratchet_printing,
                                style: TextStyle(
                                    fontSize: SizeConfig.h(17),
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),
                      ),
                    ),
                  ),
                ],
              )),
            ],
          )),
    );
  }
}
