import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/ProductCard3.dart';
import 'package:tajra/App/Widgets/ProductCard4.dart';
import 'package:tajra/App/Widgets/ProductCard5.dart';
import 'package:tajra/constants.dart';
import '../../Utils/AppSnackBar.dart';
import '../../injections.dart';
import '/App/Widgets/AppErrorWidget.dart';
import '/App/Widgets/AppLoader.dart';
import '/App/Widgets/EmptyPlacholder.dart';
import '/App/Widgets/ProductCard.dart';
import '/App/Widgets/Products_shimmer_grid.dart';
import 'package:flutter_svg/svg.dart';
import '/Ui/Categories/bloc/category_bloc.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';

class appointmentsPageAll extends StatefulWidget {
  appointmentsPageAll({
    Key? key,
  }) : super(key: key);

  @override
  _appointmentsPageAllState createState() => _appointmentsPageAllState();
}

class _appointmentsPageAllState extends State<appointmentsPageAll> {
  late GetProducatsByCategoryParams selectedCategoryId;
  final homeSettingsBloc = sl<HomesettingsBloc>();
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  bool isLoding = false;

  getAppointments() async {
    isLoding = false;
    final result = await GetAppointmentsUsecass(sl()).call("true");
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      isLoding = true;
      print(r);
      setState(() {
        appointments = r;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    if (!sl<AuthBloc>().isGuest) getAppointments();
  }

  final settings = sl<HomesettingsBloc>().settings!;
  @override
  Widget build(BuildContext context) {
    int yy = 0;
    isAppo = true;
    setState(() {});
    return SafeArea(
      child: Scaffold(
          key: _key,
          backgroundColor: Colors.white,
          body: CustomScrollView(
            slivers: [
              SliverToBoxAdapter(
                child: SizedBox(
                  height: SizeConfig.h(14),
                ),
              ),
              SliverToBoxAdapter(
                  child: Center(
                child: Text(
                  Localizations.localeOf(context).languageCode == "en"
                      ? "Your Appointments"
                      : S.of(context).PreviousAppointments,
                  style: AppStyle.vexa20.copyWith(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.h(25)),
                ),
              )),
              SliverToBoxAdapter(
                child: SizedBox(
                  height: SizeConfig.h(27),
                ),
              ),
              !isLoding
                  ? SliverToBoxAdapter(
                      child: Padding(
                        padding: EdgeInsets.only(top: 200),
                        child: Center(
                          child: AppLoader(),
                        ),
                      ),
                    )
                  : SliverToBoxAdapter(
                      child: SingleChildScrollView(
                          child: Row(children: [
                        Column(
                          children: appointments!.keys.map(
                            (e) {
                              if (!(appointments![e].isNotEmpty)) yy = yy + 1;

                              if (yy == appointments!.keys.length) {
                                return Container(
                                  height: SizeConfig.h(600),
                                  width:
                                      SizeConfig.screenWidth - SizeConfig.w(8),
                                  child: EmptyPlacholder(
                                    title: S.of(context).no_result,
                                    imageName: "assets/noSearch.png",
                                    subtitle: "",
                                    actionTitle: S.of(context).continuee,
                                    onActionTap: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                );
                              } else {
                                return Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    if (appointments![e].isNotEmpty)
                                      Padding(
                                        padding: EdgeInsets.only(
                                            top: 17, right: 30, left: 30),
                                        child: Container(
                                            height: SizeConfig.h(18),
                                            child: Text(
                                              e,
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: SizeConfig.h(16)),
                                            )),
                                      ),
                                    for (int i = 0;
                                        i < appointments![e].length;
                                        i++)
                                      Padding(
                                        padding: Localizations.localeOf(context)
                                                    .languageCode ==
                                                "en"
                                            ? EdgeInsets.only(
                                                left: 30.0, top: 14)
                                            : EdgeInsets.only(
                                                right: 30.0, top: 14),
                                        child: Container(
                                          height: SizeConfig.h(75),
                                          width: SizeConfig.w(317),
                                          child: ProductCard5(
                                            product: appointments![e][i],
                                          ),
                                        ),
                                      )
                                  ],
                                );
                              }
                            },
                          ).toList(),
                        ),
                        SizedBox(
                          width: SizeConfig.w(8),
                        ),
                      ])),
                    ),
              SliverToBoxAdapter(
                child: SizedBox(
                  height: SizeConfig.h(25),
                ),
              )
            ],
          )),
    );
  }
}
