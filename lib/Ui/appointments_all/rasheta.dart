import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/ProductCard3.dart';
import 'package:tajra/App/Widgets/ProductCard4.dart';
import 'package:tajra/App/Widgets/ProductCard5.dart';
import 'package:tajra/constants.dart';
import '../../Utils/AppSnackBar.dart';
import '../../injections.dart';
import '/App/Widgets/AppErrorWidget.dart';
import '/App/Widgets/AppLoader.dart';
import '/App/Widgets/EmptyPlacholder.dart';
import '/App/Widgets/ProductCard.dart';
import '/App/Widgets/Products_shimmer_grid.dart';
import 'package:flutter_svg/svg.dart';
import '/Ui/Categories/bloc/category_bloc.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';
import 'package:dio/dio.dart';

class RashetaPage extends StatefulWidget {
  int? idRasheta;
  RashetaPage({Key? key, this.idRasheta}) : super(key: key);

  @override
  _RashetaPageState createState() => _RashetaPageState();
}

class _RashetaPageState extends State<RashetaPage> {
  late GetProducatsByCategoryParams selectedCategoryId;

  @override
  void initState() {
    requestChangePassword();
    super.initState();
  }

  String urlImage = '';
  bool isLode = true;
  requestChangePassword() async {
    try {
      var response =
          await sl<Dio>().get('/api/reservations/${widget.idRasheta}/image');
      print(response.data);
      setState(() {
        isLode = false;
        urlImage = response.data["url"];
      });
    } on DioError catch (e) {
      throw ServerException(handleDioError(e));
    } catch (e) {
      throw ServerException(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: isLode
              ? Padding(
                  padding: EdgeInsets.only(top: 200),
                  child: Center(
                    child: AppLoader(),
                  ),
                )
              : Center(
                  child: Image.network(
                    urlImage,
                    fit: BoxFit.fill,
                  ),
                )),
    );
  }
}
