part of 'blog_bloc.dart';

@immutable
abstract class BlogEvent {}

class GetBlogs extends BlogEvent {
  GetBlogs();
}

class GetBlogdetails extends BlogEvent {
  final String Id;
  GetBlogdetails(this.Id);
}
