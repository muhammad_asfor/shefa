export 'blog_bloc.dart';
import 'dart:async';
import 'package:dartz/dartz.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:tajra/Ui/Blog/domain/model/modelBloge.dart';
import 'package:tajra/Ui/Blog/domain/usecases/get_blog.dart';
import '../../../../injections.dart';

class BlogBlocpaje extends SimpleLoaderBloc<List<DataBlog>> {
  int page = 0;

  BlogBlocpaje() : super(eventParams: NoParams());

  @override
  Future<Either<Failure, List<DataBlog>>> load(SimpleBlocEvent event) async {
    if (event is LoadEvent) {
      page = 0;
    }
    page++;

    return GetLsitBlog(sl()).call(ProducatsByCategoryParams(
      page: page,
    ));
  }
}
