import 'package:html/parser.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tajra/App/Widgets/CustomAppBar.dart';
import 'package:tajra/App/Widgets/EmptyPlacholder.dart';
import 'package:tajra/Ui/Blog/domain/model/modelBloge.dart';
import 'package:tajra/Ui/Blog/presentation/bloc/bloc.dart';
import 'package:tajra/Ui/Blog/presentation/widgets/BlogDetailsPage.dart';
import 'package:tajra/Utils/SizeConfig.dart';
import 'package:tajra/Utils/Style.dart';
import 'package:tajra/generated/l10n.dart';

class BlogPage extends StatefulWidget {
  @override
  _BlogPageState createState() => _BlogPageState();
}

class _BlogPageState extends State<BlogPage> {
  BlogBloc blog = BlogBloc();
  late BlogBlocpaje blogBlocpaje;

  @override
  void initState() {
    blogBlocpaje = BlogBlocpaje();
    blog.add(GetBlogs());

    super.initState();
  }

  List<dynamic> blogs = [];

  bool blogview = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocListener(
        bloc: blog,
        listener: (context, BlogState state) {
          if (state is BlogReady) {
            setState(() {
              blogs = state.blogs;
              blogview = true;
            });
          } else {
            blogview = false;
          }
        },
        child: BlocBuilder(
            bloc: blog,
            builder: (context, BlogState state) {
              return Scaffold(
                  backgroundColor: Colors.white,
                  body: Padding(
                    padding: EdgeInsets.only(
                        left: SizeConfig.w(20), right: SizeConfig.w(20)),
                    child: CustomScrollView(
                      physics: blogs.isNotEmpty
                          ? null
                          : NeverScrollableScrollPhysics(),
                      controller: blogBlocpaje.scrollController,
                      slivers: [
                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: SizeConfig.h(22),
                          ),
                        ),
                        buildAppBar(),
                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: SizeConfig.h(14),
                          ),
                        ),
                        !blogview
                            ? SliverToBoxAdapter(
                                child: Center(
                                  child: CircularProgressIndicator(),
                                ),
                              )
                            : blogs.isNotEmpty
                                ? buildBlogs(blogs as List<DataBlog>)
                                : SliverFillRemaining(
                                    child: EmptyPlacholder(
                                    skipNavBarHeight: true,
                                    title: S.of(context).no_result,
                                    imageName: "assets/noSearch.png",
                                    subtitle: "",
                                    actionTitle: S.of(context).continueShopping,
                                    onActionTap: () {
                                      Navigator.pushNamedAndRemoveUntil(context,
                                          "/base", ModalRoute.withName('/'));
                                    },
                                  )),
                        SliverToBoxAdapter(
                          child: SizedBox(
                            height: SizeConfig.h(30),
                          ),
                        ),
                      ],
                    ),
                  ));
            }),
      ),
    );
  }

  SliverToBoxAdapter buildAppBar() {
    return SliverToBoxAdapter(
      child: Center(
        child: Text(
          S.of(context).blog,
          style: AppStyle.vexa20.copyWith(
              color: Colors.black, fontWeight: FontWeight.bold, fontSize: 25),
        ),
      ),
    );
  }

  SliverToBoxAdapter buildBlogs(List<DataBlog> dataBlogs) {
    return SliverToBoxAdapter(
      child: SizedBox(
        width: SizeConfig.screenWidth,
        height: SizeConfig.screenHeight,
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.h(7),
            ),
            Expanded(
                child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    itemCount: dataBlogs.length,
                    itemBuilder: (context, index) {
                      final dataBlog = dataBlogs[index];

                      return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => BlogsDetailsPage(
                                        id: dataBlog.id.toString(),
                                      )));
                            },
                            child: Padding(
                              padding: EdgeInsets.only(bottom: 20),
                              child: Container(
                                width: double.infinity,
                                height: SizeConfig.h(85),
                                decoration: BoxDecoration(
                                    color: AppStyle.whiteColor,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: Offset(0, 10),
                                          blurRadius: 10,
                                          color: Colors.black12)
                                    ]),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Expanded(
                                        child: SizedBox(
                                            child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                child: Image.network(
                                                  dataBlog.coverImage!,
                                                  fit: BoxFit.fill,
                                                ))),
                                      ),
                                      Expanded(
                                        flex: 3,
                                        child: Container(
                                          padding: EdgeInsets.all(10),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                  child: Text(
                                                dataBlog.title!,
                                                style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w700,
                                                  color: Color(0xFF001133),
                                                ),
                                                overflow: TextOverflow.ellipsis,
                                              )),
                                              Expanded(
                                                  child:

                                                      // Html(
                                                      //   data: product.description,
                                                      //   style: {
                                                      //     "*":
                                                      //         Style.fromTextStyle(AppStyle.vexa12).copyWith(
                                                      //       maxLines: 2,
                                                      //       fontWeight: FontWeight.normal,
                                                      //       color: Color(0xFF8E8E8E),
                                                      //     )
                                                      //   },
                                                      // ),

                                                      Text(
                                                parse(parse(dataBlog
                                                            .description)
                                                        .body!
                                                        .text)
                                                    .documentElement!
                                                    .text,
                                                style: TextStyle(
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.normal,
                                                  color: Color(0xFF8E8E8E),
                                                ),
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                              )),
                                              Container(
                                                height: SizeConfig.h(12),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ]),
                              ),
                            ),
                          ));
                    }))
          ],
        ),
      ),
    );
  }
}
