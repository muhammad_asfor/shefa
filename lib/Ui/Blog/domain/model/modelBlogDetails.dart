class DataBlogDetalse {
  int? id;
  String? coverImage;
  String? title;
  String? description;

  DataBlogDetalse(
      {this.id,
      this.coverImage,
      this.title,
      this.description});

  DataBlogDetalse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    coverImage = json['cover_image'];
    title = json['title'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cover_image'] = this.coverImage;
    data['title'] = this.title;
    data['description'] = this.description;
    return data;
  }
}
