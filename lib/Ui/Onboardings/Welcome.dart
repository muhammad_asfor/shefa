import 'package:flutter/material.dart';

import 'package:progiom_cms/homeSettings.dart';

import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/generated/l10n.dart';

import '../../injections.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppStyle.whiteColor,
        body: Stack(
          children: [
            Column(
              children: [
                Expanded(flex: 6, child: buildOnBoarding()),
                Expanded(flex: 2, child: Container())
              ],
            ),
            Positioned(
              top: SizeConfig.screenHeight - 170,
              left: SizeConfig.h(20),
              right: SizeConfig.h(20),
              child: InkWell(
                onTap: () {
                  Navigator.pushReplacementNamed(context, "/onboard");
                },
                child: Container(
                  height: SizeConfig.h(60),
                  width: SizeConfig.screenWidth - 50,
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(SizeConfig.h(100)),
                    gradient: LinearGradient(
                      colors: [
                        AppStyle.primaryColor,
                        AppStyle.secondaryColor,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      S.of(context).startNow,
                      style:
                          AppStyle.vexa14.copyWith(color: AppStyle.whiteColor),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildOnBoarding() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(child: Container()),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              S.of(context).welcome,
              style: AppStyle.vexa20
                  .copyWith(color: AppStyle.secondaryDark, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              S.of(context).welcome2,
              style: AppStyle.vexa16
                  .copyWith(color: AppStyle.greyColor, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(child: Container()),
        Expanded(
          flex: 6,
          child: Image.asset(
            'assets/Welcome.png',
            height: SizeConfig.h(250),
            width: SizeConfig.h(250),
          ),
        ),
      ],
    );
  }
}
