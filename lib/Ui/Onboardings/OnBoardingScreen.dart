import 'package:flutter/material.dart';

import 'package:progiom_cms/homeSettings.dart';

import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/generated/l10n.dart';

import '../../injections.dart';

class OnBoardingScreen extends StatefulWidget {
  OnBoardingScreen({Key? key}) : super(key: key);

  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  final PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppStyle.whiteColor,
      body: Stack(
        children: [
          Column(
            children: [
              Expanded(
                flex: 6,
                child: PageView(
                    controller: pageController,
                    children: sl<HomesettingsBloc>()
                            .settings
                            ?.onboards
                            .map((e) => buildOnBoarding(e))
                            .toList() ??
                        []),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SmoothPageIndicator(
                    controller: pageController,
                    count:
                        sl<HomesettingsBloc>().settings?.onboards.length ?? 1,
                    effect: ExpandingDotsEffect(
                        dotHeight: 6,
                        dotWidth: 20,
                        expansionFactor: 2,
                        activeDotColor: AppStyle.secondaryColor,
                        dotColor: AppStyle.greyColor),
                  ),
                ),
              ),
              Expanded(flex: 2, child: Container())
            ],
          ),
          Positioned(
            top: SizeConfig.screenHeight - 200,
            left: SizeConfig.h(20),
            right: SizeConfig.h(20),
            child: Column(
              children: [
                InkWell(
                  onTap: () {
                    if ((pageController.page!) ==
                        (sl<HomesettingsBloc>().settings?.onboards.length ??
                                1) -
                            1)
                      Navigator.pushReplacementNamed(context, "/base");
                    else
                      pageController.animateToPage(
                          (pageController.page!.toInt()) + 1,
                          duration: Duration(milliseconds: 400),
                          curve: Curves.fastOutSlowIn);
                  },
                  child: Container(
                    height: SizeConfig.h(60),
                    width: SizeConfig.screenWidth - 50,
                    padding: EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(SizeConfig.h(100)),
                      gradient: LinearGradient(
                        colors: [
                          AppStyle.primaryColor,
                          AppStyle.secondaryColor,
                        ],
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                      ),
                    ),
                    child: Center(
                      child: Text(
                        S.of(context).next,
                        style: AppStyle.vexa14
                            .copyWith(color: AppStyle.whiteColor),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: SizeConfig.h(20)),
                InkWell(
                  onTap: () {
                    Navigator.pushReplacementNamed(context, "/base");
                  },
                  child: Container(
                    height: SizeConfig.h(60),
                    width: SizeConfig.screenWidth - 50,
                    padding: EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                    decoration: BoxDecoration(
                      color: AppStyle.backgroundColor,
                      border:
                          Border.all(width: 2.0, color: AppStyle.primaryColor),
                      borderRadius: BorderRadius.circular(SizeConfig.h(100)),
                    ),
                    child: Center(
                      child: Text(
                        S.of(context).skip,
                        style: AppStyle.vexa14.copyWith(
                          color: AppStyle.primaryColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildOnBoarding(Onboards onboard) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          flex: 6,
          child: Image.network(
            onboard.coverImage ?? "",
            height: SizeConfig.h(250),
            width: SizeConfig.h(250),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              onboard.title ?? "",
              style: AppStyle.vexa20
                  .copyWith(color: AppStyle.secondaryDark, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              onboard.description ?? "",
              style: AppStyle.vexa16
                  .copyWith(color: AppStyle.greyColor, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(child: Container()),
      ],
    );
  }
}
