import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:tajra/Ui/Auth/Welcome.dart';
import 'package:tajra/Ui/Auth/auth_const.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/data/repository/Repository.dart';
import 'package:vertical_weight_slider/vertical_weight_slider.dart';
import 'dart:math';
import './/injections.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/generated/l10n.dart';

class Create_account_Step_5Screen extends StatefulWidget {
  Create_account_Step_5Screen({Key? key}) : super(key: key);

  @override
  _Create_account_Step_5ScreenState createState() =>
      _Create_account_Step_5ScreenState();
}

int isSelected = 0;
double _dragPercentage = 30;
WeightSliderController _controller = WeightSliderController(
    initialWeight: _dragPercentage, minWeight: 0, interval: 0.1);
double _dragPercentage1 = 30;
WeightSliderController _controller1 = WeightSliderController(
    initialWeight: _dragPercentage, minWeight: 0, interval: 0.1);

class _Create_account_Step_5ScreenState
    extends State<Create_account_Step_5Screen> {
  final PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppStyle.whiteColor,
        body: Stack(
          children: [
            Column(
              children: [
                Expanded(flex: 6, child: buildOnBoarding()),
                Expanded(flex: 2, child: Container())
              ],
            ),
            Positioned(
              top: SizeConfig.screenHeight - 170,
              left: SizeConfig.w(40),
              right: SizeConfig.w(40),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => WelcomeScreen()),
                  );
                },
                child: Container(
                  height: SizeConfig.h(60),
                  width: SizeConfig.screenWidth - 50,
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(SizeConfig.h(100)),
                    gradient: LinearGradient(
                      colors: [
                        AppStyle.primaryColor,
                        AppStyle.secondaryColor,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      S.of(context).continuee,
                      style:
                          AppStyle.vexa14.copyWith(color: AppStyle.whiteColor),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildOnBoarding() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back)),
              Stack(children: [
                Container(
                  height: 5,
                  width: 140,
                  color: Colors.grey,
                ),
                Container(
                  height: 5,
                  width: 100,
                  color: AppStyle.primaryColor,
                ),
              ]),
              BlocConsumer(
                  bloc: sl<AuthBloc>(),
                  listener: (context, state) {
                    if (state is ErrorInLogin) {
                      AppSnackBar.show(context,
                          S.of(context).emailOrPasswordWrong, ToastType.Error);
                    }
                    if (state is ErrorSignUp) {
                      AppSnackBar.show(context, state.error, ToastType.Error);
                    }
                    if (state is LoginSuccess) {
                      sl<Repository>().setFcmToken();
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/base', (Route<dynamic> route) => false);
                    }
                    if (state is SignUpSuccess) {
                      sl<Repository>().setFcmToken();
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/base', (Route<dynamic> route) => false);
                    }
                  },
                  builder: (context, state) {
                    if (state is LoadingLogin || state is LoadingSignUp)
                      return Container(
                          width: 50,
                          child: Center(child: CircularProgressIndicator()));
                    return TextButton(
                        onPressed: () {
                          sl<AuthBloc>().add(SignUpEvent({
                            "name": nameAu,
                            "email": emailAu,
                            "country_code": country_codeAu,
                            "mobile": mobileAu,
                            "password": passwordAu,
                          }));
                        },
                        child: Text(
                          S.of(context).skip,
                          style: TextStyle(
                              color: AppStyle.primaryColor, fontSize: 16),
                        ));
                  }),
            ],
          ),
        )),
        Expanded(child: Container()),
        Expanded(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
              child: Column(
                children: [
                  Text(
                    S.of(context).Create_account_Step_4,
                    style: AppStyle.vexa20.copyWith(
                        color: AppStyle.secondaryDark, wordSpacing: 3),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    S.of(context).Create_account_Step_42,
                    style: AppStyle.vexa20.copyWith(
                        color: AppStyle.secondaryDark, wordSpacing: 3),
                    textAlign: TextAlign.center,
                  ),
                ],
              )),
        ),
        Expanded(child: Container()),
        Expanded(
          flex: 2,
          child: (Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(S.of(context).Create_account_Step_43),
              Wrap(
                children: [
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelected = 0;
                        blood_type = "A+";
                      });
                    },
                    child: Container(
                      margin:
                          Localizations.localeOf(context).languageCode == "ar"
                              ? EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  left: SizeConfig.h(10))
                              : EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  right: SizeConfig.h(10)),
                      padding: EdgeInsets.only(
                          top: SizeConfig.h(5),
                          bottom: SizeConfig.h(5),
                          left: SizeConfig.h(12),
                          right: SizeConfig.h(12)),
                      height: SizeConfig.h(50),
                      width: SizeConfig.w(70),
                      decoration: BoxDecoration(
                        boxShadow: [AppStyle.boxShadow3on6],
                        color: isSelected == 0
                            ? AppStyle.primaryColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Center(
                        child: Text("A+",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected == 0
                                  ? AppStyle.whiteColor
                                  : Colors.black,
                            )),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelected = 1;
                        blood_type = "A-";
                      });
                    },
                    child: Container(
                      margin:
                          Localizations.localeOf(context).languageCode == "ar"
                              ? EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  left: SizeConfig.h(10))
                              : EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  right: SizeConfig.h(10)),
                      padding: EdgeInsets.only(
                          top: SizeConfig.h(5),
                          bottom: SizeConfig.h(5),
                          left: SizeConfig.h(12),
                          right: SizeConfig.h(12)),
                      height: SizeConfig.h(50),
                      width: SizeConfig.w(70),
                      decoration: BoxDecoration(
                        boxShadow: [AppStyle.boxShadow3on6],
                        color: isSelected == 1
                            ? AppStyle.primaryColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Center(
                        child: Text("A-",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected == 1
                                  ? AppStyle.whiteColor
                                  : Colors.black,
                            )),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelected = 2;
                        blood_type = "B+";
                      });
                    },
                    child: Container(
                      margin:
                          Localizations.localeOf(context).languageCode == "ar"
                              ? EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  left: SizeConfig.h(10))
                              : EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  right: SizeConfig.h(10)),
                      padding: EdgeInsets.only(
                          top: SizeConfig.h(5),
                          bottom: SizeConfig.h(5),
                          left: SizeConfig.h(12),
                          right: SizeConfig.h(12)),
                      height: SizeConfig.h(50),
                      width: SizeConfig.w(70),
                      decoration: BoxDecoration(
                        boxShadow: [AppStyle.boxShadow3on6],
                        color: isSelected == 2
                            ? AppStyle.primaryColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Center(
                        child: Text("B+",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected == 2
                                  ? AppStyle.whiteColor
                                  : Colors.black,
                            )),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelected = 3;
                        blood_type = "B-";
                      });
                    },
                    child: Container(
                      margin:
                          Localizations.localeOf(context).languageCode == "ar"
                              ? EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  left: SizeConfig.h(10))
                              : EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  right: SizeConfig.h(10)),
                      padding: EdgeInsets.only(
                          top: SizeConfig.h(5),
                          bottom: SizeConfig.h(5),
                          left: SizeConfig.h(12),
                          right: SizeConfig.h(12)),
                      height: SizeConfig.h(50),
                      width: SizeConfig.w(70),
                      decoration: BoxDecoration(
                        boxShadow: [AppStyle.boxShadow3on6],
                        color: isSelected == 3
                            ? AppStyle.primaryColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Center(
                        child: Text("B-",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected == 3
                                  ? AppStyle.whiteColor
                                  : Colors.black,
                            )),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelected = 4;
                        blood_type = "O+";
                      });
                    },
                    child: Container(
                      margin:
                          Localizations.localeOf(context).languageCode == "ar"
                              ? EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  left: SizeConfig.h(10))
                              : EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  right: SizeConfig.h(10)),
                      padding: EdgeInsets.only(
                          top: SizeConfig.h(5),
                          bottom: SizeConfig.h(5),
                          left: SizeConfig.h(12),
                          right: SizeConfig.h(12)),
                      height: SizeConfig.h(50),
                      width: SizeConfig.w(70),
                      decoration: BoxDecoration(
                        boxShadow: [AppStyle.boxShadow3on6],
                        color: isSelected == 4
                            ? AppStyle.primaryColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Center(
                        child: Text("O+",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected == 4
                                  ? AppStyle.whiteColor
                                  : Colors.black,
                            )),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelected = 5;
                        blood_type = "O-";
                      });
                    },
                    child: Container(
                      margin:
                          Localizations.localeOf(context).languageCode == "ar"
                              ? EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  left: SizeConfig.h(10))
                              : EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  right: SizeConfig.h(10)),
                      padding: EdgeInsets.only(
                          top: SizeConfig.h(5),
                          bottom: SizeConfig.h(5),
                          left: SizeConfig.h(12),
                          right: SizeConfig.h(12)),
                      height: SizeConfig.h(50),
                      width: SizeConfig.w(70),
                      decoration: BoxDecoration(
                        boxShadow: [AppStyle.boxShadow3on6],
                        color: isSelected == 5
                            ? AppStyle.primaryColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Center(
                        child: Text("O-",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected == 5
                                  ? AppStyle.whiteColor
                                  : Colors.black,
                            )),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelected = 6;
                        blood_type = "AB+";
                      });
                    },
                    child: Container(
                      margin:
                          Localizations.localeOf(context).languageCode == "ar"
                              ? EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  left: SizeConfig.h(10))
                              : EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  right: SizeConfig.h(10)),
                      padding: EdgeInsets.only(
                          top: SizeConfig.h(5),
                          bottom: SizeConfig.h(5),
                          left: SizeConfig.h(12),
                          right: SizeConfig.h(12)),
                      height: SizeConfig.h(50),
                      width: SizeConfig.w(70),
                      decoration: BoxDecoration(
                        boxShadow: [AppStyle.boxShadow3on6],
                        color: isSelected == 6
                            ? AppStyle.primaryColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Center(
                        child: Text("AB+",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected == 6
                                  ? AppStyle.whiteColor
                                  : Colors.black,
                            )),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isSelected = 7;
                        blood_type = "AB-";
                      });
                    },
                    child: Container(
                      margin:
                          Localizations.localeOf(context).languageCode == "ar"
                              ? EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  left: SizeConfig.h(10))
                              : EdgeInsets.only(
                                  top: SizeConfig.h(10),
                                  bottom: SizeConfig.h(10),
                                  right: SizeConfig.h(10)),
                      padding: EdgeInsets.only(
                          top: SizeConfig.h(5),
                          bottom: SizeConfig.h(5),
                          left: SizeConfig.h(12),
                          right: SizeConfig.h(12)),
                      height: SizeConfig.h(50),
                      width: SizeConfig.w(70),
                      decoration: BoxDecoration(
                        boxShadow: [AppStyle.boxShadow3on6],
                        color: isSelected == 7
                            ? AppStyle.primaryColor
                            : Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Center(
                        child: Text("AB-",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected == 7
                                  ? AppStyle.whiteColor
                                  : Colors.black,
                            )),
                      ),
                    ),
                  ),
                ],
              )
            ],
          )),
        ),
      ],
    );
  }
}
