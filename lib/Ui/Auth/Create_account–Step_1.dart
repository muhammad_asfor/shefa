import 'package:flutter/material.dart';
import 'package:tajra/Ui/Auth/Create_account%E2%80%93Step_2.dart';
import 'package:tajra/Ui/Auth/auth_const.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/generated/l10n.dart';

class Create_account_Step_1Screen extends StatefulWidget {
  Create_account_Step_1Screen({Key? key}) : super(key: key);

  @override
  _Create_account_Step_1ScreenState createState() =>
      _Create_account_Step_1ScreenState();
}

class _Create_account_Step_1ScreenState
    extends State<Create_account_Step_1Screen> {
  final PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppStyle.whiteColor,
        body: Stack(
          children: [
            Column(
              children: [
                Expanded(flex: 6, child: buildOnBoarding()),
                Expanded(flex: 2, child: Container())
              ],
            ),
            Positioned(
              top: SizeConfig.screenHeight - 170,
              left: SizeConfig.w(40),
              right: SizeConfig.w(40),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Create_account_Step_2Screen()),
                  );
                },
                child: Container(
                  height: SizeConfig.h(60),
                  width: SizeConfig.screenWidth - 50,
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(SizeConfig.h(100)),
                    gradient: LinearGradient(
                      colors: [
                        AppStyle.primaryColor,
                        AppStyle.secondaryColor,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      S.of(context).continuee,
                      style:
                          AppStyle.vexa14.copyWith(color: AppStyle.whiteColor),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildOnBoarding() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(child: Container()),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              S.of(context).Create_account_Step_1,
              style: AppStyle.vexa20
                  .copyWith(color: AppStyle.secondaryDark, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              S.of(context).Create_account_Step_12,
              style: AppStyle.vexa16
                  .copyWith(color: AppStyle.greyColor, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(child: Container()),
        Expanded(
          flex: 3,
          child: InkWell(
            onTap: () {
              setState(() {
                gender = "male";
              });
            },
            child: Container(
              height: SizeConfig.h(250),
              width: SizeConfig.h(300),
              color: gender == "male" ? Colors.green : Colors.white,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Image.asset(
                  'assets/Create_account–Step_11.png',
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 3,
          child: InkWell(
            onTap: () {
              setState(() {
                gender = "female";
              });
            },
            child: Container(
              height: SizeConfig.h(250),
              width: SizeConfig.h(300),
              color: gender != "male" ? Colors.green : Colors.white,
              child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Image.asset(
                  'assets/Create_account–Step_12.png',
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
