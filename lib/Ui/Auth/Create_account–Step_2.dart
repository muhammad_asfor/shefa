import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:tajra/Ui/Auth/Create_account%E2%80%93Step_3.dart';
import 'package:tajra/Ui/Auth/auth_const.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/data/repository/Repository.dart';
import 'package:vertical_weight_slider/vertical_weight_slider.dart';
import 'dart:math';
import './/injections.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/generated/l10n.dart';

class Create_account_Step_2Screen extends StatefulWidget {
  Create_account_Step_2Screen({Key? key}) : super(key: key);

  @override
  _Create_account_Step_2ScreenState createState() =>
      _Create_account_Step_2ScreenState();
}

WeightSliderController _controller =
    WeightSliderController(initialWeight: old, minWeight: 0, interval: 0.1);

class _Create_account_Step_2ScreenState
    extends State<Create_account_Step_2Screen> {
  final PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppStyle.whiteColor,
        body: Stack(
          children: [
            Column(
              children: [
                Expanded(flex: 6, child: buildOnBoarding()),
                Expanded(flex: 2, child: Container())
              ],
            ),
            Positioned(
              top: SizeConfig.screenHeight - 170,
              left: SizeConfig.w(40),
              right: SizeConfig.w(40),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Create_account_Step_4Screen()),
                  );
                },
                child: Container(
                  height: SizeConfig.h(60),
                  width: SizeConfig.screenWidth - 50,
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(SizeConfig.h(100)),
                    gradient: LinearGradient(
                      colors: [
                        AppStyle.primaryColor,
                        AppStyle.secondaryColor,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      S.of(context).continuee,
                      style:
                          AppStyle.vexa14.copyWith(color: AppStyle.whiteColor),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildOnBoarding() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back)),
              Stack(children: [
                Container(
                  height: 5,
                  width: 140,
                  color: Colors.grey,
                ),
                Container(
                  height: 5,
                  width: 40,
                  color: AppStyle.primaryColor,
                ),
              ]),
              BlocConsumer(
                  bloc: sl<AuthBloc>(),
                  listener: (context, state) {
                    if (state is ErrorInLogin) {
                      AppSnackBar.show(context,
                          S.of(context).emailOrPasswordWrong, ToastType.Error);
                    }
                    if (state is ErrorSignUp) {
                      AppSnackBar.show(context, state.error, ToastType.Error);
                    }
                    if (state is LoginSuccess) {
                      sl<Repository>().setFcmToken();
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/base', (Route<dynamic> route) => false);
                    }
                    if (state is SignUpSuccess) {
                      sl<Repository>().setFcmToken();
                      Navigator.pushNamedAndRemoveUntil(
                          context, '/base', (Route<dynamic> route) => false);
                    }
                  },
                  builder: (context, state) {
                    if (state is LoadingLogin || state is LoadingSignUp)
                      return Container(
                          width: 50,
                          child: Center(child: CircularProgressIndicator()));
                    return TextButton(
                        onPressed: () {
                          sl<AuthBloc>().add(SignUpEvent({
                            "name": nameAu,
                            "email": emailAu,
                            "country_code": country_codeAu,
                            "mobile": mobileAu,
                            "password": passwordAu,
                          }));
                        },
                        child: Text(
                          S.of(context).skip,
                          style: TextStyle(
                              color: AppStyle.primaryColor, fontSize: 16),
                        ));
                  }),
            ],
          ),
        )),
        Expanded(child: Container()),
        Expanded(
          flex: 6,
          child: Image.asset(
            'assets/Create_account–Step_2.png',
            height: SizeConfig.h(250),
            width: SizeConfig.h(300),
          ),
        ),
        Expanded(child: Container()),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              S.of(context).Create_account_Step_2,
              style: AppStyle.vexa20
                  .copyWith(color: AppStyle.secondaryDark, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(child: Container()),
        Expanded(
            child: Container(
          child: Center(
            child: Text(
              "$old",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: AppStyle.primaryColor),
            ),
          ),
        )),
        Expanded(
            flex: 4,
            child: Transform.rotate(
                angle: 90 / 180 * pi,
                child: Container(
                  child: VerticalWeightSlider(
                    controller: _controller,
                    decoration: const PointerDecoration(
                      width: 100.0,
                      height: 3.0,
                      largeColor: Color(0xFF898989),
                      mediumColor: Color(0xFFC5C5C5),
                      smallColor: Color(0xFFF0F0F0),
                      gap: 30.0,
                    ),
                    onChanged: (double value) {
                      setState(() {
                        old = value;
                      });
                    },
                    indicator: Container(
                      height: 3.0,
                      width: 130.0,
                      alignment: Alignment.centerLeft,
                      color: AppStyle.primaryColor,
                    ),
                  ),
                ))),
      ],
    );
  }
}
