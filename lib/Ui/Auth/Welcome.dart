import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';

import 'package:progiom_cms/homeSettings.dart';

import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:tajra/Ui/Auth/auth_const.dart';
import 'package:tajra/Utils/AppSnackBar.dart';
import 'package:tajra/data/repository/Repository.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/generated/l10n.dart';
import './/injections.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WelcomeScreen extends StatefulWidget {
  WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  final PageController pageController = PageController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppStyle.whiteColor,
        body: Stack(
          children: [
            Column(
              children: [
                Expanded(flex: 6, child: buildOnBoarding()),
                Expanded(flex: 2, child: Container())
              ],
            ),
            Positioned(
              top: SizeConfig.screenHeight - 170,
              left: SizeConfig.h(20),
              right: SizeConfig.h(20),
              child: InkWell(
                onTap: () {
                  sl<AuthBloc>().add(SignUpEvent({
                    "name": nameAu,
                    "email": emailAu,
                    "country_code": country_codeAu,
                    "mobile": mobileAu,
                    "password": passwordAu,
                    "birth_date": old.toString(),
                    "weight": dragPercentage.toString(),
                    "height": dragPercentage1.toString(),
                    "blood_type": blood_type,
                    "gender": gender
                  }));
                },
                child: Container(
                  height: SizeConfig.h(60),
                  width: SizeConfig.screenWidth - 50,
                  padding: EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(SizeConfig.h(100)),
                    gradient: LinearGradient(
                      colors: [
                        AppStyle.primaryColor,
                        AppStyle.secondaryColor,
                      ],
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                    ),
                  ),
                  child: BlocConsumer(
                      bloc: sl<AuthBloc>(),
                      listener: (context, state) {
                        if (state is ErrorInLogin) {
                          AppSnackBar.show(
                              context,
                              S.of(context).emailOrPasswordWrong,
                              ToastType.Error);
                        }
                        if (state is ErrorSignUp) {
                          AppSnackBar.show(
                              context, state.error, ToastType.Error);
                        }
                        if (state is LoginSuccess) {
                          sl<Repository>().setFcmToken();
                          Navigator.pushNamedAndRemoveUntil(context, '/base',
                              (Route<dynamic> route) => false);
                        }
                        if (state is SignUpSuccess) {
                          sl<Repository>().setFcmToken();
                          Navigator.pushNamedAndRemoveUntil(context, '/base',
                              (Route<dynamic> route) => false);
                        }
                      },
                      builder: (context, state) {
                        if (state is LoadingLogin || state is LoadingSignUp)
                          return Container(
                              width: 50,
                              child:
                                  Center(child: CircularProgressIndicator()));
                        return Center(
                          child: Text(
                            S.of(context).startNow,
                            style: AppStyle.vexa14
                                .copyWith(color: AppStyle.whiteColor),
                          ),
                        );
                      }),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildOnBoarding() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(child: Container()),
        Expanded(
          flex: 6,
          child: Image.asset(
            'assets/welcome2.png',
            height: SizeConfig.h(250),
            width: SizeConfig.h(250),
          ),
        ),
        Expanded(child: Container()),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              S.of(context).welcome3,
              style: AppStyle.vexa20
                  .copyWith(color: AppStyle.secondaryDark, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
            child: Text(
              S.of(context).welcome4,
              style: AppStyle.vexa16
                  .copyWith(color: AppStyle.greyColor, wordSpacing: 3),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ],
    );
  }
}
