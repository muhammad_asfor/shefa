import 'package:flutter/material.dart';

import 'package:progiom_cms/auth.dart';
import '/App/Widgets/MainButton.dart';
import '/Ui/Auth/forgotPassword/ChangePasswordPage.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';

import '../../../injections.dart';

// class CodePage extends StatefulWidget {
//   final String email;
//   CodePage({required this.email, Key? key}) : super(key: key);

//   @override
//   _CodePageState createState() => _CodePageState(email);
// }

// class _CodePageState extends State<CodePage> {
//   _CodePageState(this.email);
//   String? code;
//   String email;
//   bool isLoading = false;
//   bool emailNotVerified = true;
//   @override
//   void initState() {
//     if (email != null) {
//       emailNotVerified = false;
//     }
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.transparent,
//         elevation: 0.0,
//         leading: IconButton(
//           icon: Icon(
//             Icons.arrow_back,
//             color: Colors.black,
//           ),
//           onPressed: () {
//             Navigator.pop(context);
//             Navigator.pop(context);
//           },
//         ),
//       ),
//       body: Stack(
//         children: [
//           Positioned(
//             top: SizeConfig.h(10),
//             left: SizeConfig.w(50),
//             right: SizeConfig.w(50),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: [
//                 Image.asset("assets/lock.png"),
//                 Text(S.of(context).change_password,
//                     textAlign: TextAlign.center,
//                     style: AppStyle.vexa20.copyWith(fontWeight: FontWeight.bold)),
//                 SizedBox(
//                   height: SizeConfig.h(27),
//                 ),
//                 Text(
//                   S.of(context).enter_code,
//                   textAlign: TextAlign.center,
//                   style: AppStyle.vexa12,
//                 ),
//                 SizedBox(
//                   height: SizeConfig.h(53),
//                 ),
//                 Container(
//                     width: SizeConfig.w(315),
//                     child: Column(children: [
//                       Row(
//                         children: [
//                           Expanded(
//                               child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Text(
//                                 S.of(context).e_mail,
//                                 style: AppStyle.vexa12
//                                     .copyWith(color: AppStyle.primaryColor),
//                               ),
//                               TextFormField(
//                                 readOnly: true,
//                                 onChanged: (v) {
//                                   setState(() {
//                                     if ((RegExp(
//                                             r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
//                                         .hasMatch(v))) {
//                                       email = v;
//                                       emailNotVerified = false;
//                                     } else {
//                                       emailNotVerified = true;
//                                     }
//                                   });
//                                 },
//                                 initialValue: email,
//                                 decoration: InputDecoration(
//                                     hintText: "user@email.com",
//                                     suffixIcon: Icon(
//                                       Icons.check_circle,
//                                       size: SizeConfig.h(28),
//                                       color: emailNotVerified
//                                           ? Colors.white12
//                                           : AppStyle.primaryColor,
//                                     )),
//                               ),
//                             ],
//                           ))
//                         ],
//                       ),
//                     ])),
//                 SizedBox(
//                   height: SizeConfig.h(27),
//                 ),
//                 Container(
//                     width: SizeConfig.w(315),
//                     child: Column(children: [
//                       Row(
//                         children: [
//                           Expanded(
//                               child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: [
//                               Text(
//                                 S.of(context).code,
//                                 style: AppStyle.vexa12
//                                     .copyWith(color: AppStyle.primaryColor),
//                               ),
//                               TextFormField(
//                                 autofocus: true,
//                                 onChanged: (value) {
//                                   setState(() {
//                                     code = value;
//                                   });
//                                 },
//                               ),
//                             ],
//                           ))
//                         ],
//                       ),
//                       SizedBox(
//                         height: SizeConfig.h(40),
//                       ),
//                     ])),
//                 SizedBox(
//                   height: SizeConfig.h(42),
//                 ),
//                 Container(
//                   width: SizeConfig.w(315),
//                   child: isLoading
//                       ? Center(
//                           child: CircularProgressIndicator(),
//                         )
//                       : Row(
//                           children: [
//                             Expanded(
//                                 child: MainButton(
//                                     onTap: () {
//                                       if (!emailNotVerified &&
//                                           (code?.isNotEmpty ?? false))
//                                         validateCode();
//                                     },
//                                     isOutlined: false,
//                                     child: Center(
//                                       child: Text(S.of(context).continuee, style: AppStyle.vexa14.copyWith(
//                                             color: Colors.white,
//                                             fontWeight: FontWeight.bold),),
//                                     )))
//                           ],
//                         ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   validateCode() async {
//     setState(() {
//       isLoading = true;
//     });
//     final result = await ValidateResetPassword(sl()).call(
//         ValidateResetPasswordParams(email: email, resetCoode: code ?? ""));
//     setState(() {
//       isLoading = false;
//     });
//     result.fold((l) {
//       AppSnackBar.show(context, l.errorMessage, ToastType.Error);
//     }, (r) {
//       Navigator.pushReplacement(context,
//           MaterialPageRoute(builder: (_) => ChangePasswordPage(code!, email)));
//     });
//   }
// }

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/Utils/SizeConfig.dart';
import 'package:tajra/Utils/Style.dart';

class CodePage extends StatefulWidget {
  final String email;
  CodePage({required this.email, Key? key}) : super(key: key);
  @override
  _CodePageState createState() => _CodePageState();
}

class _CodePageState extends State<CodePage> {
  String pinCode = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.h(100),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  "assets/pin.svg",
                  height: SizeConfig.h(64),
                  width: SizeConfig.h(64),
                )
              ],
            ),
            SizedBox(
              height: SizeConfig.h(48),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(60)),
              child: Text(
                S.of(context).Verify_Code,
                textAlign: TextAlign.center,
                // style: AppStyle.textTheme.bodyText2!
                //     .copyWith(fontWeight: FontWeight.w700),
              ),
            ),
            SizedBox(
              height: SizeConfig.h(5),
            ),
            const Spacer(),
            buildDots(),
            SizedBox(
              height: SizeConfig.h(20),
            ),
            buildPad(context)
          ],
        ),
      ),
    );
  }

  Container buildDots() {
    return Container(
      height: SizeConfig.h(48),
      decoration: BoxDecoration(
          border: pinCode.isNotEmpty
              ? Border.all(color: AppStyle.primaryColor, width: 2)
              : null,
          color: AppStyle.whiteColor,
          borderRadius: BorderRadius.circular(8)),
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: SizeConfig.h(12),
            height: SizeConfig.h(12),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: pinCode.isNotEmpty
                    ? AppStyle.primaryColor
                    : AppStyle.greyColor),
          ),
          SizedBox(
            width: SizeConfig.h(14),
          ),
          Container(
            width: SizeConfig.h(12),
            height: SizeConfig.h(12),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: pinCode.length >= 2
                    ? AppStyle.primaryColor
                    : AppStyle.greyColor),
          ),
          SizedBox(
            width: SizeConfig.h(14),
          ),
          Container(
            width: SizeConfig.h(12),
            height: SizeConfig.h(12),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: pinCode.length >= 3
                    ? AppStyle.primaryColor
                    : AppStyle.greyColor),
          ),
          SizedBox(
            width: SizeConfig.h(14),
          ),
          Container(
            width: SizeConfig.h(12),
            height: SizeConfig.h(12),
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: pinCode.length == 4
                    ? AppStyle.primaryColor
                    : AppStyle.greyColor),
          ),
        ],
      ),
    );
  }

  SizedBox buildPad(BuildContext context) {
    return SizedBox(
      width: SizeConfig.screenWidth,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [buildNumber("1"), buildNumber("2"), buildNumber("3")],
          ),
          SizedBox(
            height: SizeConfig.h(20),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [buildNumber("4"), buildNumber("5"), buildNumber("6")],
          ),
          SizedBox(
            height: SizeConfig.h(20),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [buildNumber("7"), buildNumber("8"), buildNumber("9")],
          ),
          SizedBox(
            height: SizeConfig.h(20),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: SizeConfig.h(75),
              ),
              buildNumber("0"),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: () {
                        if (pinCode.isNotEmpty) {
                          setState(() {
                            pinCode = pinCode.substring(0, pinCode.length - 1);
                          });
                        }
                      },
                      child: Text(
                        S.of(context).delete,
                        // style: AppStyle.textTheme.bodyText2!.copyWith(
                        //     fontWeight: FontWeight.w700,
                        //     color: AppColors.primaryColor.shade300),
                      ))
                ],
              ),
            ],
          )
        ],
      ),
    );
  }

  final Debouncer debouncer = Debouncer(milliseconds: 400);

  Widget buildNumber(String number) {
    return SizedBox(
      height: SizeConfig.h(75),
      width: SizeConfig.h(75),
      child: Material(
        child: InkWell(
          onTap: () {
            if (pinCode.length == 3) {
              setState(() {
                pinCode += number;
              });
              debouncer.run(() {
                validateCode();
                // Navigator.pushNamed(context, "/confirm_pin", arguments: pinCode)
                //     .then((value) {
                //   setState(() {
                //     pinCode = "";
                //   });
                // });
              });
            } else if (pinCode.length < 4) {
              setState(() {
                pinCode += number;
              });
            }
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                number,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: SizeConfig.h(40), fontWeight: FontWeight.w200),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _fetchData(BuildContext context) async {
    // show the loading dialog
    showDialog(
        // The user CANNOT close this dialog  by pressing outsite it
        barrierDismissible: false,
        context: context,
        builder: (_) {
          return Dialog(
            // The background color
            backgroundColor: Colors.white,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const [
                  // The loading indicator
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 15,
                  ),
                  // Some text
                  Text('Loading...')
                ],
              ),
            ),
          );
        });
  }

  validateCode() async {
    setState(() {
      _fetchData(context);
    });
    final result = await ValidateResetPassword(sl()).call(
        ValidateResetPasswordParams(email: widget.email, resetCoode: pinCode));
    setState(() {
      Navigator.of(context).pop();
    });
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (_) => ChangePasswordPage(pinCode, widget.email)));
    });
  }
}
