import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import '/App/Widgets/MainButton.dart';
import '/Ui/Auth/forgotPassword/CodePage.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';

import '../../../injections.dart';

class ForgotPassword extends StatefulWidget {
  ForgotPassword({Key? key}) : super(key: key);

  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  bool isLoading = false;
  bool emailNotVerified = true;
  final TextEditingController emailController = TextEditingController();
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: ListView(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: SizeConfig.screenWidth,
                  height: SizeConfig.h(250),
                  child: Image.asset(
                    "assets/login2.png",
                    width: SizeConfig.w(867.71),
                    height: SizeConfig.h(608),
                    fit: BoxFit.cover,
                    // color: AppStyle.secondaryColor,
                  ),
                ),
                SizedBox(
                  height: SizeConfig.h(100),
                ),
                Text(
                  S.of(context).change_password,
                  textAlign: TextAlign.center,
                  style: AppStyle.vexa20.copyWith(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: SizeConfig.h(53),
                ),
                Container(
                    width: SizeConfig.w(315),
                    child: Column(children: [
                      Row(
                        children: [
                          Expanded(
                              child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              buildTextField(
                                  S.of(context).e_mail,
                                  Icons.email_outlined,
                                  emailController, validator: (v) {
                                if (v != null) {
                                  if (!validEmail(v) && v != '') {
                                    return S.of(context).emailValidator;
                                  }
                                }
                                return null;
                              }),
                            ],
                          ))
                        ],
                      ),
                    ])),
                SizedBox(
                  height: SizeConfig.h(42),
                ),
                SizedBox(
                  height: SizeConfig.h(30),
                ),
                isLoading
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : InkWell(
                        onTap: () {
                          if (formKey.currentState?.validate() ?? false) {
                            if (emailController.text != '') resetPassword();
                          }
                        },
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.circular(SizeConfig.h(100)),
                            gradient: LinearGradient(
                              colors: [
                                AppStyle.primaryColor,
                                AppStyle.secondaryColor,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                          width: SizeConfig.w(315),
                          child: Center(
                            child: Text(
                              S.of(context).continuee,
                              style: AppStyle.vexa14.copyWith(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  resetPassword() async {
    setState(() {
      isLoading = true;
    });
    final result = await RequestResetPassword(sl())
        .call(RequestResetPasswordParams(email: emailController.text));
    setState(() {
      isLoading = false;
    });
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
              builder: (_) => CodePage(
                    email: emailController.text,
                  )));
    });
  }

  Widget buildTextField(
      String label, IconData icon, TextEditingController controller,
      {String? Function(String?)? validator, bool? obscureText}) {
    return SizedBox(
      // height: SizeConfig.h(50),
      child: TextFormField(
        validator: validator,
        controller: controller,
        obscureText: obscureText ?? false,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(
              vertical: SizeConfig.h(2),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.primaryColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(100),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.disabledColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(100),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.primaryColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(100),
              ),
            ),
            prefixIcon: Icon(
              icon,
            ),
            labelText: label,
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            labelStyle: TextStyle(
              fontSize: SizeConfig.h(14),
            ),
            errorStyle: TextStyle(fontSize: SizeConfig.h(14)),
            fillColor: Colors.white70),
      ),
    );
  }
}
