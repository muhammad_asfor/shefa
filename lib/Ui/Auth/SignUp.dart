import 'dart:io';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:intl_phone_field/phone_number.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:tajra/Ui/Auth/Create_account%E2%80%93Step_1.dart';
import 'package:tajra/Ui/Auth/auth_const.dart';
import './/Utils/AppSnackBar.dart';
import './/Utils/SizeConfig.dart';
import './/Utils/Style.dart';
import './/data/repository/Repository.dart';
import './/generated/l10n.dart';
import './/injections.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'forgotPassword/ForgotPassword.dart';

class CreateAcPage extends StatefulWidget {
  CreateAcPage({Key? key}) : super(key: key);

  @override
  _CreateAcPageState createState() => _CreateAcPageState();
}

class _CreateAcPageState extends State<CreateAcPage>
    with SingleTickerProviderStateMixin {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  bool isSignUp = false;
  late TabController _controller;
  final TextEditingController emailController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  String? countryCode = '';
  String? mobile = '';

  @override
  void initState() {
    _controller = TabController(length: 2, vsync: this);
    tapGestureRecognizer = TapGestureRecognizer()..onTap = _goToRegister;

    super.initState();
    _controller.addListener(() {
      if (_controller.indexIsChanging) {
        if (_controller.index == 0) {
          setState(() {
            isSignUp = false;
          });
        } else {
          setState(() {
            isSignUp = true;
          });
        }
      }
    });
  }

  TapGestureRecognizer? tapGestureRecognizer;

  void _goToRegister() {
    Navigator.popAndPushNamed(context, '/login');
  }

  @override
  void dispose() {
    tapGestureRecognizer?.dispose();

    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: formKey,
        child: ListView(
          children: [
            SizedBox(
              width: SizeConfig.screenWidth,
              height: SizeConfig.h(200),
              child: Image.asset(
                "assets/login2.png",
                width: SizeConfig.w(867.71),
                height: SizeConfig.h(608),
                fit: BoxFit.cover,
                // color: AppStyle.secondaryColor,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(24)),
              child: SizedBox(
                height: SizeConfig.h(700),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: RichText(
                            text: TextSpan(
                                text: "" + "\n",
                                style: AppStyle.welcome20.copyWith(
                                    fontFamily: "Almaria",
                                    color: AppStyle.primaryColor),
                                children: [
                                  TextSpan(
                                      text: S.of(context).create_account,
                                      style: TextStyle(
                                          fontSize: 30,
                                          color: AppStyle.grediantLightColor))
                                ]),
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: SizeConfig.h(35),
                    ),
                    buildSignUpDialoge(),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: GestureDetector(
                        onTap: () {
                          FocusScope.of(context).unfocus();
                          if (formKey.currentState?.validate() ?? false) {
                            nameAu = nameController.text;
                            emailAu = emailController.text == ''
                                ? ""
                                : emailController.text;
                            country_codeAu = countryCode!;
                            mobileAu = mobile!;
                            passwordAu = passwordController.text;
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      Create_account_Step_1Screen()),
                            );
                            // sl<AuthBloc>().add(SignUpEvent({
                            //   "name": nameController.text,
                            //   "email": emailController.text == ''
                            //       ? null
                            //       : emailController.text,
                            //   "country_code": countryCode,
                            //   "mobile": mobile,
                            //   "password": passwordController.text,
                            // }));
                          }
                        },
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(vertical: SizeConfig.h(20)),
                          decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.circular(SizeConfig.h(100)),
                            gradient: LinearGradient(
                              colors: [
                                AppStyle.primaryColor,
                                AppStyle.secondaryColor,
                              ],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                          height: SizeConfig.h(60),
                          width: SizeConfig.screenWidth,
                          child: BlocConsumer(
                              bloc: sl<AuthBloc>(),
                              listener: (context, state) {
                                if (state is ErrorInLogin) {
                                  AppSnackBar.show(
                                      context,
                                      S.of(context).emailOrPasswordWrong,
                                      ToastType.Error);
                                }
                                if (state is ErrorSignUp) {
                                  AppSnackBar.show(
                                      context, state.error, ToastType.Error);
                                }
                                if (state is LoginSuccess) {
                                  sl<Repository>().setFcmToken();
                                  Navigator.pushReplacementNamed(
                                      context, '/base');
                                }
                                if (state is SignUpSuccess) {
                                  sl<Repository>().setFcmToken();
                                  Navigator.pushReplacementNamed(
                                      context, '/base');
                                }
                              },
                              builder: (context, state) {
                                if (state is LoadingLogin ||
                                    state is LoadingSignUp)
                                  return Container(
                                      width: 50,
                                      child: Center(
                                          child: CircularProgressIndicator()));
                                return Container(
                                    height: SizeConfig.h(54),
                                    width: SizeConfig.h(54),
                                    child: Container(
                                      height: SizeConfig.h(60),
                                      width: SizeConfig.screenWidth - 50,
                                      child: Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              S.of(context).continuee,
                                              style: AppStyle.vexa14.copyWith(
                                                  color: AppStyle.whiteColor),
                                            )
                                          ],
                                        ),
                                      ),
                                    ));
                              }),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(40),
                    ),
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        _goToRegister();
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(6.7),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            RichText(
                                text: TextSpan(
                                    text: S.of(context).Do_you_have_account,
                                    style: TextStyle(
                                            color: AppStyle.secondaryColor,
                                            fontSize: SizeConfig.h(12))
                                        .copyWith(fontFamily: "Almaria"),
                                    children: [
                                  TextSpan(
                                      text: " " + S.of(context).sign_in + " ",
                                      style: TextStyle(
                                              color: AppStyle.primaryColor,
                                              fontWeight: FontWeight.bold)
                                          .copyWith(fontFamily: "Almaria"),
                                      recognizer: tapGestureRecognizer),
                                ])),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(25),
                    ),
                    buildSocialLogin(context),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Column buildSocialLogin(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Expanded(
                child: Container(
              height: 0,
              color: AppStyle.secondaryColor,
            )),
            Expanded(
                flex: 2,
                child: Center(
                    child: Text(
                  S.of(context).sign_in_with,
                  style:
                      AppStyle.vexa12.copyWith(color: AppStyle.secondaryColor),
                ))),
            Expanded(
                child: Container(
              height: 0,
              color: AppStyle.secondaryColor,
            )),
          ],
        ),
        SizedBox(
          height: SizeConfig.h(15),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(),
            SizedBox(),
            GestureDetector(
              onTap: () async {
                final result = await sl<Repository>().getFacebookToken();
                result.fold((l) {
                  AppSnackBar.show(context, l.errorMessage, ToastType.Error);
                }, (r) {
                  sl<AuthBloc>().add(LoginSocialEvent(
                      provider: SocialLoginProvider.face, token: r));
                });
              },
              child: Container(
                height: SizeConfig.h(54),
                width: SizeConfig.h(54),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  border: Border.all(color: Colors.black26),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(0.0, 5),
                        blurRadius: 5)
                  ],
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/face.svg",
                    height: SizeConfig.h(22),
                    width: SizeConfig.h(22),
                  ),
                ),
              ),
            ),
            // Container(
            //   height: SizeConfig.h(54),
            //   width: SizeConfig.h(54),
            //   decoration: BoxDecoration(
            //     shape: BoxShape.circle,
            //     color: Colors.white,
            //     border: Border.all(color: Colors.black26),
            //     boxShadow: [
            //       BoxShadow(
            //           color: Colors.black12,
            //           offset: Offset(0.0, 5),
            //           blurRadius: 5)
            //     ],
            //   ),
            //   child: Center(
            //     child: SvgPicture.asset(
            //       "assets/twitter.svg",
            //       height: SizeConfig.h(22),
            //       width: SizeConfig.h(22),
            //     ),
            //   ),
            // ),
            GestureDetector(
              onTap: () async {
                final result = await sl<Repository>().getGoogleToken();
                result.fold((l) {
                  AppSnackBar.show(context, l.errorMessage, ToastType.Error);
                }, (r) {
                  sl<AuthBloc>().add(LoginSocialEvent(
                      provider: SocialLoginProvider.google, token: r));
                });
              },
              child: Container(
                height: SizeConfig.h(54),
                width: SizeConfig.h(54),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  border: Border.all(color: Colors.black26),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black12,
                        offset: Offset(0.0, 5),
                        blurRadius: 5)
                  ],
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/googleIcon.svg",
                    height: SizeConfig.h(22),
                    width: SizeConfig.h(22),
                  ),
                ),
              ),
            ),
            SizedBox(),
            SizedBox(),
          ],
        ),
        if (Platform.isIOS) AppleButton(),
      ],
    );
  }

  Column buildLoginDialoge() {
    return Column(
      children: [
        SizedBox(
          height: SizeConfig.h(4),
        ),
        buildTextField(
            S.of(context).e_mail + ' / ' + S.of(context).phone_number,
            Icons.person_outline,
            emailController, validator: (v) {
          if (v != null) {
            if (v.contains(new RegExp(r'[A-Za-z]'))) {
              if (!validEmail(v)) {
                return S.of(context).emailValidator;
              }
            } else {
              if (!validPhoneNumber(v)) {
                return S.of(context).mobileValidator;
              }
            }
          }
          return null;
        }),
        SizedBox(
          height: SizeConfig.h(15),
        ),
        buildTextField(
            S.of(context).password, Icons.lock_outline, passwordController,
            validator: (v) {
          if (v != null && !validPassword(v))
            return S.of(context).passwordValidator;
          return null;
        }, obscureText: true),
        SizedBox(
          height: SizeConfig.h(15),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (_) => ForgotPassword()));
              },
              child: Text(S.of(context).forget_password,
                  style: TextStyle(
                      fontSize: SizeConfig.h(12),
                      color: AppStyle.secondaryColor,
                      decoration: TextDecoration.underline)),
            )
          ],
        )
      ],
    );
  }

  Widget buildSignUpDialoge() {
    return Column(
      children: [
        SizedBox(
          height: SizeConfig.h(4),
        ),
        buildTextField(
            S.of(context).fullName, Icons.person_outline, nameController,
            validator: (v) {
          if (v != null && v.isEmpty) return S.of(context).nameRequired;
          return null;
        }),
        SizedBox(
          height: SizeConfig.h(15),
        ),
        buildTextField(
            S.of(context).e_mail, Icons.email_outlined, emailController,
            validator: (v) {
          if (v != null) {
            if (!validEmail(v) && v != '') {
              return S.of(context).emailValidator;
            }
          }
          return null;
        }),
        SizedBox(
          height: SizeConfig.h(15),
        ),
        Directionality(
          textDirection: TextDirection.ltr,
          child: IntlPhoneField(
            searchText: S.of(context).searchHere,
            initialCountryCode:
                WidgetsBinding.instance!.window.locale.countryCode,
            dropdownTextStyle: TextStyle(fontSize: SizeConfig.w(12)),
            style: TextStyle(fontSize: SizeConfig.w(12)),
            showCountryFlag: countryCode == '+963' ? false : true,
            onCountryChanged: (c) {
              setState(() {
                countryCode = '+' + c.dialCode;
              });
            },
            onChanged: (PhoneNumber phoneNumber) {
              setState(() {
                countryCode = phoneNumber.countryCode;
                mobile = '0' + phoneNumber.number;
              });
            },
            controller: phoneController,
            invalidNumberMessage: S.of(context).mobileValidator,
            // disableLengthCheck: true,
            // validator: (v) {
            //   if (v != null) {
            //     if (!validPhoneNumber(mobile)) {
            //       return S.of(context).mobileValidator;
            //     }
            //   }
            //   return null;
            // },
            decoration: InputDecoration(
                hintTextDirection: TextDirection.rtl,
                alignLabelWithHint: true,
                suffixIcon: Icon(Icons.phone_android),
                labelText: S.of(context).phone_number,
                contentPadding: EdgeInsets.symmetric(
                  // vertical: SizeConfig.h(2),
                  horizontal: SizeConfig.w(10),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 1,
                      style: BorderStyle.solid,
                      color: AppStyle.primaryColor),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(100),
                  ),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 1,
                      style: BorderStyle.solid,
                      color: AppStyle.disabledColor),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(100),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      width: 1,
                      style: BorderStyle.solid,
                      color: AppStyle.primaryColor),
                  borderRadius: const BorderRadius.all(
                    const Radius.circular(100),
                  ),
                ),
                floatingLabelBehavior: FloatingLabelBehavior.auto,
                labelStyle: TextStyle(
                  fontSize: SizeConfig.h(14),
                ),
                errorStyle: TextStyle(fontSize: SizeConfig.h(14)),
                fillColor: Colors.white70),
          ),
        ),
        SizedBox(
          height: SizeConfig.h(15),
        ),
        buildTextField(
            S.of(context).password, Icons.lock_outline, passwordController,
            validator: (v) {
          if (v != null && !validPassword(v))
            return S.of(context).passwordValidator;
          return null;
        }, obscureText: true),
        SizedBox(
          height: SizeConfig.h(15),
        ),
      ],
    );
  }

  Widget buildTextField(
      String label, IconData icon, TextEditingController controller,
      {String? Function(String?)? validator, bool? obscureText}) {
    return SizedBox(
      // height: SizeConfig.h(50),
      child: TextFormField(
        validator: validator,
        controller: controller,
        obscureText: obscureText ?? false,
        decoration: InputDecoration(
            contentPadding: EdgeInsets.symmetric(
              vertical: SizeConfig.h(2),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.primaryColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(100),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.disabledColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(100),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.primaryColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(100),
              ),
            ),
            prefixIcon: Icon(
              icon,
            ),
            labelText: label,
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            labelStyle: TextStyle(
              fontSize: SizeConfig.h(14),
            ),
            errorStyle: TextStyle(fontSize: SizeConfig.h(14)),
            fillColor: Colors.white70),
      ),
    );
  }
}

class AppleButton extends StatelessWidget {
  const AppleButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        final result = await sl<Repository>().logInByApple();
        result.fold((l) {
          AppSnackBar.show(context, l.errorMessage, ToastType.Error);
        }, (r) {
          sl<AuthBloc>().add(
              LoginSocialEvent(provider: SocialLoginProvider.apple, token: r));
        });
      },
      child: Container(
        width: SizeConfig.screenWidth * 0.6,
        height: SizeConfig.h(50),
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  offset: Offset(0.0, 3.0),
                  blurRadius: 6,
                  color: Colors.black.withOpacity(0.2)),
            ],
            borderRadius: BorderRadius.circular(SizeConfig.h(28))),
        child: Row(
          children: [
            Row(
              children: [
                SizedBox(
                  width: SizeConfig.h(17),
                ),
                Icon(FontAwesomeIcons.apple)
              ],
            ),
            SizedBox(
              width: SizeConfig.h(5),
            ),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(S.of(context).sign_with_apple, style: AppStyle.vexa12),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
