import 'dart:io';
import 'dart:ui';

import 'package:country_pickers/country_pickers.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:intl_phone_field/phone_number.dart';
import 'package:vertical_weight_slider/vertical_weight_slider.dart';
import '/App/Widgets/AppLoader.dart';
import '/Utils/AppSnackBar.dart';
import '/App/Widgets/MainButton.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import 'package:image_picker/image_picker.dart';
import '/data/repository/Repository.dart';

import '/generated/l10n.dart';
import '/injections.dart';

import 'package:progiom_cms/auth.dart';
import 'dart:math';
import 'package:intl/intl.dart' as intl;

import 'package:progiom_cms/homeSettings.dart';

class EditProfilePage extends StatefulWidget {
  EditProfilePage({Key? key}) : super(key: key);

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

PhoneNumber? phoneNumber;

class _EditProfilePageState extends State<EditProfilePage> {
  late final TextEditingController nameController;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController phoneController = TextEditingController();
  WeightSliderController? _controllerweight;
  double weight = 0.0;
  WeightSliderController? _controllerheight;
  double height = 0.0;
  final TextEditingController birthDataController = TextEditingController();
  String blood_type = 'A+';

  @override
  void initState() {
    super.initState();
    nameController = TextEditingController(
        text: sl<HomesettingsBloc>().settings?.user?.name ?? "");
    if (sl<HomesettingsBloc>().settings?.user?.countryCode != null) {
      final countryCode = CountryPickerUtils.getCountryByPhoneCode(
              sl<HomesettingsBloc>()
                      .settings
                      ?.user
                      ?.countryCode
                      ?.substring(1) ??
                  '')
          .isoCode;
      // print('cc is ${cc.numeric}');
      phoneNumber = PhoneNumber(
          countryISOCode: countryCode,
          countryCode: sl<HomesettingsBloc>().settings?.user?.countryCode ?? '',
          number: sl<HomesettingsBloc>().settings?.user?.mobile ?? '');
    }
    phoneController.text =
        (sl<HomesettingsBloc>().settings?.user?.mobile == null
            ? ''
            : sl<HomesettingsBloc>().settings?.user?.mobile!.substring(1))!;

    height = sl<HomesettingsBloc>().settings?.user?.height?.toDouble() ?? 0.0;
    _controllerheight = WeightSliderController(
        initialWeight:
            sl<HomesettingsBloc>().settings?.user?.height?.toDouble() ?? 0.0,
        minWeight: 0,
        interval: 0.1);
    weight = sl<HomesettingsBloc>().settings?.user?.weight?.toDouble() ?? 0.0;
    _controllerweight = WeightSliderController(
        initialWeight:
            sl<HomesettingsBloc>().settings?.user?.weight?.toDouble() ?? 0.0,
        minWeight: 0,
        interval: 0.1);

    birthDataController.text =
        sl<HomesettingsBloc>().settings?.user?.birth_date ?? "";

    blood_type = sl<HomesettingsBloc>().settings?.user?.blood_type ?? "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          title: Text(
            S.of(context).personalInfo,
            style: AppStyle.vexa16,
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          elevation: 0.0),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: SizeConfig.h(28),
          ),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                Stack(
                  alignment: Alignment.bottomRight,
                  children: [
                    CircleAvatar(
                      radius: SizeConfig.h(80),
                      foregroundImage: profilePhotoHandlerPath != null
                          ? FileImage(File(profilePhotoHandlerPath))
                          : null,
                      backgroundImage:
                          (sl<HomesettingsBloc>().settings?.user?.coverImage !=
                                  null)
                              ? NetworkImage((sl<HomesettingsBloc>()
                                      .settings
                                      ?.user
                                      ?.coverImage) ??
                                  "")
                              : null,
                    ),
                    Positioned(
                      bottom: SizeConfig.h(25),
                      child: Container(
                        height: SizeConfig.h(35),
                        width: SizeConfig.h(35),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.white),
                        child: IconButton(
                          onPressed: () {
                            pickImage();
                          },
                          icon: Icon(
                            Icons.add_a_photo_outlined,
                            color: AppStyle.primaryColor,
                            size: SizeConfig.h(21),
                          ),
                          padding: EdgeInsets.zero,
                        ),
                      ),
                    )
                  ],
                ),
                Row(
                  children: [
                    Text(
                      S.of(context).fullName,
                      style: AppStyle.vexa16,
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(10),
                ),
                Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: SizeConfig.h(60),
                      child: TextFormField(
                        validator: (v) {
                          if (v != null) {
                            if (v.isEmpty) {
                              return S.of(context).nameRequired;
                            }
                          }
                          return null;
                        },
                        controller: nameController,
                        style: AppStyle.vexa14.copyWith(color: Colors.black),
                        decoration: InputDecoration(
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(16))),
                      ),
                    ))
                  ],
                ),
                Row(
                  children: [
                    Text(
                      S.of(context).e_mail,
                      style: AppStyle.vexa16,
                    ),
                  ],
                ),
                Directionality(
                  textDirection: TextDirection.ltr,
                  child: IntlPhoneField(
                    initialCountryCode: phoneNumber?.countryISOCode,
                    searchText: S.of(context).searchHere,
                    dropdownTextStyle: TextStyle(fontSize: SizeConfig.w(12)),
                    style: TextStyle(fontSize: SizeConfig.w(12)),
                    showCountryFlag:
                        phoneNumber?.countryCode == '+963' ? false : true,
                    onCountryChanged: (c) {
                      setState(() {
                        phoneNumber?.countryCode = '+' + c.dialCode;
                      });
                    },
                    onChanged: (PhoneNumber phoneNumber) {
                      setState(() {
                        phoneNumber.countryCode = phoneNumber.countryCode;
                        phoneNumber.number = '0' + phoneNumber.number;
                      });
                    },
                    controller: phoneController,
                    invalidNumberMessage: S.of(context).mobileValidator,
                    decoration: InputDecoration(
                        hintTextDirection: TextDirection.rtl,
                        alignLabelWithHint: true,
                        suffixIcon: Icon(Icons.phone_android),
                        labelText: S.of(context).phone_number,
                        contentPadding: EdgeInsets.symmetric(
                          // vertical: SizeConfig.h(2),
                          horizontal: SizeConfig.w(10),
                        ),
                        border: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 1,
                              style: BorderStyle.solid,
                              color: AppStyle.primaryColor),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(16),
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 1,
                              style: BorderStyle.solid,
                              color: AppStyle.disabledColor),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(16),
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 1,
                              style: BorderStyle.solid,
                              color: AppStyle.primaryColor),
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(16),
                          ),
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.auto,
                        labelStyle: TextStyle(
                          fontSize: SizeConfig.h(14),
                        ),
                        errorStyle: TextStyle(fontSize: SizeConfig.h(14)),
                        fillColor: Colors.white70),
                  ),
                ),
                SizedBox(
                  height: SizeConfig.h(10),
                ),
                Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: SizeConfig.h(60),
                      child: TextFormField(
                        readOnly: true,
                        initialValue:
                            sl<HomesettingsBloc>().settings?.user?.email ?? "",
                        style: AppStyle.vexa14.copyWith(color: Colors.black),
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: AppStyle.disabledColor,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(16))),
                      ),
                    ))
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(20),
                ),
                Row(
                  children: [
                    Expanded(
                        child: SizedBox(
                      height: SizeConfig.h(60),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.black, spreadRadius: 1),
                          ],
                        ),
                        child: buildBirthDateField(
                          context,
                          controller: birthDataController,
                          label: S.of(context).old,
                        ),
                      ),
                    ))
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(50),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
                  child: Text(
                    S.of(context).weight,
                    style: AppStyle.vexa20.copyWith(
                        color: AppStyle.secondaryDark, wordSpacing: 3),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: SizeConfig.h(30),
                ),
                Container(
                  child: Center(
                    child: Text(
                      "$weight",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: AppStyle.primaryColor),
                    ),
                  ),
                ),
                Transform.rotate(
                    angle: 90 / 180 * pi,
                    child: Container(
                      child: VerticalWeightSlider(
                        controller: _controllerweight!,
                        decoration: const PointerDecoration(
                          width: 100.0,
                          height: 3.0,
                          largeColor: Color(0xFF898989),
                          mediumColor: Color(0xFFC5C5C5),
                          smallColor: Color(0xFFF0F0F0),
                          gap: 30.0,
                        ),
                        onChanged: (double value) {
                          setState(() {
                            weight = value;
                          });
                        },
                        indicator: Container(
                          height: 3.0,
                          width: 130.0,
                          alignment: Alignment.centerLeft,
                          color: AppStyle.primaryColor,
                        ),
                      ),
                    )),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(50)),
                  child: Text(
                    S.of(context).height,
                    style: AppStyle.vexa20.copyWith(
                        color: AppStyle.secondaryDark, wordSpacing: 3),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: SizeConfig.h(30),
                ),
                Container(
                  child: Center(
                    child: Text(
                      "$height",
                      style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.bold,
                          color: AppStyle.primaryColor),
                    ),
                  ),
                ),
                Transform.rotate(
                    angle: 90 / 180 * pi,
                    child: Container(
                      child: VerticalWeightSlider(
                        controller: _controllerheight!,
                        decoration: const PointerDecoration(
                          width: 100.0,
                          height: 3.0,
                          largeColor: Color(0xFF898989),
                          mediumColor: Color(0xFFC5C5C5),
                          smallColor: Color(0xFFF0F0F0),
                          gap: 30.0,
                        ),
                        onChanged: (double value) {
                          setState(() {
                            height = value;
                          });
                        },
                        indicator: Container(
                          height: 3.0,
                          width: 130.0,
                          alignment: Alignment.centerLeft,
                          color: AppStyle.primaryColor,
                        ),
                      ),
                    )),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(S.of(context).Create_account_Step_43),
                    Wrap(
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              blood_type = "A+";
                            });
                          },
                          child: Container(
                            margin:
                                Localizations.localeOf(context).languageCode ==
                                        "ar"
                                    ? EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        left: SizeConfig.h(10))
                                    : EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        right: SizeConfig.h(10)),
                            padding: EdgeInsets.only(
                                top: SizeConfig.h(5),
                                bottom: SizeConfig.h(5),
                                left: SizeConfig.h(12),
                                right: SizeConfig.h(12)),
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(70),
                            decoration: BoxDecoration(
                              boxShadow: [AppStyle.boxShadow3on6],
                              color: blood_type == "A+"
                                  ? AppStyle.primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: Center(
                              child: Text("A+",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: SizeConfig.h(12),
                                    color: blood_type == "A+"
                                        ? AppStyle.whiteColor
                                        : Colors.black,
                                  )),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              blood_type = "A-";
                            });
                          },
                          child: Container(
                            margin:
                                Localizations.localeOf(context).languageCode ==
                                        "ar"
                                    ? EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        left: SizeConfig.h(10))
                                    : EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        right: SizeConfig.h(10)),
                            padding: EdgeInsets.only(
                                top: SizeConfig.h(5),
                                bottom: SizeConfig.h(5),
                                left: SizeConfig.h(12),
                                right: SizeConfig.h(12)),
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(70),
                            decoration: BoxDecoration(
                              boxShadow: [AppStyle.boxShadow3on6],
                              color: blood_type == "A-"
                                  ? AppStyle.primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: Center(
                              child: Text("A-",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: SizeConfig.h(12),
                                    color: blood_type == "A-"
                                        ? AppStyle.whiteColor
                                        : Colors.black,
                                  )),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              blood_type = "B+";
                            });
                          },
                          child: Container(
                            margin:
                                Localizations.localeOf(context).languageCode ==
                                        "ar"
                                    ? EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        left: SizeConfig.h(10))
                                    : EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        right: SizeConfig.h(10)),
                            padding: EdgeInsets.only(
                                top: SizeConfig.h(5),
                                bottom: SizeConfig.h(5),
                                left: SizeConfig.h(12),
                                right: SizeConfig.h(12)),
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(70),
                            decoration: BoxDecoration(
                              boxShadow: [AppStyle.boxShadow3on6],
                              color: blood_type == "B+"
                                  ? AppStyle.primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: Center(
                              child: Text("B+",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: SizeConfig.h(12),
                                    color: blood_type == "B+"
                                        ? AppStyle.whiteColor
                                        : Colors.black,
                                  )),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              blood_type = "B-";
                            });
                          },
                          child: Container(
                            margin:
                                Localizations.localeOf(context).languageCode ==
                                        "ar"
                                    ? EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        left: SizeConfig.h(10))
                                    : EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        right: SizeConfig.h(10)),
                            padding: EdgeInsets.only(
                                top: SizeConfig.h(5),
                                bottom: SizeConfig.h(5),
                                left: SizeConfig.h(12),
                                right: SizeConfig.h(12)),
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(70),
                            decoration: BoxDecoration(
                              boxShadow: [AppStyle.boxShadow3on6],
                              color: blood_type == "B-"
                                  ? AppStyle.primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: Center(
                              child: Text("B-",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: SizeConfig.h(12),
                                    color: blood_type == "B-"
                                        ? AppStyle.whiteColor
                                        : Colors.black,
                                  )),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              blood_type = "O+";
                            });
                          },
                          child: Container(
                            margin:
                                Localizations.localeOf(context).languageCode ==
                                        "ar"
                                    ? EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        left: SizeConfig.h(10))
                                    : EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        right: SizeConfig.h(10)),
                            padding: EdgeInsets.only(
                                top: SizeConfig.h(5),
                                bottom: SizeConfig.h(5),
                                left: SizeConfig.h(12),
                                right: SizeConfig.h(12)),
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(70),
                            decoration: BoxDecoration(
                              boxShadow: [AppStyle.boxShadow3on6],
                              color: blood_type == "O+"
                                  ? AppStyle.primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: Center(
                              child: Text("O+",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: SizeConfig.h(12),
                                    color: blood_type == "O+"
                                        ? AppStyle.whiteColor
                                        : Colors.black,
                                  )),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              blood_type = "O-";
                            });
                          },
                          child: Container(
                            margin:
                                Localizations.localeOf(context).languageCode ==
                                        "ar"
                                    ? EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        left: SizeConfig.h(10))
                                    : EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        right: SizeConfig.h(10)),
                            padding: EdgeInsets.only(
                                top: SizeConfig.h(5),
                                bottom: SizeConfig.h(5),
                                left: SizeConfig.h(12),
                                right: SizeConfig.h(12)),
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(70),
                            decoration: BoxDecoration(
                              boxShadow: [AppStyle.boxShadow3on6],
                              color: blood_type == "O-"
                                  ? AppStyle.primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: Center(
                              child: Text("O-",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: SizeConfig.h(12),
                                    color: blood_type == "O-"
                                        ? AppStyle.whiteColor
                                        : Colors.black,
                                  )),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              blood_type = "AB+";
                            });
                          },
                          child: Container(
                            margin:
                                Localizations.localeOf(context).languageCode ==
                                        "ar"
                                    ? EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        left: SizeConfig.h(10))
                                    : EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        right: SizeConfig.h(10)),
                            padding: EdgeInsets.only(
                                top: SizeConfig.h(5),
                                bottom: SizeConfig.h(5),
                                left: SizeConfig.h(12),
                                right: SizeConfig.h(12)),
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(70),
                            decoration: BoxDecoration(
                              boxShadow: [AppStyle.boxShadow3on6],
                              color: blood_type == "AB+"
                                  ? AppStyle.primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: Center(
                              child: Text("AB+",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: SizeConfig.h(12),
                                    color: blood_type == "AB+"
                                        ? AppStyle.whiteColor
                                        : Colors.black,
                                  )),
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              blood_type = "AB-";
                            });
                          },
                          child: Container(
                            margin:
                                Localizations.localeOf(context).languageCode ==
                                        "ar"
                                    ? EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        left: SizeConfig.h(10))
                                    : EdgeInsets.only(
                                        top: SizeConfig.h(10),
                                        bottom: SizeConfig.h(10),
                                        right: SizeConfig.h(10)),
                            padding: EdgeInsets.only(
                                top: SizeConfig.h(5),
                                bottom: SizeConfig.h(5),
                                left: SizeConfig.h(12),
                                right: SizeConfig.h(12)),
                            height: SizeConfig.h(50),
                            width: SizeConfig.w(70),
                            decoration: BoxDecoration(
                              boxShadow: [AppStyle.boxShadow3on6],
                              color: blood_type == "AB-"
                                  ? AppStyle.primaryColor
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(80),
                            ),
                            child: Center(
                              child: Text("AB-",
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: SizeConfig.h(12),
                                    color: blood_type == "AB-"
                                        ? AppStyle.whiteColor
                                        : Colors.black,
                                  )),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(30),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: SizeConfig.h(50),
                    ),
                    if (isLoading)
                      AppLoader()
                    else
                      Expanded(
                        child: InkWell(
                          onTap: () async {
                            if (formKey.currentState?.validate() ?? false) {
                              editData();
                            }
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(SizeConfig.h(50)),
                                gradient: LinearGradient(
                                  colors: [
                                    AppStyle.primaryColor,
                                    AppStyle.secondaryColor,
                                  ],
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                ),
                              ),
                              height: SizeConfig.h(55),
                              width: double.infinity,
                              child: Center(
                                child: Text(
                                  S.of(context).editProfile,
                                  style: TextStyle(
                                      fontSize: SizeConfig.h(17),
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              )),
                        ),
                      ),
                    SizedBox(
                      width: SizeConfig.h(50),
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig.h(60),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  ClipRRect buildBirthDateField(BuildContext context,
      {required TextEditingController controller, required String label}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(16),
      child: TextFormField(
        readOnly: true,
        onTap: () async {
          var result = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime(1900),
              lastDate: DateTime.now());

          if (result != null) {
            birthDataController.text =
                intl.DateFormat("yyyy-MM-dd", "en").format(result);
          }
        },
        validator: (v) {
          if ((v?.isEmpty ?? true)) {
            return S.of(context).birthdate_validator;
          }
          return null;
        },
        controller: controller,
        style: TextStyle(
            fontSize: SizeConfig.h(14),
            fontWeight: FontWeight.bold,
            height: 2.0,
            color: Colors.black,
            fontFeatures: [FontFeature("liga", 0)]),
        decoration: InputDecoration(
            filled: true,
            fillColor: AppStyle.disabledColor,
            border: InputBorder.none,
            label: Text(label),
            labelStyle: TextStyle(
                fontSize: SizeConfig.h(14),
                fontWeight: FontWeight.bold,
                height: 2.0,
                color: AppStyle.blacColor,
                fontFeatures: [FontFeature("liga", 0)])),
      ),
    );
  }

  bool isLoading = false;
  var profilePhotoHandlerPath;

  pickImage() async {
    final ImagePicker _picker = ImagePicker();

    final XFile? photo = await _picker.pickImage(source: ImageSource.gallery);
    if (photo != null) {
      setState(() {
        profilePhotoHandlerPath = photo.path;
      });
    }
  }

  editData() async {
    setState(() {
      isLoading = true;
    });
    var data = {
      "name": nameController.text,
      "country_code": phoneNumber!.countryCode,
      "mobile": phoneNumber!.number,
      "birth_date": birthDataController.text,
      "weight": weight.toString(),
      "height": height.toString(),
      "blood_type": blood_type
    };
    if (profilePhotoHandlerPath != null) {
      var url = await sl<Repository>().uploadFile(
        profilePhotoHandlerPath,
      );
      data.putIfAbsent("image", () => url);
    }

    final result = await UpdateProfile(sl()).call(UpdateProfileParams(data));
    setState(() {
      isLoading = false;
    });
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      sl<HomesettingsBloc>().add(GetSettings());

      Navigator.pop(context, true);
    });
  }
}
