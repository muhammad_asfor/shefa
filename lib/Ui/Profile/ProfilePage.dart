import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:progiom_cms/notifications.dart';
import 'package:tajra/Ui/appointments_all/AppointmentsPageAll.dart';
import 'package:tutorial_coach_mark/tutorial_coach_mark.dart';
import '/App/App.dart';
import '/App/Widgets/MainButton.dart';
import '/Ui/Profile/page_view_page.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import 'package:restart_app/restart_app.dart';
import '/data/sharedPreferences/SharedPrefHelper.dart';
import '/generated/l10n.dart';
import '../../injections.dart';
import 'package:package_info_plus/package_info_plus.dart';

part 'widgets/profile_tutorial.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool notificationEnabled = true;
  bool showLanguages = false;
  bool showCurrencies = false;

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
      MaterialState.selected
    };
    if (states.any(interactiveStates.contains)) {
      return AppStyle.primaryColor;
    }
    return AppStyle.disabledColor;
  }

  final TextEditingController userNameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  bool notificationsEnabled = false;
  final debouncer = Debouncer();
  String selectedLanguage = "ar";
  String? selectedCurrency;
  PackageInfo? packageInfo;
  late final String uuid;

  @override
  void initState() {
    super.initState();

    getPreferences();
    getLanguage();
    PackageInfo.fromPlatform().then((value) {
      if (mounted)
        setState(() {
          packageInfo = value;
        });
    }).catchError((onError) {});
    uuid = sl<HomesettingsBloc>().settings?.user?.uuid ?? "";
  }

  Map? preferences;

  getPreferences() async {
    final result = await GetPreferences(sl()).call(NoParams());
    result.fold((l) {
      AppSnackBar.show(context, l.errorMessage, ToastType.Error);
    }, (r) {
      if (mounted)
        setState(() {
          preferences = r;
          notificationsEnabled = preferences!["push_notifications"] == 1;
        });
    });
  }

  getLanguage() async {
    selectedLanguage =
        await sl<PrefsHelper>().loadLangFromSharedPref() ?? App.defaultLanguage;
    selectedCurrency = await sl<PrefsHelper>().loadCurrencyFromSharedPref();
    if (mounted) setState(() {});
  }

  restart(var v) async {
    selectedLanguage = await v;
    await sl<PrefsHelper>().saveLangToSharedPref(v);
    App.setLocale(context, selectedLanguage);
  }

  @override
  Widget build(BuildContext context) {
    final homeModel = sl<HomesettingsBloc>().settings!;
    if (selectedCurrency == null &&
        homeModel.currencies != null &&
        homeModel.currencies!.isNotEmpty)
      selectedCurrency = homeModel.currencies![0].code;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(40.0),
          child: Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                    height: SizeConfig.h(22),
                  ),
                  Container(
                    width: double.infinity,
                    child: Center(
                      child: Text(
                        S.of(context).profile,
                        overflow: TextOverflow.ellipsis,
                        style: AppStyle.vexa20.copyWith(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: SizeConfig.h(25)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.w(35),
                  ),
                  CircleAvatar(
                    radius: SizeConfig.h(60),
                    backgroundImage:
                        (sl<HomesettingsBloc>().settings?.user?.coverImage !=
                                null)
                            ? NetworkImage((sl<HomesettingsBloc>()
                                    .settings
                                    ?.user
                                    ?.coverImage) ??
                                "")
                            : null,
                  ),
                  SizedBox(
                    height: SizeConfig.w(8),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 150,
                        child: Center(
                          child: Text(
                            sl<HomesettingsBloc>().settings?.user?.name ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: SizeConfig.w(8),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        S.of(context).numhelth,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: AppStyle.primaryColor,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        sl<HomesettingsBloc>().settings?.user?.uuid ?? "",
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  SizedBox(
                    width: SizeConfig.w(60),
                  ),
                ],
              ),
              SizedBox(
                height: SizeConfig.h(50),
              ),
              buildSubSection(
                S.of(context).PreviousAppointments,
                () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => appointmentsPageAll()),
                  );
                },
                svgSize: 18,
                icon: Icons.date_range_outlined,
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios_sharp,
                      size: SizeConfig.h(18)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => appointmentsPageAll()),
                    );
                  },
                ),
              ),
              SizedBox(
                height: SizeConfig.h(14),
              ),
              buildSubSection(
                S.of(context).personalInfo,
                () {
                  Navigator.pushNamed(context, "/editProfile");
                },
                svgSize: 18,
                icon: Icons.edit_rounded,
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios_sharp,
                      size: SizeConfig.h(18)),
                  onPressed: () {
                    Navigator.pushNamed(context, "/editProfile");
                  },
                ),
              ),
              SizedBox(
                height: SizeConfig.h(14),
              ),
              buildSubSection(
                S.of(context).technicalSupport,
                () {
                  if (preferences != null)
                    openWhatsapp(preferences!["support_phone"]);
                },
                svgSize: 18,
                svgAsset: "assets/support.svg",
                trailing: IconButton(
                  icon: Icon(Icons.arrow_forward_ios_sharp,
                      size: SizeConfig.h(18)),
                  onPressed: () {
                    if (preferences != null)
                      openWhatsapp(preferences!["support_phone"]);
                  },
                ),
              ),
              SizedBox(
                height: SizeConfig.h(14),
              ),
              buildSubSection(
                S.of(context).notifications,
                () {},
                svgSize: 18,
                svgAsset: "assets/notification(1).svg",
                trailing: Transform.scale(
                    scale: 0.7,
                    child: Switch(
                        value: notificationEnabled,
                        thumbColor: MaterialStateProperty.resolveWith(getColor),
                        inactiveThumbColor: AppStyle.disabledColor,
                        inactiveTrackColor: AppStyle.disabledColor,
                        activeColor: AppStyle.primaryColor,
                        onChanged: (v) {
                          setState(() {
                            notificationsEnabled = v;
                          });
                          debouncer.run(() {
                            DisableEnableNotification(sl()).call(
                                DisableEnableNotificationParams(
                                    enable: notificationsEnabled));
                          });
                        })),
              ),
              SizedBox(
                height: SizeConfig.h(14),
              ),
              buildSubSection(S.of(context).language, () {
                setState(() {
                  showLanguages = !showLanguages;
                });
              },
                  svgSize: 18,
                  icon: Icons.language,
                  trailing: SizedBox(
                    height: SizeConfig.h(50),
                    child: IconButton(
                      icon: Icon(Icons.arrow_forward_ios_sharp,
                          size: SizeConfig.h(18)),
                      onPressed: () {
                        setState(() {
                          showLanguages = !showLanguages;
                        });
                      },
                    ),
                  )),
              if (showLanguages)
                Column(
                  children: [
                    if (isLanguageActive("ar", homeModel))
                      Row(
                        children: [
                          Expanded(
                            child: SizedBox(
                              child: RadioListTile<String>(
                                  contentPadding: EdgeInsets.zero,
                                  title: Text(S.of(context).arabic,
                                      style: AppStyle.vexaLight12),
                                  activeColor: AppStyle.primaryColor,
                                  value: "ar",
                                  groupValue: selectedLanguage,
                                  onChanged: (dynamic v) async {
                                    await restart(v);
                                    Restart.restartApp();
                                  }),
                            ),
                          )
                        ],
                      ),
                    if (isLanguageActive("en", homeModel))
                      Row(
                        children: [
                          Expanded(
                            child: SizedBox(
                              child: RadioListTile<String>(
                                  contentPadding: EdgeInsets.zero,
                                  title: Text(S.of(context).english,
                                      style: AppStyle.vexaLight12),
                                  activeColor: AppStyle.primaryColor,
                                  value: "en",
                                  groupValue: selectedLanguage,
                                  onChanged: (dynamic v) async {
                                    await restart(v);
                                    Restart.restartApp();
                                  }),
                            ),
                          )
                        ],
                      ),
                  ],
                ),
              SizedBox(
                height: SizeConfig.h(14),
              ),
              for (PageModel page in homeModel.pages ?? [])
                Column(
                  children: [
                    GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onTap: () {
                        Navigator.push(
                            context,
                            CupertinoPageRoute(
                                builder: (_) => PageViewScreen(page.id)));
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                            bottom: SizeConfig.h(30), top: SizeConfig.h(7)),
                        width: double.infinity,
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: Colors.black12, width: 0.5))),
                        child: Row(
                          children: [
                            Image.network(
                              page.coverImage ?? "",
                              fit: BoxFit.fill,
                              height: SizeConfig.h(18),
                              width: SizeConfig.h(18),
                            ),
                            SizedBox(
                              width: SizeConfig.h(8),
                            ),
                            Text(
                              page.title,
                              style: AppStyle.vexaLight12,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: SizeConfig.h(30),
                    ),
                  ],
                ),
              Row(
                children: [
                  SizedBox(
                    width: SizeConfig.h(24),
                  ),
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      sl<AuthBloc>().add(LogoutEvent());
                      sl<HomesettingsBloc>().settings?.user = null;
                      // Navigator.pop(context);
                      Navigator.pushReplacementNamed(context, "/login",
                          arguments: false);
                    },
                    child: SizedBox(
                      height: SizeConfig.h(44),
                      child: MainButton(
                        color: AppStyle.redColor,
                        isOutlined: true,
                        child: Center(
                            child: Text(
                          S.of(context).signOut,
                          style: AppStyle.vexa16
                              .copyWith(color: AppStyle.redColor),
                        )),
                      ),
                    ),
                  )),
                  SizedBox(
                    width: SizeConfig.h(24),
                  ),
                ],
              ),
              SizedBox(
                height: SizeConfig.h(40),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildSubSection(
    String title,
    Function onTap, {
    IconData? icon,
    String? svgAsset,
    double? svgSize,
    Widget? trailing,
    GlobalKey? key,
  }) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        onTap();
      },
      child: Container(
        padding: EdgeInsets.only(bottom: SizeConfig.h(8)),
        width: double.infinity,
        decoration: BoxDecoration(
            border:
                Border(bottom: BorderSide(color: Colors.black12, width: 0.5))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          key: key,
          children: [
            icon != null
                ? Icon(
                    icon,
                    color: AppStyle.primaryColor,
                    size: SizeConfig.h(20),
                  )
                : SvgPicture.asset(
                    svgAsset!,
                    height: SizeConfig.h(svgSize!),
                    width: SizeConfig.h(svgSize),
                    color: AppStyle.primaryColor,
                  ),
            SizedBox(
              width: SizeConfig.h(16),
            ),
            Text(
              title,
              style: AppStyle.vexa14
                  .copyWith(color: Colors.black, fontWeight: FontWeight.w700),
            ),
            if (trailing != null) Spacer(),
            if (trailing != null) trailing
          ],
        ),
      ),
    );
  }

  Widget buildTextField(
      String label, IconData icon, TextEditingController controller) {
    return SizedBox(
      height: SizeConfig.h(50),
      child: TextField(
        readOnly: true,
        controller: controller,
        decoration: InputDecoration(
            border: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.primaryColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(10),
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.disabledColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(10),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  width: 1,
                  style: BorderStyle.solid,
                  color: AppStyle.primaryColor),
              borderRadius: const BorderRadius.all(
                const Radius.circular(10),
              ),
            ),
            prefixIcon: Icon(
              icon,
            ),
            labelText: label,
            floatingLabelBehavior: FloatingLabelBehavior.auto,
            labelStyle: TextStyle(fontSize: SizeConfig.h(14)),
            fillColor: Colors.white70),
      ),
    );
  }
}

void shareApp() {
  if (Platform.isIOS)
    actionShare("https://apps.apple.com/us/app/شفاء/id1589506302",
        SizeConfig.screenWidth);
  else
    actionShare(
        "https://play.google.com/store/apps/details?id=com.progiom.shefa",
        SizeConfig.screenWidth);
}

bool isLanguageActive(String slug, homeModel) {
  return (homeModel.languages.contains(Languages(slug: slug)));
}
