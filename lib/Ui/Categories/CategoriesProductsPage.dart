import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/ProductCard3.dart';
import '../../Utils/AppSnackBar.dart';
import '../../injections.dart';
import '/App/Widgets/AppErrorWidget.dart';
import '/App/Widgets/AppLoader.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '/App/Widgets/EmptyPlacholder.dart';
import '/App/Widgets/Products_shimmer_grid.dart';
import 'package:flutter_svg/svg.dart';
import '/Ui/Categories/bloc/category_bloc.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';

class CategoryProductsPage extends StatefulWidget {
  String? idCategory;
  CategoryProductsPage({Key? key, this.idCategory}) : super(key: key);

  @override
  _CategoryProductsPageState createState() => _CategoryProductsPageState();
}

class _CategoryProductsPageState extends State<CategoryProductsPage> {
  late GetProducatsByCategoryParams selectedCategoryId;
  late final CategoryBloc bloc;
  final homeSettingsBloc = sl<HomesettingsBloc>();
  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  bool showResult = false;

  @override
  void initState() {
    selectedCategoryId =
        GetProducatsByCategoryParams(speciality_id: widget.idCategory ?? "");

    bloc = CategoryBloc(selectedCategoryId);
    bloc.add(
      LoadEvent(
        {
          'speciality_id': selectedCategoryId.speciality_id,
        },
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  final settings = sl<HomesettingsBloc>().settings!;
  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: homeSettingsBloc,
      listener: (context, HomesettingsState state) {
        if (state is HomeSettingsReady) {
          setState(() {});
        }
      },
      child: BlocBuilder(
          bloc: homeSettingsBloc,
          builder: (context, HomesettingsState state) {
            return SafeArea(
              child: Scaffold(
                  key: _key,
                  backgroundColor: Colors.white,
                  body: CustomScrollView(
                    controller: bloc.scrollController,
                    slivers: [
                      SliverToBoxAdapter(
                        child: SizedBox(
                          height: SizeConfig.h(22),
                        ),
                      ),
                      SliverToBoxAdapter(
                          child: Center(
                        child: Text(
                          S.of(context).Doctors_List,
                          style: AppStyle.vexa20.copyWith(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 25),
                        ),
                      )),
                      SliverToBoxAdapter(
                        child: SizedBox(
                          height: SizeConfig.h(14),
                        ),
                      ),
                      buildSubCategories(),
                      SliverToBoxAdapter(
                        child: SizedBox(
                          height: SizeConfig.h(24),
                        ),
                      ),
                      BlocBuilder(
                        bloc: bloc,
                        builder: (context, state) {
                          if (state is LoadingState) {
                            return SliverToBoxAdapter(
                              child: ProductsShimmerGrid(),
                            );
                          }
                          if (state is ErrorState) {
                            return SliverFillRemaining(
                                child: AppErrorWidget(text: state.error));
                          }
                          if (state is SuccessState<List<TopDoctor>>) {
                            if (state.items.isEmpty) {
                              return SliverFillRemaining(
                                  child: EmptyPlacholder(
                                title: S.of(context).no_result,
                                imageName: "assets/noSearch.png",
                                subtitle: "",
                                actionTitle: S.of(context).continuee,
                                onActionTap: () {
                                  Navigator.pop(context);
                                },
                              ));
                            }
                            return SliverGrid(
                              gridDelegate:
                                  SliverGridDelegateWithMaxCrossAxisExtent(
                                      maxCrossAxisExtent:
                                          SizeConfig.screenWidth,
                                      crossAxisSpacing:
                                          0, //cuase the card already taken margin
                                      mainAxisExtent: SizeConfig.h(129),
                                      mainAxisSpacing: SizeConfig.h(0)),
                              delegate:
                                  SliverChildBuilderDelegate((context, index) {
                                return ProductCard3(
                                    product: state.items[index]);
                              }, childCount: state.items.length),
                            );
                          }
                          return SliverToBoxAdapter();
                        },
                      ),
                      SliverToBoxAdapter(
                        child: BlocBuilder(
                          bloc: bloc,
                          builder: (context, state) {
                            if (state is SuccessState) if (!state.hasReachedMax)
                              return Center(
                                child: AppLoader(),
                              );
                            return Container();
                          },
                        ),
                      ),
                      SliverToBoxAdapter(
                        child: SizedBox(
                          height: SizeConfig.h(25),
                        ),
                      )
                    ],
                  )),
            );
          }),
    );
  }

  SliverToBoxAdapter buildSubCategories() {
    return SliverToBoxAdapter(
      child: SizedBox(
        height: SizeConfig.h(60),
        child: Padding(
          padding: Localizations.localeOf(context).languageCode == "en"
              ? EdgeInsets.only(left: SizeConfig.w(29))
              : EdgeInsets.only(right: SizeConfig.w(29)),
          child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: (settings.specialities!.length + 1),
              itemBuilder: (context, index) {
                if (index == 0) return allProductsSection();
                final category = settings.specialities![index - 1];
                final isSelected =
                    selectedCategoryId.speciality_id == category.id.toString();
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      selectedCategoryId = GetProducatsByCategoryParams(
                          speciality_id: category.id.toString());
                    });

                    bloc.add(LoadEvent({
                      'speciality_id': selectedCategoryId.speciality_id,
                    }));
                  },
                  child: Container(
                    margin: Localizations.localeOf(context).languageCode == "ar"
                        ? EdgeInsets.only(
                            top: SizeConfig.h(10),
                            bottom: SizeConfig.h(10),
                            left: SizeConfig.h(10))
                        : EdgeInsets.only(
                            top: SizeConfig.h(10),
                            bottom: SizeConfig.h(10),
                            right: SizeConfig.h(10)),
                    padding: EdgeInsets.only(
                        top: SizeConfig.h(5),
                        bottom: SizeConfig.h(5),
                        left: SizeConfig.h(12),
                        right: SizeConfig.h(12)),
                    height: 50,
                    decoration: BoxDecoration(
                      boxShadow: [AppStyle.boxShadow3on6],
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(80),
                    ),
                    child: Row(
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 7),
                          height: SizeConfig.h(14),
                          width: SizeConfig.w(14),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5),
                            child: CachedNetworkImage(
                                imageUrl: category.coverImage,
                                fit: BoxFit.cover,
                                color: isSelected
                                    ? AppStyle.primaryColor
                                    : Colors.black),
                          ),
                        ),
                        Text(category.title,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: SizeConfig.h(12),
                              color: isSelected
                                  ? AppStyle.primaryColor
                                  : Colors.black,
                            ))
                      ],
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }

  allProductsSection() {
    final isSelected = selectedCategoryId.speciality_id == "";
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedCategoryId = GetProducatsByCategoryParams(speciality_id: "");
        });

        bloc.add(LoadEvent({
          'speciality_id': "",
        }));
      },
      child: Container(
        margin: EdgeInsets.all(SizeConfig.h(10)),
        padding: EdgeInsets.only(
            top: SizeConfig.h(5),
            bottom: SizeConfig.h(5),
            left: SizeConfig.h(12),
            right: SizeConfig.h(12)),
        decoration: BoxDecoration(
          boxShadow: [AppStyle.boxShadow3on6],
          color: Colors.white,
          borderRadius: BorderRadius.circular(80),
        ),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 7),
              height: SizeConfig.h(20),
              width: SizeConfig.h(20),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: SvgPicture.asset(
                  "assets/favorite.svg",
                  fit: BoxFit.cover,
                  color: isSelected ? AppStyle.primaryColor : Colors.black,
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              S.of(context).all,
              //    overflow: TextOverflow.ellipsis,
              maxLines: 1,
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: SizeConfig.h(12),
                  color: isSelected ? AppStyle.primaryColor : Colors.black),
            )
          ],
        ),
      ),
    );
  }
}

class SortByWidget extends StatefulWidget {
  const SortByWidget({
    Key? key,
  }) : super(key: key);

  @override
  _SortByWidgetState createState() => _SortByWidgetState();
}

class _SortByWidgetState extends State<SortByWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: SizeConfig.h(280),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(SizeConfig.h(20)),
              topRight: Radius.circular(SizeConfig.h(20)))),
      margin: EdgeInsets.symmetric(horizontal: SizeConfig.h(24)),
      child: ClipRRect(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(SizeConfig.h(20)),
            topRight: Radius.circular(SizeConfig.h(20))),
        child: Column(
          children: [
            SizedBox(
              height: SizeConfig.h(27),
            ),
            Text(
              S.of(context).sort_by,
              style: AppStyle.vexa20,
            ),
            Expanded(
              child: ShaderMask(
                shaderCallback: (bounds) => LinearGradient(colors: [
                  Colors.white.withOpacity(0.2),
                  Colors.white.withOpacity(0.5),
                  Colors.white.withOpacity(1.0),
                  Colors.white.withOpacity(0.5),
                  Colors.white.withOpacity(0.2)
                ], stops: [
                  0.2,
                  0.3,
                  0.5,
                  0.6,
                  1.0
                ], begin: Alignment.topCenter, end: Alignment.bottomCenter)
                    .createShader(bounds),
                child: CupertinoPicker(
                  backgroundColor: Colors.white,
                  onSelectedItemChanged: (value) {
                    // setState(() {
                    //   selectedValue = value;
                    // });
                  },
                  selectionOverlay: Container(),
                  itemExtent: SizeConfig.h(37),
                  children: [
                    Text(
                      'الترتيب الافتراضي',
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                    Text(
                      'السعر من الأعلى للأقل',
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                    Text(
                      'السعر من الأعلى للأقل',
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                    Text(
                      'السعر من الأعلى للأقل',
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                    Text(
                      'السعر من الأعلى للأقل',
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                    Text(
                      'السعر من الأعلى للأقل',
                      style: AppStyle.vexa16.copyWith(color: Colors.black),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
