import 'dart:async';
import 'package:dartz/dartz.dart';

import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';

import '../../../injections.dart';

class CategoryBloc extends SimpleLoaderBloc<List<TopDoctor>> {
  int page = 0;
  int? minPrice;
  int? maxPrice;

  String orderColumn = "created_at";
  String orderDirection = "asc";
  int? categoryId;
  int? rating;

  GetProducatsByCategoryParams? params;
  CategoryBloc(this.params) : super(eventParams: params);

  @override
  Future<Either<Failure, List<TopDoctor>>> load(SimpleBlocEvent event) async {
    if (event is LoadEvent) {
      page = 0;
    }
    page++;
    if (event is LoadMoreEvent)
      print('params ar ${event.params.speciality_id}');
    final String parentId = (event is LoadEvent)
        ? event.params['speciality_id']
        : (event is LoadMoreEvent)
            ? params!.speciality_id
            : "";
    return GetProducatsByCategory(sl()).call(ProducatsByCategoryParams(
      parentId: parentId,
      page: page,
      categoryId: categoryId,
    ));
  }
}

class GetProducatsByCategoryParams {
  final String speciality_id;

  GetProducatsByCategoryParams({required this.speciality_id});
}
