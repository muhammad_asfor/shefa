// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ar locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ar';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "About": MessageLookupByLibrary.simpleMessage("حول"),
        "Appointments": MessageLookupByLibrary.simpleMessage("المعاينات"),
        "Book_Appointment": MessageLookupByLibrary.simpleMessage("موعد الحجز"),
        "Certificates": MessageLookupByLibrary.simpleMessage("الشهادات"),
        "Clients": MessageLookupByLibrary.simpleMessage("عملاء"),
        "Create_account_Step_1":
            MessageLookupByLibrary.simpleMessage("أيهما أنت؟"),
        "Create_account_Step_12": MessageLookupByLibrary.simpleMessage(
            "لمنحك تجربة المحافظ ، نحتاج إلى معرفة جنسك"),
        "Create_account_Step_2":
            MessageLookupByLibrary.simpleMessage("كم عمرك ؟"),
        "Create_account_Step_3":
            MessageLookupByLibrary.simpleMessage("كم وزنك ؟"),
        "Create_account_Step_32":
            MessageLookupByLibrary.simpleMessage("كم طولك ؟"),
        "Create_account_Step_4":
            MessageLookupByLibrary.simpleMessage("حان الوقت"),
        "Create_account_Step_42":
            MessageLookupByLibrary.simpleMessage("لملء تاريخك الطبي"),
        "Create_account_Step_43":
            MessageLookupByLibrary.simpleMessage("فصيلة الدم"),
        "Currentlythe":
            MessageLookupByLibrary.simpleMessage("ما زلت تريد حجز موعد."),
        "Diagnosis": MessageLookupByLibrary.simpleMessage("التشخيص"),
        "Do_you_have_account":
            MessageLookupByLibrary.simpleMessage("هل لديك حساب؟"),
        "Doctor_Profile":
            MessageLookupByLibrary.simpleMessage("الملف الشخصي للطبيب"),
        "Doctors": MessageLookupByLibrary.simpleMessage("الاطباء"),
        "Doctors_List": MessageLookupByLibrary.simpleMessage("قائمة الاطباء"),
        "Good_Morning": MessageLookupByLibrary.simpleMessage("👋 اهلا بك"),
        "Medicine": MessageLookupByLibrary.simpleMessage("الادوية"),
        "Open": MessageLookupByLibrary.simpleMessage("متاح"),
        "Perfect": MessageLookupByLibrary.simpleMessage("التقييم"),
        "Preview_evaluation":
            MessageLookupByLibrary.simpleMessage("أكتب مراجعة"),
        "PreviousAppointments":
            MessageLookupByLibrary.simpleMessage("المعاينات السابقة"),
        "Ratchet_printing":
            MessageLookupByLibrary.simpleMessage("طباعة الراشيتة"),
        "Sorry": MessageLookupByLibrary.simpleMessage("عذرا!"),
        "Specialties": MessageLookupByLibrary.simpleMessage("التخصصات"),
        "Successfully_booked":
            MessageLookupByLibrary.simpleMessage("تم الحجز بنجاح"),
        "Tests_list": MessageLookupByLibrary.simpleMessage("قائمة الاختبارات"),
        "Unavailable": MessageLookupByLibrary.simpleMessage("غير متوفر"),
        "Verify_Code": MessageLookupByLibrary.simpleMessage("رمز التحقق"),
        "View_Profile":
            MessageLookupByLibrary.simpleMessage("عرض الصفحة الشخصية"),
        "Write_review": MessageLookupByLibrary.simpleMessage("أكتب مراجعة"),
        "Years_exp": MessageLookupByLibrary.simpleMessage("سنوات الخبرة"),
        "You_donot_have_any": MessageLookupByLibrary.simpleMessage("ليس لديك "),
        "addAddress": MessageLookupByLibrary.simpleMessage("إضافة عنوان"),
        "addCoupon": MessageLookupByLibrary.simpleMessage("إضافة كوبون"),
        "addToCart": MessageLookupByLibrary.simpleMessage("إضافة الى السلة"),
        "add_address_subtitle": MessageLookupByLibrary.simpleMessage(
            "يمكنك إضافة عنوانك مباشرةً لإستخدامة في عملية الشراء بسهولة."),
        "addedToCart":
            MessageLookupByLibrary.simpleMessage("تمت اضافة المنتج الى السلة."),
        "address": MessageLookupByLibrary.simpleMessage("العنوان"),
        "addressDetails":
            MessageLookupByLibrary.simpleMessage("العنوان بالتفصيل"),
        "addressName": MessageLookupByLibrary.simpleMessage("اسم العنوان:"),
        "address_name_required":
            MessageLookupByLibrary.simpleMessage("يرجى ادخال الاسم."),
        "address_tour": MessageLookupByLibrary.simpleMessage(
            "هنا يمكنك إضافة و تعديل عناوينك الخاصة."),
        "addresses": MessageLookupByLibrary.simpleMessage("العناوين"),
        "all": MessageLookupByLibrary.simpleMessage("الكل"),
        "and": MessageLookupByLibrary.simpleMessage(" و "),
        "app": MessageLookupByLibrary.simpleMessage("التطبيق"),
        "appNotifications":
            MessageLookupByLibrary.simpleMessage("إشعارات التطبيق"),
        "appVersion": MessageLookupByLibrary.simpleMessage("رقم الإصدار"),
        "applyFilter": MessageLookupByLibrary.simpleMessage("تطبيق الفلتر"),
        "appointments_today":
            MessageLookupByLibrary.simpleMessage("مواعيد اليوم!"),
        "arabic": MessageLookupByLibrary.simpleMessage("العربية"),
        "are_you_sure": MessageLookupByLibrary.simpleMessage("هل أنت متأكد."),
        "back": MessageLookupByLibrary.simpleMessage("رجوع"),
        "birthdate_validator":
            MessageLookupByLibrary.simpleMessage("تاريخ الميلاد مطلوب"),
        "blog": MessageLookupByLibrary.simpleMessage("المدونة"),
        "blogs": MessageLookupByLibrary.simpleMessage("المدونة"),
        "bookingappointment": MessageLookupByLibrary.simpleMessage(
            "شكرا لك على تحديد موعد الحجز"),
        "brand": MessageLookupByLibrary.simpleMessage("الماركة"),
        "buyNow": MessageLookupByLibrary.simpleMessage("اشتري الأن"),
        "by_login": MessageLookupByLibrary.simpleMessage(
            "سجَل دخول لتستفيد من مزايا التطبيق"),
        "by_login_dialoge": MessageLookupByLibrary.simpleMessage(
            "عند تسجيل الدخول ستتمكن من الإستفادة من مزايا التطبيق ."),
        "cancel": MessageLookupByLibrary.simpleMessage("إلغاء"),
        "cancelOrder": MessageLookupByLibrary.simpleMessage("إلغاء الطلب"),
        "cancel_order": MessageLookupByLibrary.simpleMessage("الغاء الطلب ؟"),
        "canceled": MessageLookupByLibrary.simpleMessage("ملغي"),
        "cardName": MessageLookupByLibrary.simpleMessage("اسم البطاقة"),
        "cardNumber": MessageLookupByLibrary.simpleMessage("رقم البطاقة"),
        "cart": MessageLookupByLibrary.simpleMessage("السلة"),
        "cartEmpty": MessageLookupByLibrary.simpleMessage("السلة فارغة!"),
        "cart_empty_subtitle": MessageLookupByLibrary.simpleMessage(
            "عند إضافة منتجات إلى السلة ستظهر هنا."),
        "cart_tour": MessageLookupByLibrary.simpleMessage(
            "هنا تجد المنتجات التي تمت إضافتها للسلة لمتابعة عملية الشراء."),
        "categories_tour": MessageLookupByLibrary.simpleMessage(
            "من هنا يمكنك الدخول إلى التصنيفات الرئيسية."),
        "change_address": MessageLookupByLibrary.simpleMessage("تغيير العنوان"),
        "change_password":
            MessageLookupByLibrary.simpleMessage("إعادة تعيين كلمة المرور"),
        "change_password_message": MessageLookupByLibrary.simpleMessage(
            "أدخل عنوان البريد الإلكتروني الذي استخدمته لإنشاء حسابك وسنرسل لك رابطًا عبر البريد الإلكتروني لإعادة تعيين  كلمة المرور الخاصة بك"),
        "change_points_with_products":
            MessageLookupByLibrary.simpleMessage("إستبدال النقاط"),
        "checkoutCart": MessageLookupByLibrary.simpleMessage("شراء السلة"),
        "checkoutSuccess":
            MessageLookupByLibrary.simpleMessage("تمت عملية الشراء بنجاح"),
        "checkoutSuccess_subtitle":
            MessageLookupByLibrary.simpleMessage("شكرا لك. تم وضع طلبك بنجاح."),
        "checkout_tour": MessageLookupByLibrary.simpleMessage(
            "لمتابعة عملية الشراء عن طريق التطبيق وباستخدام معلومات الحساب الشخصي (يتطلب تسجيل دخول)"),
        "checkout_whatsapp_tour_subtitle": MessageLookupByLibrary.simpleMessage(
            " عن طريق ارسال رسالة واتساب للبائع تحوي معلومات السلة دون الحاجة لتسجيل الدخول"),
        "checkout_whatsapp_tour_title":
            MessageLookupByLibrary.simpleMessage("شراء السلة عن طريق الواتساب"),
        "choose_value": MessageLookupByLibrary.simpleMessage("اختر قيمة"),
        "city": MessageLookupByLibrary.simpleMessage("القضاء:"),
        "clear": MessageLookupByLibrary.simpleMessage("مسح"),
        "click_twice":
            MessageLookupByLibrary.simpleMessage("اضغط مرتين للخروج"),
        "code": MessageLookupByLibrary.simpleMessage("الكود"),
        "comment": MessageLookupByLibrary.simpleMessage("التعليق"),
        "commonSearch": MessageLookupByLibrary.simpleMessage("البحث الشائع"),
        "confirm_password":
            MessageLookupByLibrary.simpleMessage("تأكيد كلمة المرور"),
        "continueAsGuest": MessageLookupByLibrary.simpleMessage("الدخول كضيف"),
        "continueShopping":
            MessageLookupByLibrary.simpleMessage("أكمل التسوّق"),
        "continuee": MessageLookupByLibrary.simpleMessage("متابعة"),
        "country": MessageLookupByLibrary.simpleMessage("البلد:"),
        "coupon": MessageLookupByLibrary.simpleMessage("كوبون"),
        "coupon_accepted":
            MessageLookupByLibrary.simpleMessage("تم إستخدام الكوبون."),
        "coupon_rejected":
            MessageLookupByLibrary.simpleMessage("الكوبون غير صالح."),
        "create_account": MessageLookupByLibrary.simpleMessage("أنشئ حساب "),
        "creditCard": MessageLookupByLibrary.simpleMessage("بطاقة ائتمانية"),
        "currency": MessageLookupByLibrary.simpleMessage("العملة"),
        "current_points_balance":
            MessageLookupByLibrary.simpleMessage("رصيد النقاط الحالي"),
        "danger": MessageLookupByLibrary.simpleMessage("تحذير"),
        "dateOfOrder": MessageLookupByLibrary.simpleMessage("تاريخ الطلب : "),
        "delete": MessageLookupByLibrary.simpleMessage("حذف"),
        "delete_all": MessageLookupByLibrary.simpleMessage("حذف الكل"),
        "delete_tour": MessageLookupByLibrary.simpleMessage(
            "لحذف المنتج من السلة, قم بسحب المنتج  "),
        "deliver_address":
            MessageLookupByLibrary.simpleMessage("عنوان الإستلام"),
        "deliveredOrders":
            MessageLookupByLibrary.simpleMessage("طلباتي السابقة"),
        "details": MessageLookupByLibrary.simpleMessage("التفاصيل"),
        "discover": MessageLookupByLibrary.simpleMessage("اكتشف"),
        "dont_have_account":
            MessageLookupByLibrary.simpleMessage("ليس لديك حساب ؟"),
        "dont_have_points_enogh":
            MessageLookupByLibrary.simpleMessage("ليس لديك نقاط كافية"),
        "e_mail": MessageLookupByLibrary.simpleMessage("البريد الالكتروني"),
        "edit": MessageLookupByLibrary.simpleMessage("تعديل"),
        "editProfile":
            MessageLookupByLibrary.simpleMessage("تعديل الملف الشخصي"),
        "edit_profile_tour": MessageLookupByLibrary.simpleMessage(
            "لتعديل المعلومات الشخصية اضغط هنا."),
        "emailOrPasswordWrong": MessageLookupByLibrary.simpleMessage(
            "البريد الإلكتروني أو كلمة المرور غير صحيحة"),
        "emailRequired": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال البريد الإلكتروني"),
        "emailValidator": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال بريد الكتروني صالح"),
        "english": MessageLookupByLibrary.simpleMessage("الإنكليزية"),
        "enterCouponHere":
            MessageLookupByLibrary.simpleMessage("أدخل الكود هنا"),
        "enter_code": MessageLookupByLibrary.simpleMessage(
            "الرجاء إدخال الرمز المرسل للبريد الالكتروني"),
        "evaluation_date":
            MessageLookupByLibrary.simpleMessage("تاريخ التقييم"),
        "experience":
            MessageLookupByLibrary.simpleMessage("أخبر الناس عن تجربتك"),
        "expiryDate": MessageLookupByLibrary.simpleMessage("تاريخ الإنتهاء"),
        "facebook": MessageLookupByLibrary.simpleMessage("فيسبوك"),
        "favorite_tour": MessageLookupByLibrary.simpleMessage(
            "هنا تجد المنتجات التي قمت بإضافتها للمفضلة."),
        "featured_products":
            MessageLookupByLibrary.simpleMessage("منتجات جديدة"),
        "features": MessageLookupByLibrary.simpleMessage("المميزات"),
        "filter": MessageLookupByLibrary.simpleMessage("الفلتر"),
        "forget_password":
            MessageLookupByLibrary.simpleMessage("هل نسيت كلمة المرور ؟"),
        "fullName": MessageLookupByLibrary.simpleMessage("الاسم الكامل"),
        "get_appointment":
            MessageLookupByLibrary.simpleMessage("احصل على موعد"),
        "google": MessageLookupByLibrary.simpleMessage("غوغل"),
        "hand": MessageLookupByLibrary.simpleMessage("ناحية"),
        "haveAnAccount": MessageLookupByLibrary.simpleMessage("لديك حساب ؟ "),
        "height": MessageLookupByLibrary.simpleMessage("الطول"),
        "here": MessageLookupByLibrary.simpleMessage("هنا"),
        "high_price": MessageLookupByLibrary.simpleMessage("الأعلى سعراً"),
        "home": MessageLookupByLibrary.simpleMessage("الرئيسية"),
        "home_subtitle_tour": MessageLookupByLibrary.simpleMessage(
            "هنا تجد المنتجات المميزة و أحدث العروض."),
        "home_title_tour":
            MessageLookupByLibrary.simpleMessage("الصفحة الرئيسية"),
        "info": MessageLookupByLibrary.simpleMessage("معلومة"),
        "ingredients": MessageLookupByLibrary.simpleMessage("المكونات"),
        "language": MessageLookupByLibrary.simpleMessage("اللغة"),
        "leaveAComment":
            MessageLookupByLibrary.simpleMessage("اترك تعليقك هنا"),
        "less_price": MessageLookupByLibrary.simpleMessage("الأقل سعراً"),
        "login": MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
        "mobileValidator":
            MessageLookupByLibrary.simpleMessage("الرجاء التأكد من رقم الهاتف"),
        "mustAddAddress":
            MessageLookupByLibrary.simpleMessage("الرجاء إضافة عنوان."),
        "my_account": MessageLookupByLibrary.simpleMessage("حسابي"),
        "my_addresses": MessageLookupByLibrary.simpleMessage("عناويني"),
        "my_points": MessageLookupByLibrary.simpleMessage("نقاطي"),
        "my_registered_addresses":
            MessageLookupByLibrary.simpleMessage("عناويني المسجلة"),
        "nameRequired":
            MessageLookupByLibrary.simpleMessage("الرجاء إدخال الاسم الكامل"),
        "new_password":
            MessageLookupByLibrary.simpleMessage("كلمة المرور الجديدة"),
        "new_user": MessageLookupByLibrary.simpleMessage("مستخدم جديد"),
        "newest": MessageLookupByLibrary.simpleMessage("الأحدث"),
        "next": MessageLookupByLibrary.simpleMessage("التالي"),
        "no": MessageLookupByLibrary.simpleMessage("لا"),
        "noAddresses": MessageLookupByLibrary.simpleMessage("لا يوجد عناوين"),
        "noInternet":
            MessageLookupByLibrary.simpleMessage("لا يوجد اتصال بالانترنت"),
        "noReviews":
            MessageLookupByLibrary.simpleMessage("لا يوجد تقييمات بعد"),
        "no_favorite": MessageLookupByLibrary.simpleMessage("لا يوجد مفضلة"),
        "no_favorite_subtitle": MessageLookupByLibrary.simpleMessage(
            "يمكنك رؤية جميع المنتجات المفضلة لديك هنا من مكان واحد."),
        "no_notifications":
            MessageLookupByLibrary.simpleMessage("لا يوجد اشعارات"),
        "no_notifications_subtitle": MessageLookupByLibrary.simpleMessage(
            "سيتم إظهار الإشعارات الخاصة بك بمجرد حصول أي حدث جديد."),
        "no_result": MessageLookupByLibrary.simpleMessage("لا يوجد نتائج"),
        "no_result_subtitle": MessageLookupByLibrary.simpleMessage(
            "سيتم إظهار المنتجات هنا عند توفرها مباشرةً."),
        "no_search_subtitle": MessageLookupByLibrary.simpleMessage(
            "يمكنك تجربة البحث بكلمات مختلفة عن المنتجات التي تبحث عنها."),
        "not_loggedin_content": MessageLookupByLibrary.simpleMessage(
            "الرجاء تسجيل الدخول للمتابعة ."),
        "not_loggedin_title":
            MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
        "notes": MessageLookupByLibrary.simpleMessage("ملاحظات"),
        "notifications": MessageLookupByLibrary.simpleMessage("الإشعارات"),
        "numberOfProducts":
            MessageLookupByLibrary.simpleMessage("عدد المنتجات : "),
        "numhelth": MessageLookupByLibrary.simpleMessage("رقم التأمين الصحي:"),
        "offersNotifications":
            MessageLookupByLibrary.simpleMessage("إشعارات العروض"),
        "old": MessageLookupByLibrary.simpleMessage("العمر"),
        "oldest": MessageLookupByLibrary.simpleMessage("الأقدم"),
        "onDelivery": MessageLookupByLibrary.simpleMessage("الدفع عند الباب"),
        "options": MessageLookupByLibrary.simpleMessage("خيارات"),
        "orderHistory": MessageLookupByLibrary.simpleMessage("طلباتي"),
        "orderNumber": MessageLookupByLibrary.simpleMessage("رقم السلة :"),
        "orderPrice": MessageLookupByLibrary.simpleMessage("سعر السلة : "),
        "order_by": MessageLookupByLibrary.simpleMessage("طلب عبر "),
        "order_date": MessageLookupByLibrary.simpleMessage("تاريخ الطلب"),
        "order_details": MessageLookupByLibrary.simpleMessage("تفاصيل الطلب"),
        "order_tour": MessageLookupByLibrary.simpleMessage(
            "هنا يمكنك تتبع حالة الطلبات الخاصة بك ."),
        "password": MessageLookupByLibrary.simpleMessage("كلمة المرور"),
        "passwordConfirm":
            MessageLookupByLibrary.simpleMessage("كلمة المرور غير متطابقة"),
        "passwordConfirmValidator":
            MessageLookupByLibrary.simpleMessage("كلمة المرور غير متطابقة."),
        "passwordValidator": MessageLookupByLibrary.simpleMessage(
            "يجب أن تحوي 8 محارف على الأقل"),
        "pay": MessageLookupByLibrary.simpleMessage("ادفع"),
        "paymentMethod": MessageLookupByLibrary.simpleMessage("طريقة الدفع"),
        "personalInfo":
            MessageLookupByLibrary.simpleMessage("المعلومات الشخصية"),
        "phone_number": MessageLookupByLibrary.simpleMessage("رقم الموبايل"),
        "please_sign_in":
            MessageLookupByLibrary.simpleMessage("الرجاء تسجيل الدخول"),
        "please_sign_up":
            MessageLookupByLibrary.simpleMessage("الرجاء انشاء حساب"),
        "point": MessageLookupByLibrary.simpleMessage("نقطة"),
        "price": MessageLookupByLibrary.simpleMessage("السعر"),
        "privcy": MessageLookupByLibrary.simpleMessage("سياسة الخصوصية"),
        "product_details": MessageLookupByLibrary.simpleMessage("تفاصيل الطلب"),
        "products": MessageLookupByLibrary.simpleMessage("منتجات"),
        "productsList": MessageLookupByLibrary.simpleMessage("قائمة المنتجات"),
        "profile": MessageLookupByLibrary.simpleMessage("الملف الشخصي"),
        "profileImage": MessageLookupByLibrary.simpleMessage("الصورة الشخصية"),
        "profile_tour": MessageLookupByLibrary.simpleMessage(
            "حيث تجد المعلومات الشخصية و الطلبات السابقة لديك."),
        "rate": MessageLookupByLibrary.simpleMessage("التقيم"),
        "rateIt": MessageLookupByLibrary.simpleMessage("تقييم"),
        "rateOrder": MessageLookupByLibrary.simpleMessage("قيم الطلب"),
        "rates": MessageLookupByLibrary.simpleMessage("التقيمات"),
        "readyToGo": MessageLookupByLibrary.simpleMessage("أنت جاهز للإنطلاق"),
        "recent_products":
            MessageLookupByLibrary.simpleMessage("أحدث المنتجات"),
        "related_products":
            MessageLookupByLibrary.simpleMessage("منتجات ذات صلة"),
        "reportDescription": MessageLookupByLibrary.simpleMessage("شرح"),
        "rev_sc": MessageLookupByLibrary.simpleMessage("تمت المراجعة بنجاح"),
        "sale": MessageLookupByLibrary.simpleMessage("الخصم"),
        "salesNotifications":
            MessageLookupByLibrary.simpleMessage("إشعارات التخفيضات"),
        "searchHere": MessageLookupByLibrary.simpleMessage("ابحث هنا"),
        "searchHistory": MessageLookupByLibrary.simpleMessage("تاريخ البحث"),
        "search_tour": MessageLookupByLibrary.simpleMessage(
            "للبحث عن أي منتج مع إمكانية الفلترة حسب (السعر,التصنيف,التقييم..)."),
        "see_all": MessageLookupByLibrary.simpleMessage("مشاهدة الكل"),
        "selectOptions": MessageLookupByLibrary.simpleMessage(
            "الرجاء تحديد خيارات المنتج قبل إضافته للسلة."),
        "select_country": MessageLookupByLibrary.simpleMessage("إختيار دولة"),
        "send": MessageLookupByLibrary.simpleMessage("ارسال"),
        "send_to": MessageLookupByLibrary.simpleMessage("إرسال الى "),
        "sensitivities": MessageLookupByLibrary.simpleMessage("الحساسيات"),
        "settings": MessageLookupByLibrary.simpleMessage("الإعدادات"),
        "shareApp": MessageLookupByLibrary.simpleMessage("مشاركة التطبيق"),
        "share_to_get_points": MessageLookupByLibrary.simpleMessage(
            "شارك التطبيق مع أصدقائك لتحصل على المزيد من النقاط"),
        "shefa_points": MessageLookupByLibrary.simpleMessage("نقاط شفاء "),
        "shefa_points_desc": MessageLookupByLibrary.simpleMessage(
            "تستخدم في تخفيض التكلفة على مشترياتك, حيث يمكنك استبدالهم بمنتجات معينة."),
        "shop_now": MessageLookupByLibrary.simpleMessage("تسوق الآن"),
        "show_order": MessageLookupByLibrary.simpleMessage("عرض السلة"),
        "signOut": MessageLookupByLibrary.simpleMessage("تسجيل الخروج"),
        "sign_in": MessageLookupByLibrary.simpleMessage("تسجيل الدخول"),
        "sign_in_with":
            MessageLookupByLibrary.simpleMessage("  أو سجل الدخول عن طريق  "),
        "sign_up_with":
            MessageLookupByLibrary.simpleMessage("  أو أنشئ حساب عن طريق  "),
        "sign_with_apple":
            MessageLookupByLibrary.simpleMessage("تسجيل الدخول باستخدام Apple"),
        "singup": MessageLookupByLibrary.simpleMessage("أنشئ حساب"),
        "skip": MessageLookupByLibrary.simpleMessage("تخطي"),
        "sort_by": MessageLookupByLibrary.simpleMessage("ترتيب حسب"),
        "startNow": MessageLookupByLibrary.simpleMessage("ابدأ الأن"),
        "state": MessageLookupByLibrary.simpleMessage("المحافظة:"),
        "submit": MessageLookupByLibrary.simpleMessage("تطبيق"),
        "success": MessageLookupByLibrary.simpleMessage("نجاح"),
        "technicalSupport": MessageLookupByLibrary.simpleMessage("الدعم الفني"),
        "thanksForTime": MessageLookupByLibrary.simpleMessage(
            "نشكرك على الوقت الذي أمضيته في إنشاء حسابك. الآن هذا هو الجزء الممتع ، دعنا نستكشف التطبيق."),
        "to_shefa": MessageLookupByLibrary.simpleMessage("في تطبيق شفاء"),
        "top_doctor": MessageLookupByLibrary.simpleMessage("أفضل الاطباء"),
        "total": MessageLookupByLibrary.simpleMessage("الإجمالي"),
        "totalNumber": MessageLookupByLibrary.simpleMessage("العدد الكلي"),
        "total_saving": MessageLookupByLibrary.simpleMessage(
            "كمية التوفير من إستخدام النقاط"),
        "trackOrder": MessageLookupByLibrary.simpleMessage("تعقب الطلب"),
        "tryAgain": MessageLookupByLibrary.simpleMessage("حاول مجدداً"),
        "turkish": MessageLookupByLibrary.simpleMessage("التركية"),
        "use_coupon": MessageLookupByLibrary.simpleMessage("تطبيق"),
        "used_points": MessageLookupByLibrary.simpleMessage("النقاط المستخدمة"),
        "view_details": MessageLookupByLibrary.simpleMessage("عرض التفاصيل"),
        "waitings": MessageLookupByLibrary.simpleMessage("قيد الإنتظار"),
        "warning": MessageLookupByLibrary.simpleMessage("تنبيه"),
        "weight": MessageLookupByLibrary.simpleMessage("الوزن"),
        "welcome":
            MessageLookupByLibrary.simpleMessage("مرحبا بك في تطبيق شفاء"),
        "welcome2": MessageLookupByLibrary.simpleMessage(
            "أفضل تطبيق لمشروعك الصحي والحجز القادم!"),
        "welcome3": MessageLookupByLibrary.simpleMessage("أنت جاهز للبدء!"),
        "welcome4": MessageLookupByLibrary.simpleMessage(
            "نشكرك على الوقت الذي أمضيته في إنشاء حساب معنا. دعنا نستكشف التطبيق."),
        "whatsapp": MessageLookupByLibrary.simpleMessage("واتساب"),
        "yes": MessageLookupByLibrary.simpleMessage("نعم")
      };
}
