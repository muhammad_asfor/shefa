// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "About": MessageLookupByLibrary.simpleMessage("About"),
        "Appointments": MessageLookupByLibrary.simpleMessage("Appointments"),
        "Book_Appointment":
            MessageLookupByLibrary.simpleMessage("Book Appointment"),
        "Certificates": MessageLookupByLibrary.simpleMessage("Certificates"),
        "Clients": MessageLookupByLibrary.simpleMessage("Clients"),
        "Create_account_Step_1":
            MessageLookupByLibrary.simpleMessage("Which one are you?"),
        "Create_account_Step_12": MessageLookupByLibrary.simpleMessage(
            "To give you a prefect experience we need to know your gender"),
        "Create_account_Step_2":
            MessageLookupByLibrary.simpleMessage("How old are you?"),
        "Create_account_Step_3":
            MessageLookupByLibrary.simpleMessage("How weight are you?"),
        "Create_account_Step_32":
            MessageLookupByLibrary.simpleMessage("How height are you?"),
        "Create_account_Step_4":
            MessageLookupByLibrary.simpleMessage("Time to fill"),
        "Create_account_Step_42":
            MessageLookupByLibrary.simpleMessage("your medical history"),
        "Create_account_Step_43":
            MessageLookupByLibrary.simpleMessage("Blood Type"),
        "Currentlythe": MessageLookupByLibrary.simpleMessage(
            "Currently the consulting physician is busy or not the session. You still want to book appointment."),
        "Diagnosis": MessageLookupByLibrary.simpleMessage("Diagnosis"),
        "Do_you_have_account":
            MessageLookupByLibrary.simpleMessage("Do you have account?"),
        "Doctor_Profile":
            MessageLookupByLibrary.simpleMessage("Doctor Profile"),
        "Doctors": MessageLookupByLibrary.simpleMessage("Doctors"),
        "Doctors_List": MessageLookupByLibrary.simpleMessage("Doctors List"),
        "Good_Morning":
            MessageLookupByLibrary.simpleMessage("👋  Good Morning!"),
        "Medicine": MessageLookupByLibrary.simpleMessage("Medicine"),
        "Open": MessageLookupByLibrary.simpleMessage("Open"),
        "Perfect": MessageLookupByLibrary.simpleMessage("Perfect"),
        "Preview_evaluation":
            MessageLookupByLibrary.simpleMessage("Write a review"),
        "PreviousAppointments":
            MessageLookupByLibrary.simpleMessage("Previous Appointments"),
        "Ratchet_printing":
            MessageLookupByLibrary.simpleMessage("Ratchet printing"),
        "Sorry": MessageLookupByLibrary.simpleMessage("Sorry!"),
        "Specialties": MessageLookupByLibrary.simpleMessage("Specialties"),
        "Successfully_booked":
            MessageLookupByLibrary.simpleMessage("Successfully booked"),
        "Tests_list": MessageLookupByLibrary.simpleMessage("Tests list"),
        "Unavailable": MessageLookupByLibrary.simpleMessage("Unavailable"),
        "Verify_Code": MessageLookupByLibrary.simpleMessage("Verify Code"),
        "View_Profile": MessageLookupByLibrary.simpleMessage("View Profile"),
        "Write_review": MessageLookupByLibrary.simpleMessage("Write a review"),
        "Years_exp": MessageLookupByLibrary.simpleMessage("Years exp"),
        "You_donot_have_any":
            MessageLookupByLibrary.simpleMessage("You don’t have any"),
        "addAddress": MessageLookupByLibrary.simpleMessage("Add address"),
        "addCoupon": MessageLookupByLibrary.simpleMessage("Add a coupon"),
        "addToCart": MessageLookupByLibrary.simpleMessage("Add to cart"),
        "add_address_subtitle": MessageLookupByLibrary.simpleMessage(
            "You can add your address directly to be used in the purchase process easily."),
        "addedToCart": MessageLookupByLibrary.simpleMessage(
            "The product has been added to the cart."),
        "address": MessageLookupByLibrary.simpleMessage("Address"),
        "addressDetails":
            MessageLookupByLibrary.simpleMessage("Address in detail"),
        "addressName": MessageLookupByLibrary.simpleMessage("Address name:"),
        "address_name_required":
            MessageLookupByLibrary.simpleMessage("name required."),
        "address_tour": MessageLookupByLibrary.simpleMessage(
            "Here you can add and edit your own addresses."),
        "addresses": MessageLookupByLibrary.simpleMessage("Addresses"),
        "all": MessageLookupByLibrary.simpleMessage("All"),
        "and": MessageLookupByLibrary.simpleMessage(" And "),
        "app": MessageLookupByLibrary.simpleMessage("shefa"),
        "appNotifications":
            MessageLookupByLibrary.simpleMessage("App notifications"),
        "appVersion": MessageLookupByLibrary.simpleMessage("Version number"),
        "applyFilter": MessageLookupByLibrary.simpleMessage("Apply filter"),
        "appointments_today":
            MessageLookupByLibrary.simpleMessage("appointments today!"),
        "arabic": MessageLookupByLibrary.simpleMessage("Arabic"),
        "are_you_sure": MessageLookupByLibrary.simpleMessage("Are you sure."),
        "back": MessageLookupByLibrary.simpleMessage("Back"),
        "birthdate_validator":
            MessageLookupByLibrary.simpleMessage("birthdate required."),
        "blog": MessageLookupByLibrary.simpleMessage("blog"),
        "blogs": MessageLookupByLibrary.simpleMessage("Blog"),
        "bookingappointment": MessageLookupByLibrary.simpleMessage(
            "Thank you for booking appointment"),
        "brand": MessageLookupByLibrary.simpleMessage("Brand"),
        "buyNow": MessageLookupByLibrary.simpleMessage("buy now"),
        "by_login": MessageLookupByLibrary.simpleMessage(
            "By login you can enjoy all shefa\'s features."),
        "by_login_dialoge": MessageLookupByLibrary.simpleMessage(
            "By login you can use all app features."),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "cancelOrder": MessageLookupByLibrary.simpleMessage("Cancel order"),
        "cancel_order":
            MessageLookupByLibrary.simpleMessage("Cancel the order?"),
        "canceled": MessageLookupByLibrary.simpleMessage("Cancelled"),
        "cardName":
            MessageLookupByLibrary.simpleMessage("The name of the card"),
        "cardNumber": MessageLookupByLibrary.simpleMessage("Card number"),
        "cart": MessageLookupByLibrary.simpleMessage("Cart"),
        "cartEmpty": MessageLookupByLibrary.simpleMessage("The cart is empty!"),
        "cart_empty_subtitle": MessageLookupByLibrary.simpleMessage(
            "Products added to the cart will be shown here."),
        "cart_tour": MessageLookupByLibrary.simpleMessage(
            "Here you will find the products that have been added to the cart to continue the purchase process."),
        "categories_tour": MessageLookupByLibrary.simpleMessage(
            "From here you can access the main categories."),
        "change_address":
            MessageLookupByLibrary.simpleMessage("Change address"),
        "change_password":
            MessageLookupByLibrary.simpleMessage("Reset password"),
        "change_password_message": MessageLookupByLibrary.simpleMessage(
            "Enter the email address you used to create your account and we\'ll email you a link to reset your password"),
        "change_points_with_products":
            MessageLookupByLibrary.simpleMessage("Exchange Points"),
        "checkoutCart": MessageLookupByLibrary.simpleMessage("Purchase Cart"),
        "checkoutSuccess": MessageLookupByLibrary.simpleMessage(
            "Purchase completed successfully"),
        "checkoutSuccess_subtitle": MessageLookupByLibrary.simpleMessage(
            "Thank you. Your request has been placed successfully."),
        "checkout_tour": MessageLookupByLibrary.simpleMessage(
            "To proceed with the purchase via the app and using personal account information (login required)"),
        "checkout_whatsapp_tour_subtitle": MessageLookupByLibrary.simpleMessage(
            "By sending a WhatsApp message to the seller containing the basket information without the need to log in"),
        "checkout_whatsapp_tour_title":
            MessageLookupByLibrary.simpleMessage("Purchase cart via WhatsApp"),
        "choose_value": MessageLookupByLibrary.simpleMessage("Choose value"),
        "city": MessageLookupByLibrary.simpleMessage("spend:"),
        "clear": MessageLookupByLibrary.simpleMessage("clear"),
        "click_twice":
            MessageLookupByLibrary.simpleMessage("Double click to exit"),
        "code": MessageLookupByLibrary.simpleMessage("Code"),
        "comment": MessageLookupByLibrary.simpleMessage("Comment"),
        "commonSearch": MessageLookupByLibrary.simpleMessage("Common search"),
        "confirm_password":
            MessageLookupByLibrary.simpleMessage("Confirm password"),
        "continueAsGuest":
            MessageLookupByLibrary.simpleMessage("Login as guest"),
        "continueShopping":
            MessageLookupByLibrary.simpleMessage("Continue shopping"),
        "continuee": MessageLookupByLibrary.simpleMessage("Continue"),
        "country": MessageLookupByLibrary.simpleMessage("Country:"),
        "coupon": MessageLookupByLibrary.simpleMessage("Coupon"),
        "coupon_accepted":
            MessageLookupByLibrary.simpleMessage("Coupon Applied."),
        "coupon_rejected":
            MessageLookupByLibrary.simpleMessage("Coupon is not valid."),
        "create_account":
            MessageLookupByLibrary.simpleMessage("Create an account"),
        "creditCard": MessageLookupByLibrary.simpleMessage("Credit card"),
        "currency": MessageLookupByLibrary.simpleMessage("Currency"),
        "current_points_balance":
            MessageLookupByLibrary.simpleMessage("current point balance"),
        "danger": MessageLookupByLibrary.simpleMessage("danger"),
        "dateOfOrder": MessageLookupByLibrary.simpleMessage("DateOfOrder: "),
        "delete": MessageLookupByLibrary.simpleMessage("Delete"),
        "delete_all": MessageLookupByLibrary.simpleMessage("Delete all"),
        "delete_tour": MessageLookupByLibrary.simpleMessage(
            "To delete the product from the cart, Swip the product"),
        "deliver_address":
            MessageLookupByLibrary.simpleMessage("Delivery Address"),
        "deliveredOrders":
            MessageLookupByLibrary.simpleMessage("My previous orders"),
        "details": MessageLookupByLibrary.simpleMessage("Details"),
        "discover": MessageLookupByLibrary.simpleMessage("Discover"),
        "dont_have_account":
            MessageLookupByLibrary.simpleMessage("don\'t have an account ?"),
        "dont_have_points_enogh": MessageLookupByLibrary.simpleMessage(
            "you don\'t have enough points"),
        "e_mail": MessageLookupByLibrary.simpleMessage("E-mail"),
        "edit": MessageLookupByLibrary.simpleMessage("edit"),
        "editProfile": MessageLookupByLibrary.simpleMessage("Edit Profile"),
        "edit_profile_tour": MessageLookupByLibrary.simpleMessage(
            "To modify personal information, click here."),
        "emailOrPasswordWrong": MessageLookupByLibrary.simpleMessage(
            "The password or e-mail isn\'t correct"),
        "emailRequired":
            MessageLookupByLibrary.simpleMessage("Please enter an email"),
        "emailValidator":
            MessageLookupByLibrary.simpleMessage("Please enter a valid email"),
        "english": MessageLookupByLibrary.simpleMessage("English"),
        "enterCouponHere":
            MessageLookupByLibrary.simpleMessage("Enter the code here"),
        "enter_code": MessageLookupByLibrary.simpleMessage(
            "Please enter the code sent to the e-mail"),
        "evaluation_date":
            MessageLookupByLibrary.simpleMessage("Evaluation date"),
        "experience": MessageLookupByLibrary.simpleMessage(
            "Tell people about your experience"),
        "expiryDate": MessageLookupByLibrary.simpleMessage("Expiration date"),
        "facebook": MessageLookupByLibrary.simpleMessage("Facebook"),
        "favorite_tour": MessageLookupByLibrary.simpleMessage(
            "Here you will find the products you have added to your favourites."),
        "featured_products":
            MessageLookupByLibrary.simpleMessage("New products"),
        "features": MessageLookupByLibrary.simpleMessage("Features"),
        "filter": MessageLookupByLibrary.simpleMessage("Filter"),
        "forget_password":
            MessageLookupByLibrary.simpleMessage("Forgot your password?"),
        "fullName": MessageLookupByLibrary.simpleMessage("Full name"),
        "get_appointment":
            MessageLookupByLibrary.simpleMessage("Get Appointment"),
        "google": MessageLookupByLibrary.simpleMessage("Google"),
        "hand": MessageLookupByLibrary.simpleMessage("hand"),
        "haveAnAccount":
            MessageLookupByLibrary.simpleMessage("Have an account?"),
        "height": MessageLookupByLibrary.simpleMessage("height"),
        "here": MessageLookupByLibrary.simpleMessage("Here"),
        "high_price": MessageLookupByLibrary.simpleMessage("Highest price"),
        "home": MessageLookupByLibrary.simpleMessage("Home"),
        "home_subtitle_tour": MessageLookupByLibrary.simpleMessage(
            "Here you will find the featured products and the latest offers."),
        "home_title_tour": MessageLookupByLibrary.simpleMessage("Home page"),
        "info": MessageLookupByLibrary.simpleMessage("info"),
        "ingredients": MessageLookupByLibrary.simpleMessage("Ingredients"),
        "language": MessageLookupByLibrary.simpleMessage("Language"),
        "leaveAComment":
            MessageLookupByLibrary.simpleMessage("Leave your comment here"),
        "less_price": MessageLookupByLibrary.simpleMessage("Less price"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "mobileValidator": MessageLookupByLibrary.simpleMessage(
            "Please verify the phone number"),
        "mustAddAddress":
            MessageLookupByLibrary.simpleMessage("Please add an address."),
        "my_account": MessageLookupByLibrary.simpleMessage("Profile"),
        "my_addresses": MessageLookupByLibrary.simpleMessage("My Addresses"),
        "my_points": MessageLookupByLibrary.simpleMessage("My Points"),
        "my_registered_addresses":
            MessageLookupByLibrary.simpleMessage("Registered Addresses"),
        "nameRequired":
            MessageLookupByLibrary.simpleMessage("Please enter your full name"),
        "new_password": MessageLookupByLibrary.simpleMessage("New password"),
        "new_user": MessageLookupByLibrary.simpleMessage("New user?"),
        "newest": MessageLookupByLibrary.simpleMessage("Newest"),
        "next": MessageLookupByLibrary.simpleMessage("Next"),
        "no": MessageLookupByLibrary.simpleMessage("No"),
        "noAddresses": MessageLookupByLibrary.simpleMessage("No addresses"),
        "noInternet":
            MessageLookupByLibrary.simpleMessage("No internet connection"),
        "noReviews": MessageLookupByLibrary.simpleMessage("No reviews yet"),
        "no_favorite": MessageLookupByLibrary.simpleMessage("No Favorites"),
        "no_favorite_subtitle": MessageLookupByLibrary.simpleMessage(
            "You can see all your favorite products here from one place."),
        "no_notifications":
            MessageLookupByLibrary.simpleMessage("No Notifications"),
        "no_notifications_subtitle": MessageLookupByLibrary.simpleMessage(
            "Your notifications will appear as soon as any new event occurs."),
        "no_result": MessageLookupByLibrary.simpleMessage("No Results"),
        "no_result_subtitle": MessageLookupByLibrary.simpleMessage(
            "Products will be shown here immediately when available."),
        "no_search_subtitle": MessageLookupByLibrary.simpleMessage(
            "You can try searching with different words for the products you are looking for."),
        "not_loggedin_content":
            MessageLookupByLibrary.simpleMessage("Please login to continue."),
        "not_loggedin_title": MessageLookupByLibrary.simpleMessage("Logged in"),
        "notes": MessageLookupByLibrary.simpleMessage("Notes"),
        "notifications": MessageLookupByLibrary.simpleMessage("Notifications"),
        "numberOfProducts":
            MessageLookupByLibrary.simpleMessage("Number of products : "),
        "numhelth":
            MessageLookupByLibrary.simpleMessage("Health insurance number:"),
        "offersNotifications":
            MessageLookupByLibrary.simpleMessage("Offers notices"),
        "old": MessageLookupByLibrary.simpleMessage("old"),
        "oldest": MessageLookupByLibrary.simpleMessage("Oldest"),
        "onDelivery": MessageLookupByLibrary.simpleMessage("Pay at the door"),
        "options": MessageLookupByLibrary.simpleMessage("Options"),
        "orderHistory": MessageLookupByLibrary.simpleMessage("My orders"),
        "orderNumber": MessageLookupByLibrary.simpleMessage("Cart number:"),
        "orderPrice": MessageLookupByLibrary.simpleMessage("Cart price:"),
        "order_by": MessageLookupByLibrary.simpleMessage("Checkout by "),
        "order_date": MessageLookupByLibrary.simpleMessage("Order Date"),
        "order_details": MessageLookupByLibrary.simpleMessage("Order Details"),
        "order_tour": MessageLookupByLibrary.simpleMessage(
            "Here you can track the status of your orders."),
        "password": MessageLookupByLibrary.simpleMessage("Password"),
        "passwordConfirm":
            MessageLookupByLibrary.simpleMessage("Password does not match"),
        "passwordConfirmValidator": MessageLookupByLibrary.simpleMessage(
            "The password does not match."),
        "passwordValidator": MessageLookupByLibrary.simpleMessage(
            "Must contain at least 8 characters"),
        "pay": MessageLookupByLibrary.simpleMessage("Pay"),
        "paymentMethod": MessageLookupByLibrary.simpleMessage("Payment method"),
        "personalInfo":
            MessageLookupByLibrary.simpleMessage("Personal information"),
        "phone_number": MessageLookupByLibrary.simpleMessage("Phone number"),
        "please_sign_in":
            MessageLookupByLibrary.simpleMessage("Please sign in"),
        "please_sign_up":
            MessageLookupByLibrary.simpleMessage("Please create an account"),
        "point": MessageLookupByLibrary.simpleMessage("Point"),
        "price": MessageLookupByLibrary.simpleMessage("Price"),
        "product_details":
            MessageLookupByLibrary.simpleMessage("Product Details"),
        "products": MessageLookupByLibrary.simpleMessage("Products"),
        "productsList": MessageLookupByLibrary.simpleMessage("Product list"),
        "profile": MessageLookupByLibrary.simpleMessage("Profile"),
        "profileImage": MessageLookupByLibrary.simpleMessage("Profile"),
        "profile_tour": MessageLookupByLibrary.simpleMessage(
            "Where you will find your personal information and previous requests."),
        "rate": MessageLookupByLibrary.simpleMessage("Rate"),
        "rateIt": MessageLookupByLibrary.simpleMessage("Rate"),
        "rateOrder": MessageLookupByLibrary.simpleMessage("Order values"),
        "rates": MessageLookupByLibrary.simpleMessage("Reviews"),
        "readyToGo":
            MessageLookupByLibrary.simpleMessage("You\'re ready to go"),
        "recent_products":
            MessageLookupByLibrary.simpleMessage("Latest products"),
        "related_products":
            MessageLookupByLibrary.simpleMessage("Related Products"),
        "reportDescription":
            MessageLookupByLibrary.simpleMessage("Description"),
        "rev_sc": MessageLookupByLibrary.simpleMessage(
            "The review was completed successfully"),
        "sale": MessageLookupByLibrary.simpleMessage("Discount"),
        "salesNotifications":
            MessageLookupByLibrary.simpleMessage("Sales notifications"),
        "searchHere": MessageLookupByLibrary.simpleMessage("Search here"),
        "searchHistory": MessageLookupByLibrary.simpleMessage("Search history"),
        "search_tour": MessageLookupByLibrary.simpleMessage(
            "To search for any product with the ability to filter by (price, rating, rating..)."),
        "see_all": MessageLookupByLibrary.simpleMessage("see all"),
        "selectOptions": MessageLookupByLibrary.simpleMessage(
            "Select product options before adding it to the cart."),
        "select_country":
            MessageLookupByLibrary.simpleMessage("select country"),
        "send": MessageLookupByLibrary.simpleMessage("send"),
        "send_to": MessageLookupByLibrary.simpleMessage("Send to "),
        "sensitivities": MessageLookupByLibrary.simpleMessage("Sensitivities"),
        "settings": MessageLookupByLibrary.simpleMessage("Settings"),
        "shareApp": MessageLookupByLibrary.simpleMessage("Share App"),
        "share_to_get_points": MessageLookupByLibrary.simpleMessage(
            "Share the app to get more points"),
        "shefa_points": MessageLookupByLibrary.simpleMessage("shefa points "),
        "shefa_points_desc": MessageLookupByLibrary.simpleMessage(
            "Used to reduce the cost of your purchases, you can exchange it with products."),
        "shop_now": MessageLookupByLibrary.simpleMessage("Shop now"),
        "show_order": MessageLookupByLibrary.simpleMessage("Order Detials"),
        "signOut": MessageLookupByLibrary.simpleMessage("Sign Out"),
        "sign_in": MessageLookupByLibrary.simpleMessage("Sign In"),
        "sign_in_with":
            MessageLookupByLibrary.simpleMessage(" Or sign in with "),
        "sign_up_with":
            MessageLookupByLibrary.simpleMessage("Or create an account with "),
        "sign_with_apple":
            MessageLookupByLibrary.simpleMessage("Login with Apple"),
        "singup": MessageLookupByLibrary.simpleMessage("Create an account"),
        "skip": MessageLookupByLibrary.simpleMessage("Skip"),
        "sort_by": MessageLookupByLibrary.simpleMessage("Sort by"),
        "startNow": MessageLookupByLibrary.simpleMessage("Start now"),
        "state": MessageLookupByLibrary.simpleMessage("Governorate:"),
        "submit": MessageLookupByLibrary.simpleMessage("Apply"),
        "success": MessageLookupByLibrary.simpleMessage("success"),
        "technicalSupport":
            MessageLookupByLibrary.simpleMessage("Technical Support"),
        "thanksForTime": MessageLookupByLibrary.simpleMessage(
            "Thank you for the time you took to create your account. Now that\'s the fun part, let\'s explore the app."),
        "to_shefa": MessageLookupByLibrary.simpleMessage("in shefa"),
        "top_doctor": MessageLookupByLibrary.simpleMessage("Top Doctors"),
        "total": MessageLookupByLibrary.simpleMessage("Total"),
        "totalNumber": MessageLookupByLibrary.simpleMessage("Total number"),
        "total_saving": MessageLookupByLibrary.simpleMessage(
            "the amount of savings from using points"),
        "trackOrder": MessageLookupByLibrary.simpleMessage("Track the order"),
        "tryAgain": MessageLookupByLibrary.simpleMessage("Try again"),
        "turkish": MessageLookupByLibrary.simpleMessage("Turkish"),
        "use_coupon": MessageLookupByLibrary.simpleMessage("apply"),
        "used_points": MessageLookupByLibrary.simpleMessage("used points"),
        "view_details": MessageLookupByLibrary.simpleMessage("View details"),
        "waitings": MessageLookupByLibrary.simpleMessage("Pending"),
        "warning": MessageLookupByLibrary.simpleMessage("warning"),
        "weight": MessageLookupByLibrary.simpleMessage("weight"),
        "welcome":
            MessageLookupByLibrary.simpleMessage("Welcome To Shifaa App"),
        "welcome2": MessageLookupByLibrary.simpleMessage(
            "The best App for your next health and booking project!"),
        "welcome3":
            MessageLookupByLibrary.simpleMessage("You are ready to start!"),
        "welcome4": MessageLookupByLibrary.simpleMessage(
            "Thanks for taking your time to create account with us. Let’s explore the app."),
        "whatsapp": MessageLookupByLibrary.simpleMessage("Whatsapp"),
        "yes": MessageLookupByLibrary.simpleMessage("Yes")
      };
}
