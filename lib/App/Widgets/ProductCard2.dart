import 'package:flutter/material.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/Ui/Blog/presentation/widgets/BlogDetailsPage.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import 'package:html/parser.dart';

class ProductCard2 extends StatefulWidget {
  final Blog product;
  final bool forPointSale;

  ProductCard2({required this.product, Key? key, this.forPointSale: false})
      : super(key: key);

  @override
  _ProductCard2State createState() => _ProductCard2State(product);
}

class _ProductCard2State extends State<ProductCard2> {
  _ProductCard2State(this.product);

  final Blog product;
  bool loadingCart = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => BlogsDetailsPage(
                  id: product.id.toString(),
                )));
      },
      child: Padding(
        padding: EdgeInsets.only(bottom: 20),
        child: Container(
          width: double.infinity,
          height: SizeConfig.h(100),
          decoration: BoxDecoration(
              color: AppStyle.whiteColor,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 10,
                    color: Colors.black12)
              ]),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: SizedBox(
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(
                            product.coverImage,
                            fit: BoxFit.fill,
                          ))),
                ),
                Expanded(
                  flex: 3,
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            child: Text(
                          product.title,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF001133),
                          ),
                          overflow: TextOverflow.ellipsis,
                        )),
                        Expanded(
                            child: Text(
                          parse(parse(product.description).body!.text)
                              .documentElement!
                              .text,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.normal,
                            color: Color(0xFF8E8E8E),
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        )),
                        Container(
                          height: SizeConfig.h(12),
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
        ),
      ),
    );
  }

  final debouncer = Debouncer(milliseconds: 1000);
}
