import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import '/App/Widgets/MainButton.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';
import '/injections.dart';

import '../../constants.dart';

class ProductCardTest extends StatefulWidget {
  final TopDoctor product;
  final bool forPointSale;

  ProductCardTest({required this.product, Key? key, this.forPointSale: false})
      : super(key: key);

  @override
  _ProductCardTestState createState() => _ProductCardTestState(product);
}

class _ProductCardTestState extends State<ProductCardTest> {
  _ProductCardTestState(this.product);

  final TopDoctor product;
  bool loadingCart = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          "/productDetails",
          arguments: {"id": product.id.toString(), "goToOptions": false},
        );
      },
      child: Padding(
        padding: EdgeInsets.only(bottom: 20),
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 6.5),
          width: SizeConfig.w(285),
          height: SizeConfig.h(149),
          decoration: BoxDecoration(
              color: AppStyle.whiteColor,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 10,
                    color: Colors.black12)
              ]),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: SizedBox(
                      height: double.infinity,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(
                            product.coverImage,
                            fit: BoxFit.fill,
                            errorBuilder: (context, o, s) {
                              return Icon(Icons.warning);
                            },
                          ))),
                ),
                SizedBox(width: SizeConfig.w(10)),
                Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: SizeConfig.h(140),
                        padding: EdgeInsets.all(10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Text(
                                product.speciality?.title ?? "",
                                style: TextStyle(
                                  fontSize: SizeConfig.h(12),
                                  color: AppStyle.primaryColor,
                                ),
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(
                              height: SizeConfig.h(2.5),
                            ),
                            Text(
                              product.name,
                              style: TextStyle(
                                fontSize: SizeConfig.h(14),
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            SizedBox(
                              height: SizeConfig.h(2.5),
                            ),
                            if (product.doctor_card != null)
                              Expanded(
                                child: Row(
                                  children: [
                                    Text(
                                      ((product.doctor_card?.start_work_time!
                                                      .split(":")[0] ??
                                                  '') +
                                              ":" +
                                              (product.doctor_card
                                                      ?.start_work_time!
                                                      .split(":")[1] ??
                                                  ''))
                                          .toString(),
                                      style: TextStyle(
                                          fontSize: SizeConfig.h(10),
                                          color: AppStyle.greyColor),
                                      maxLines: 1,
                                    ),
                                    Text(
                                      "  -  ",
                                      style: TextStyle(
                                          fontSize: SizeConfig.h(10),
                                          color: AppStyle.greyColor),
                                      maxLines: 1,
                                    ),
                                    Text(
                                      ((product.doctor_card?.end_work_time!
                                                  .split(":")[0] ??
                                              '') +
                                          ":" +
                                          (product.doctor_card?.end_work_time!
                                                  .split(":")[1] ??
                                              '')),
                                      style: TextStyle(
                                          fontSize: SizeConfig.h(10),
                                          color: AppStyle.greyColor),
                                      maxLines: 1,
                                    )
                                  ],
                                ),
                              ),
                            SizedBox(
                              height: SizeConfig.h(2.5),
                            ),
                            Expanded(
                              child: RatingBar.builder(
                                initialRating: product.user_rating.toDouble(),
                                direction: Axis.horizontal,
                                allowHalfRating: true,
                                itemCount: 5,
                                itemSize: SizeConfig.h(16),
                                itemPadding:
                                    EdgeInsets.symmetric(horizontal: 0.0),
                                itemBuilder: (context, _) => Icon(
                                  Icons.star,
                                  color: Colors.amber,
                                ),
                                updateOnDrag: true,
                                glow: false,
                                ignoreGestures: true,
                                onRatingUpdate: (rating) {},
                              ),
                            ),
                            SizedBox(
                              height: SizeConfig.h(10),
                            ),
                            Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(SizeConfig.h(30)),
                                  gradient: LinearGradient(
                                    colors: [
                                      AppStyle.primaryColor,
                                      AppStyle.secondaryColor,
                                    ],
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                  ),
                                ),
                                height: SizeConfig.h(30),
                                width: SizeConfig.w(136),
                                child: Center(
                                  child: Text(
                                    S.of(context).get_appointment,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 10),
                                  ),
                                )),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ]),
        ),
      ),
    );
  }

  final debouncer = Debouncer(milliseconds: 1000);
}
