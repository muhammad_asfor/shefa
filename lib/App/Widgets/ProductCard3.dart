import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import 'package:tajra/Ui/ProductDetails/reservations.dart';
import '/App/Widgets/MainButton.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';
import '/injections.dart';
import 'package:flutter_svg/svg.dart';

import '../../constants.dart';

class ProductCard3 extends StatefulWidget {
  final TopDoctor product;
  final bool forPointSale;

  ProductCard3({required this.product, Key? key, this.forPointSale: false})
      : super(key: key);

  @override
  _ProductCard3State createState() => _ProductCard3State(product);
}

class _ProductCard3State extends State<ProductCard3> {
  _ProductCard3State(this.product);

  final TopDoctor product;
  bool loadingCart = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          "/productDetails",
          arguments: {"id": product.id.toString(), "goToOptions": false},
        );
      },
      child: Padding(
        padding: EdgeInsets.only(
            right: SizeConfig.w(29),
            left: SizeConfig.w(29),
            bottom: SizeConfig.h(14)),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: SizeConfig.h(75),
              decoration: BoxDecoration(
                  color: AppStyle.whiteColor,
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 0),
                        blurRadius: 10,
                        color: Colors.black12)
                  ]),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: SizedBox(),
                    ),
                    Expanded(
                      flex: 3,
                      child: SizedBox(
                        height: SizeConfig.h(45),
                        width: SizeConfig.h(45),
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                            product.coverImage,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 15,
                      child: Container(
                        height: SizeConfig.h(75),
                        padding: EdgeInsets.all(14),
                        child: Column(
                          children: [
                            Expanded(
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    product.name,
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold,
                                      color: AppStyle.greyDarktext,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ))
                                ],
                              ),
                            ),
                            Expanded(
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    product.speciality?.title ?? "",
                                    style: TextStyle(
                                      fontSize: 12,
                                      color: AppStyle.greyColor,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  ))
                                ],
                              ),
                            ),
                            Expanded(
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.star,
                                    color: Colors.yellow,
                                    size: SizeConfig.h(16),
                                  ),
                                  SizedBox(
                                    width: 2,
                                  ),
                                  Text(
                                    product.user_rating.toString(),
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                      color: AppStyle.greyDarktext,
                                    ),
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
            ),
            Container(
              height: SizeConfig.h(40),
              width: SizeConfig.w(285),
              decoration: BoxDecoration(
                  color: AppStyle.grediantLightColor,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 10),
                        blurRadius: 10,
                        color: Colors.black12)
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SvgPicture.asset("assets/bookApp.svg",
                            width: SizeConfig.w(24),
                            color: Colors.white,
                            height: SizeConfig.w(24)),
                        SizedBox(
                          width: SizeConfig.w(2),
                        ),
                        Text(
                          S.of(context).View_Profile,
                          style: TextStyle(
                              color: Colors.white, fontSize: SizeConfig.w(11)),
                        )
                      ],
                    ),
                  ),
                  Container(
                    color: Colors.white,
                    height: 50,
                    width: 2,
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () async {
                        if (sl<AuthBloc>().isGuest) {
                          showLoginDialoge(context);
                        } else {
                          var result = await showDatePicker(
                              context: context,
                              initialDate: DateTime.now(),
                              firstDate: DateTime.now(),
                              lastDate: DateTime(2050));
                          if (result != null)
                            showSelectreservationsSheet(
                                    context,
                                    product.id.toString(),
                                    result.toString().split(" ")[0])
                                .then((value) {
                              if (value != null) {
                                setState(() {});
                              }
                            });
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset("assets/view .svg",
                              width: SizeConfig.w(24),
                              color: Colors.white,
                              height: SizeConfig.h(24)),
                          SizedBox(
                            width: SizeConfig.w(2),
                          ),
                          Text(
                            S.of(context).Book_Appointment,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: SizeConfig.w(11)),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  final debouncer = Debouncer(milliseconds: 1000);
}
