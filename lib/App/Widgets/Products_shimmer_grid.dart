import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:tajra/App/Widgets/ShimmerProductCardlistDoc.dart';
import '/App/Widgets/ShimmerProductCard.dart';
import '/Utils/SizeConfig.dart';

class ProductsShimmerGrid extends StatelessWidget {
  const ProductsShimmerGrid({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: SizeConfig.screenHeight,
          child: ListView.builder(
              padding: EdgeInsets.symmetric(horizontal: SizeConfig.h(3)),
              scrollDirection: Axis.vertical,
              itemCount: 10,
              itemBuilder: (context, index) {
                return Padding(
                  padding: EdgeInsets.only(top: 12),
                  child: ProductShimmerCardListDoc(),
                );
              }),
        ),
      ],
    );
  }
}
