import 'package:flutter/material.dart';
import 'package:tajra/Ui/appointments_all/AppointmentDetailsPage.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';

class ProductCard5 extends StatefulWidget {
  final Map<dynamic, dynamic> product;

  ProductCard5({
    required this.product,
    Key? key,
  }) : super(key: key);

  @override
  _ProductCard5State createState() => _ProductCard5State(product);
}

class _ProductCard5State extends State<ProductCard5> {
  _ProductCard5State(this.product);

  final Map<dynamic, dynamic> product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  AppointmentDetailsPage(appointment: product)),
        );
      },
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: SizeConfig.h(75),
            decoration: BoxDecoration(
                color: AppStyle.whiteColor,
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 0),
                      blurRadius: 10,
                      color: Colors.black12)
                ]),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: SizedBox(),
                  ),
                  Expanded(
                    flex: 3,
                    child: SizedBox(
                      height: SizeConfig.h(45),
                      width: SizeConfig.h(45),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                          product["doctor"]["cover_image"],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 12,
                    child: Container(
                      height: SizeConfig.h(75),
                      padding: EdgeInsets.all(14),
                      child: Column(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  product["doctor"]["name"],
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: AppStyle.greyDarktext,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ))
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  product["doctor"]["speciality"] != null
                                      ? product["doctor"]["speciality"]["title"]
                                      : "",
                                  style: TextStyle(
                                    height: 1.1,
                                    fontSize: 10,
                                    color: AppStyle.greyColor,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ))
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Text(
                                  product["date"].toString() +
                                      "  " +
                                      product["time"].toString().split(":")[0] +
                                      ":" +
                                      product["time"].toString().split(":")[1],
                                  style: TextStyle(
                                    height: 1.1,
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold,
                                    color: AppStyle.primaryColor,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: SizedBox(
                        height: SizeConfig.h(45),
                        width: SizeConfig.h(45),
                        child: Icon(
                          Icons.print_outlined,
                          color: AppStyle.primaryColor,
                        )),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}
