import 'package:flutter/material.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';

class ProductCard4 extends StatefulWidget {
  final Map<dynamic, dynamic> product;

  ProductCard4({
    required this.product,
    Key? key,
  }) : super(key: key);

  @override
  _ProductCard4State createState() => _ProductCard4State(product);
}

class _ProductCard4State extends State<ProductCard4> {
  _ProductCard4State(this.product);

  final Map<dynamic, dynamic> product;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          "/productDetails",
          arguments: {
            "id": product["doctor"]["id"].toString(),
            "goToOptions": false
          },
        );
      },
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: SizeConfig.h(75),
            decoration: BoxDecoration(
                color: AppStyle.whiteColor,
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 0),
                      blurRadius: 10,
                      color: Colors.black12)
                ]),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: SizedBox(),
                  ),
                  Expanded(
                    flex: 3,
                    child: SizedBox(
                      height: SizeConfig.h(45), width: SizeConfig.h(45),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                          product["doctor"]["cover_image"],
                        ),
                      ),
                      //  ClipRRect(
                      //     borderRadius: BorderRadius.circular(10),
                      //     child: Image.network(
                      //       product.coverImage,
                      //       fit: BoxFit.cover,
                      //       errorBuilder: (context, o, s) {
                      //         return Icon(Icons.warning);
                      //       },
                      //     ))
                    ),
                  ),
                  Expanded(
                    flex: 15,
                    child: Container(
                      height: SizeConfig.h(75),
                      padding: EdgeInsets.all(14),
                      child: Column(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  product["doctor"]["name"] ?? "",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: AppStyle.greyDarktext,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ))
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  product["doctor"]["speciality"] != null
                                      ? product["doctor"]["speciality"]["title"]
                                      : "" +
                                                  " - " +
                                                  product["doctor"]
                                                              ["doctor_card"]
                                                          ["experience_years"]
                                                      .toString() ==
                                              null
                                          ? ""
                                          : product["doctor"]["doctor_card"]
                                                      ["experience_years"]
                                                  .toString() +
                                              " yrs of exp",
                                  style: TextStyle(
                                    height: 1.1,
                                    fontSize: 10,
                                    color: AppStyle.greyColor,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ))
                              ],
                            ),
                          ),
                          Expanded(
                            child: Row(
                              children: [
                                Text(
                                  product["date"].toString() +
                                      "  " +
                                      product["time"].toString().split(":")[0] +
                                      ":" +
                                      product["time"].toString().split(":")[1],
                                  style: TextStyle(
                                    height: 1.1,
                                    fontSize: 10,
                                    fontWeight: FontWeight.bold,
                                    color: AppStyle.primaryColor,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ]),
          ),
        ],
      ),
    );
  }
}
