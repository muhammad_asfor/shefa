import 'package:flutter/material.dart';
import 'package:progiom_cms/auth.dart';
import 'package:progiom_cms/core.dart';
import 'package:progiom_cms/ecommerce.dart';
import 'package:progiom_cms/homeSettings.dart';
import 'package:tajra/App/Widgets/LoginDialoge.dart';
import '/App/Widgets/MainButton.dart';
import '/Utils/AppSnackBar.dart';
import '/Utils/SizeConfig.dart';
import '/Utils/Style.dart';
import '/generated/l10n.dart';
import '/injections.dart';

import '../../constants.dart';

class ProductCard extends StatefulWidget {
  final TopDoctor product;
  final bool forPointSale;

  ProductCard({required this.product, Key? key, this.forPointSale: false})
      : super(key: key);

  @override
  _ProductCardState createState() => _ProductCardState(product);
}

class _ProductCardState extends State<ProductCard> {
  _ProductCardState(this.product);

  final TopDoctor product;
  bool loadingCart = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(
          context,
          "/productDetails",
          arguments: {"id": product.id.toString(), "goToOptions": false},
        );
      },
      child: Padding(
        padding: EdgeInsets.only(bottom: 20),
        child: Container(
          padding: EdgeInsets.all(5),
          margin: EdgeInsets.symmetric(horizontal: 6.5),
          width: 300,
          decoration: BoxDecoration(
              color: AppStyle.whiteColor,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 10),
                    blurRadius: 10,
                    color: Colors.black12)
              ]),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: SizedBox(
                      width: 147,
                      height: 123,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(
                            product.coverImage,
                            fit: BoxFit.cover,
                            errorBuilder: (context, o, s) {
                              return Icon(Icons.warning);
                            },
                          ))),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 97,
                        padding: EdgeInsets.all(10),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  product.name,
                                  style: TextStyle(
                                    height: 1.1,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    color: AppStyle.secondaryColor,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ))
                              ],
                            ),
                            Spacer(),
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  product.speciality?.title ?? "",
                                  style: TextStyle(
                                    height: 1.1,
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    color: AppStyle.secondaryColor,
                                  ),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ))
                              ],
                            ),
                            Container(
                              height: 5,
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.circular(SizeConfig.h(30)),
                                    gradient: LinearGradient(
                                      colors: [
                                        AppStyle.primaryColor,
                                        AppStyle.secondaryColor,
                                      ],
                                      begin: Alignment.topLeft,
                                      end: Alignment.bottomRight,
                                    ),
                                  ),
                                  height: SizeConfig.h(30),
                                  width: SizeConfig.w(136),
                                  child: Center(
                                    child: Text(
                                      "Get Appointment",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10),
                                    ),
                                  )),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ]),
        ),
      ),
    );
  }

  final debouncer = Debouncer(milliseconds: 1000);
}
